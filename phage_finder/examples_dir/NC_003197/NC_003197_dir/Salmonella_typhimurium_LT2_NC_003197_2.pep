>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1005_gi16764365 integrase
MANASYPTGVENHGGSLRIWFLYKGKRVRENLGIPDTAKNRKIAGELRSSVCFAIRMGNF
NYVEKFPNSPNLARFGQDRKEITVLELTERWSELKRMEISSNTMSRYESIIKNMLPLIGE
NKMVSAVTTEDLLYVRKELLTGFQVMKKDHRTQVKGRKSSTVNNYMMLMAEIFQFGTDNG
YAKENPFSGINRLKKAKGEPDPLTTDEFIRFIQACGHQQMRNLWSLAVYTGMRHGELCGL
AWEDIDLHAGTIIVKRNLTQTDEFTLPKTDAGTDRVIYLIQPAIDALRNQAQLTRLGRQF
EVEVKLREYGQSVIQPCTFVFSPQCVKRGPRTGYHYAVNSINKIWAPIIKRAGIRYRNAY
QSRHTYACWSLSAGANPNFIATQMGHTDAQMVYKVYGKWMSEKSAEQVSLLNQALFRYAP
SLPQSMVAAQ
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1006_gi16764366 excisionase
MSNIIQLTPNKWVSEKVLIAVTGLKPGTITRARKESWMLGREYLHISPDGNPKPSSECIY
NREAVDQWIEAQKKNQPGAKTT
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1007_gi16764367 hypothetical protein
MRLINRSKQSPLGRRACDVALAAHHEKFGDYGRQKHVTNYTVVVDGVKVPVEVVNRATSY
VATAMIGVRKLRNLPAQAN
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1008.S_gi39546307 hypothetical protein hypothetical protein aac26069.1 (gi|7467267)
MENTNIVTTEQQAPNTISASNAIFNVQALGQLTAFANLMADSQVTVPAHLAGKPADCMAI
VMQAMQWGMNPYAVAQKTHLVNGVLGYEAQLVNAVIASSSAIHGRFHYRYGGDWERCTRT
QEITRDKNGKNGKYTVTERVRGWTDEDEIGLFVQVGAILRGESEITWGEPLYLSGVVTRN
SPLWVSNPKQQIAYLGVKYWARLYCPEVILGVYSPDEVEQREEREINPAPVQRMSVQEIT
SEVSTRTSAQESAANVDAVADDLRERIDTASSVDQAKAIRADIESQKALLGTALFTELKN
KAVKRYYQVDAQNKVEAVINSIPNPGEPEAAEMFAKAESTLGAAKRHLGDELHDKYRVTL
DDMKPEYIG
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1009_gi16764369 exodeoxyribonuclease
MSGTNPVFLVRKAKKSSGQKDAVLWCSDDFEAANATLDYLLIKSGAKLKDYFKAVATNFP
VVNELPPEGELSLTFCDYYQLAKDNMTWTQIPGVTLPSSEAAAAARQHIVDGVDTETGEV
LEDHTENFGNESNSPAQATAPAPELTVVATMPLRHRVLAQYIGEGEYLYHVDASQKKEIL
RLEMDTDNSYVQNLLLAAENVEAFKKAIEHDIHKIVNAVKKVFPVDGKTPELATVIQFLK
TWFETEHIDRGLLVKEWAKGNRVSAIQRTESGANAGGGNKTDRNPDYEHTLDTLDVEIAM
ATLPMDFNIYELPGSVYRRAKEIVKKKESPFKEWSAALRATPGILDYSRAAIFALIRSAH
PEFYHYPGRLQGYINANLTETDHENPTEEALTAARHTPEKDAVEEANRQLAAARGEYVEG
ISDPNDPKWVKTGTSQPTTEPELVKNVGNGIFDVSALMQNSSTHGTETNPETTSNVQVQK
ADSDEKQAGDAVQAGEDDLGTGKEAVTVENQNQAEAQQNVPESQQEEPEAAWPEYFEPGR
YEGVPNEVYHAANGISSTQVKDARVSLMYFNARHVEKTIVKERSPVLDMGNLVHVLALQP
ENLEAEFSVEPEIPEGAFTTTATLREFIDAHNASLPALLSADDIKALLEEYNATLPSQMP
LGASVDETYASYEQLPEEFQRIENGTKHTATAMKACIKEYNVTLPAPVKTSGSRDALLEQ
LAIINPDLVAQEAQKSSPLKVSGTKADLIQAVKSVNPAVVFADELLDAWRENTEGKVLVT
RQQLSTALNIQKALLEHPTAGKLLTHPSRAVEVSYFGIDEETGLEVRVRPDLELDMGGLR
IGADLKTISMWNIKQEGLRAKLHREIIDRDYHLSAAMYCETAALDQFFWIFVNKDENYHW
VAIIEASTELLELGMLEYRKTMREIANGFDTGEWSAPITEDYTDELNDFDVRRLEALRVQ
A
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1010_gi16764370 hypothetical protein
MKNSPLIWRGFIHPEFKTGEIMEIVKIEMNLKAVNKSIALFNCENLDGGYVLGKFDSPHC
AVKAISLLTVKVSDGEQAGFGNYRSYKLDYSEKFYQTIH
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1010.1n_gi39546308 hypothetical protein
MLKQCGYCRKSIDEGKEVKNTLLYRNGSQLASKEKEYCSRQCAEYDQMAHES
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1011_gi16764371 hypothetical protein
MLQDRTFSCTFCFTNRTFGFIVSFIDNKRIVVRCKTFRWPAIRQTRVRMIDFARRPARQQ
AVPLNRIEVLIRRLCYLLAQKGDPDA
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1012_gi16764372 probable regulatory protein probable regulatory protein (gi|7467281)
MNKNLHPIFAKRIQQVLDENGWSMADLSRRVMLSHTSVRKWASGTSVASGERLKRLSAVT
GRPEYWFFMEPGDESEGERAEPKPRVLDEKEETLLSLFNQLPEAEKLRVILHTKAVLQEM
DLLKNNVFDLINDLKK
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1013_gi16764373 probable regulatory protein probable regulatory protein (gi|7467282)
MAYQAKRHQPNNHRGYLPMENAIARKLDPPEINPIEIESVLLNRLASVGQKSYAEHMGIS
ESTVSRRKAEGYFCNMAKELAFLGIQAAPPEAVLVSRNYLTAVEILADAGLKAERARPDA
LGWD
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1014_gi16764374 probable regulatory protein probable replication protein (gi|7467283)
MSMSNTAEIYKFPAPIPTQQECRMADLENGYLRLANQIQDALCIVELSGREFRVLNAIIR
LTYGWSKKSDRIANSLIADKTTLKVKHVSEAVLSLAYRNIIILRRIGQTRYIGINTNLDK
WAYSKPHCSKCPVSFPDDEIATWIISVPETRDSYPRKGGRASPKTGIVIPENRDSVLPHS
AIPENGDSYPRKEGRASPKTGNTKDIIPKTNIKDLTPFNPPKGKVKFDPLSIPVPEWLNA
ASWNEWVTYRQQSGKPIKTELTVTKAFRLLKECLDEGHDPVNVINTSIANGYQGLFKPKF
ALNDRRAGRDVNHISAPDKTIPTGFRG
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1015_gi16764375 dna replication initiation atpase
MKNVIGTGSALDRLKRIIPASVQPKFSTADEWRAWQEAEGRKRSEELDRMNQKSRTEKIF
GRSGIQDLHRSCTFANYEVSGEGQRKAYTMAKSYAQNFGSGFASFVFSGGPGTGKNHLAA
AIGNHLLAGGHSVLVVTIPDLMLRVRECYDGGQSEASLLDDLCKVDLLVLDEVGIQRGSS
GEKVILNQVIDRRLSSMRPVGVLTNLNHEGLLDSLGARVIDRLQMDGGMWVNFDWESYRK
NVSHLRIVK
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1016_gi16764376 hypothetical protein
MARPKTQSERMIILERIIGLVKEQGRITTNDVVAIFGVHRTTAEKYLRIALERGGFIRHG
RCGIFRDQRAVIDYDLRRYSSSQVTGFSALPVLEKSPVMQVYGASKMSINKGGAQ
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1017_gi16764377 hypothetical protein
MSNIDKRALREVAERATPGNWRRTSSLFNGITVTPFSLCGEEVTLAHTVEKRDAEFIAAA
NPATVLALLDVLYEFGEDEVAISEYVTNLEDALRVAAAPQQEE
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1018_gi16764378 hypothetical protein
MNYEDKLMYQRMIFEEGKEPRLYSSQHDSKFIPISNNNGKVQKVPVMELKDTRGDVFYAY
NYAYSGKKPSNDEIYKAIDELKPIGEKFEMSKTISD
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1019_gi16764379 hypothetical protein
MRIELVISRAKQLPEGAVPALEKELITRLQNQYENCNLTIRRGSQDGLSIVGAADGDKKR
IQSILQETWESADDWFY
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1020_gi16764380 hypothetical protein
MAHELQLIKQSSGILIPATPETSEILQSKIKLGAVLVAEFRQVRNPAFHRRFFALLNLGF
EYWEPTGGTISANERKLVNGYAKFLAAYGGNESALLDAAEQYLEQIANRRVTNGISLCKS
FDAYRAWVTVEAGHYDAIQLPDGTLRKHPRSIAFSSMDEVEFQQLYKSALDVLWRWILSR
TFRTQREAENAAAQLMSFAG
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1021_gi16764381 hypothetical protein
MAKLPRRKCANKECRQWFHPIREGQIVCSYQCASAVGKEQTRKAREAAQRKAQSLQRAAE
KKERAAWRQRKAAVKPLKHWIDLTQRAVNDICRETELAEGLGCISCGTKTAFAWHAGHYR
STAAAGHLRFTRFNIHLQCDVYNVYKSGNIEAYRAALVERYGEAAVLALENNNTPHRWTV
EELKEIRLAALADLRALKKLEAA
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1021.1n_gi39980852 hypothetical protein
MKPELIEILRMRWLRLRIYRYRGSFPVAYRILRNYVRIEAKREHRNES
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1022_gi16764382 putative molecular chaperone dnaj family
MNLESLPKYFSPKSMMPGAVPCGITSDTLTITDVMASLGLLTAKAAVGIELYLAKAGVLS
SENIIAYIRQLAEQRAERHGALRKMEKGKRSKFLDTMARYVFRDYSLSAASLVTCSSCHG
AKLIDAEVFTNKVTYPDGKPPKWVKDTKGISPSDWEVWKSVREQVRVVCKACDGKGHVKN
ECRCRGRGEILDKKKSELQGVPVYKKCPRCKGRGYPRLKDTEIFKALGVTEMVWRYNYKL
FFDRLVEHCHIEESYAEKVLGNVTR
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1023_gi16764383 hypothetical protein
MPVTLQELRTSPRRGFCFFWWYIPLVSNHRGGDMGFYYVFQYKPKGMSSGKHLNVSEEFS
DRNEAKRAKSRHMINDPDCVFSGIVQADSPAAVLQEVQAESLNRL
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1024_gi16764384 hypothetical protein
MKMHNDPHSMDSQSIFAGSQLLPMEKTSHLALSVGFRSHLA
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1025_gi16764385 hypothetical protein
MKNTALGKFIFIVGTALLLGGCSGMVMPPYATHGTSVGIIAPAGGYSEWHTDSRNHTTGD
SHSQSQGNCTQSEDSQLNENGLTRTHQSNCNTRSQTHSSSTSKTRSSSVGFSVGGPVGAS
IGLIKQMESMNRAPANDMSSNEMFKNFGF
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1026_gi16764386 hypothetical protein
MCLAEYLISGPGGMDPDIEIDDDTYDECREVLSRILEDAYTQSGTFRRLMNYAYDQELHD
VEQRWLLGAGENFGTTVTDEDLESSEGRKVIALNLDDTDDDSIPEYYESNDGPQQFDTTR
SFIHEVVHALTHLQDKEDSNPRGPVVEYTNIILKEMGHTSPPRIAYEFSN
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1027_gi16764387 hypothetical protein
MAGLIAYGRLIYDGATRKNKWLEGVLCGALSLCVTSALDVVGLPVSISPFVGGIIGFVGV
DKLREIAISALKKRAGVNDENQ
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1028_gi16764388 lysozyme
MMRISEKGITLIKEFEGCSLTAYPDPGTGGDPWTIGYGWTHSVDGKPVKPGMMIDEATAE
RLLNTGLVGYENDVSRLVKVKLTQGQFDALVSFAYNLGARTLSSSTLLRKLNAGDYAGAA
DEFLRWNKAGGKVLNGLTRRREAERALFLS
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1029_gi16764389 hypothetical protein bacteriophage es18 gp15 protein (gi|1143595)
MFVGLLLVSLIVAGRLANHYRNNAITYKEQRDTVTHRLTLANATITDMTKRQRDVAALDE
KYTKELADAKAENDALRDDVAAGRRRLYVNATCPAVPTGKSTSTARMDNAASPRLADSAQ
RDYFALKERVKTMQKQLEGAQAYIRTQCHGNAGKTSNQW
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1030_gi16764390 hypothetical protein
MDQEIKSLELNITQLSAITGAHRQTIASRLKGVKTSGGNGSNLKIYRLVDILTAMMTMPA
VTGENDPNKMKPSDRRAWFQSEMTRIELEKEMRTLIPASEVLSVYAVMAKTVVKTLEILP
DLLERDAALPPDALEMTQKIIDQLREDLASMTYQACADAINGDDDDNGDEGQEEEQE
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1031_gi16764391 hypothetical protein {Terminase BLAST PHAGE_wolbac_wKue_WO_INCOM-gi6723244} {Terminase HMM PF05876}
MMTTVTKGKRKSRNSLASAVVAGCDISSMFRPPRRMKISDAVRKYMRVPRDAGNSVAWES
TLTPYVVEAMNCLSSRSYDAVVFVAPARTGKTLGLIDGWISYNIVCDPSDMLVVQMTQDK
AQEHSKRRLAKMFRHSPAIARRLSPHRNDNNVHDKTFRDGSFLKIGWPSINVFSSSDFKC
VALTDYDRFPEDVDGEGDAFSLASKRTTTFMSLGMTLVESSPGREITDTRWKPSSPHEAP
PTTGILSLYNRGDRRRWYWPCPHCGEYFQPSMANMTGYRHIADPMEASEAARIQCPHCHK
LTEPQQKRELNNRGVWLREGQHIDRDGNITGEARRSRIASFWMEGPAAAYQTWAQLVYKL
LTAEEEYERTGSEETLKAVINTDWGLPYQSRRSLEARSGDALMARAEDVSKRTVSDGVRF
IVATVDVQGGKKRRFVVQVVGYGAYGERWIIDRYNIRYSLRVNEDGESQPVNPAARPEDW
DLLKTDVLEKTYPLAADPEQFMPVLAMAVDSGGEDGVTDNAYAFWRRCKRKGVAGRVYLF
KGDSTRREKLITKTYPDNTERSERRAKARGDVPLYLLQTNALKDRVAAALEREEPGANYI
HFPDWLGPWFYEELTYEERSADGKWKKPGRGNNEALDLMCYAHALAILRGYERINWEKPP
GWARLPEKDAAKTTQPVEVRQQEQHEGGEKAVKARKKKILPAWGGGSGGGWL
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1032_gi16764392 hypothetical protein {Portal HMM PF05136} {Portal HMM TIGR01539}
MEERVIGAKGIIVEPQPLTVAGTLNNALAEQIRARWAEWSVSPDVTGQYTRPVLERLLLR
TWLRDGEVFSQMVAGKMPGLEPVAGVPFWLEAMEPDYVPMEQTDSTNNLIQGIYFNDWQR
PKSYIVCKSWPGFATAMVATKLIDAENMLHLKFTRRLNQARGVTLLAPVIIRLLDLKEYE
DSERLAARISAAFAMFIRRSDAMVQDGDAPDYADKDRDLDIEPGTILKDLLPGEDIGTIK
SDRPNANLESFRMGQLRAVAAGVRGSFSSIARNYDGTYSAQRQELVEAQEGYAILQDNFI
AAVSRPVYRRWLATAITAGVIDVPTDTDMATLFNAVYSGPVMPWIDPLKEANAWRILIRG
GAATESDWVRARGGAPAEVKRRRKAEIDENRKLGLVFDTDPAHDPGEQDNAGSEDNSGGD
KNAAGDDSRERTRGGKQ
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1033_gi16764393 clp protease-like protein
MKTTVAAIKMPPVMTAGNGRGEGSSNSWYSIRAAANNTAEVRIYDEIGGWGISARWFAEE
LAALGQINRINLHIHSPGGAVLDGIAIYNLLKNHPAQKTVYIDGMACSMASAIAMVGNPI
IMPENAMMMIHKPRGVAGGEAEDIREYADLLDKIESVIIPIYTEKTGKTPEDIAAMLAKE
TWMSGAECVSEGFADKLIQPVKAMACIHSKRVEEFEHMPQSIKGMIIAPQGNAGAQPQPQ
AKAPESQIQSQAQSLVTVDENAIRARLQEEQRNRITGIQNVFSLSGDRYASLMAKCIADV
DCSLEMAKDRLLAEMAKGITPTNQLNGPQNRAEFHAGMYTGNGNITGDAVRAAVMARAGY
EEAQKDNPYNCMTLRELARISLVARGTGVSSMNPMQMIGMAFTHSTSDFGNILLDVANKS
ILQGWQEAPETFDAWTKKGQLSDFRIAHRVGMGGFSSLRQVREGAEYKYVTTGDKQATIA
LATYGELFSITRQAIINDDMNMLTDVPMKLGRAAKATIADLVYDVLISNQKLSSDNVALF
DKTKHANVLEKAVMDVASLDKARQLMRMQKEGDRHLNIRPAFVLVPTAMESVSNQVIKSV
SVKGADINAGIINPVKDFATVIAEPRLDDASQSTFYLTAAKGSDTVEVAYLNGVDEPYID
QQEGFTVDGVTTKVRIDAGVAPVDYRGMVKCTV
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1034_gi16764394 putative reca/rada recombinase
MAKNYVEDGKTIEIVATTSLKSGDLVQVGDMFAVAVTDIAAGSAGTGIAEGVFSIPKLTT
EDIAVGKKVYLKDNVVQTDATGSLPYVGVVWAPAANGDETVPVKING
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1035_gi16764395 atp-binding sugar transporter-like protein
MADLFDGMKRRMDALIAERFGMKVNINGTDCIVVESDFLAELGPVEGNGKNVVVFSGNVI
PRRGDRVVLRGSEFTVTRIRRFNGKPQLTLEENNGGKGA
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1036_gi16764396 probable minor tail protein
MEVKGLKEAISVLKELDRGYVTRAKIRAINRVAKRVVSVSVRSAAALVVAGDNRRQGIPV
RTVRRRARVRLARADKPFANIYVNCDPLTAIRLLSSPPSTPMRGRKGKPLRIGKYRFDRG
FIAQAPNGWWQVFERSGAGRYPLNVVKIPVADALRHAFNTQVVLQMKTEMPKELKHEISY
ELRRFTKK
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1037_gi16764397 probable minor tail protein
MTRHSAVRQAIIAALKKTDDGSTTFFDGRPVVVEEDELPAVAVYLSDAQYTGPEVDGDIW
SAVLHVEVFLKATAPDSALDEQMENRVYPALGSVAGLGDIIRTMSAQGYNYQRDDEMAMW
GSADLSYDITYSM
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1038_gi16764398 probable major tail protein
MTDTTIPNPLAPVKGANTTFWMYNSEGDPFASPLSDNNWLRLANVKDLQPGELTADAEDN
NYLDDENADWKSTTQGQKSAGDTSVTLAWKPGEDVQKKLIQLFTTGQKRGFRIKYPNGTV
DVFRGWVSSLGKTVQSKDEIARTVKITSVGRPYMAEEDAPEVVSVTGLTVEPTNATVKVG
ATTAVTFTVKPDNVTDKSLRIATSDPTTATVTQAENIATVKGVKAGTVKIIGMTTDGNFT
AIADITVQA
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1039_gi16764399 probable minor tail protein
MFLKKETFTRGDASVALFELSGLQRIEYLEFIQKRTAKYDTDMDGTTEADKRVAYMQMAL
EINAWLVSRSLLNGDSSQDADTLYQSVQAKWSYEALDAGAESVLMLSGLSADKKDNASDS
GNESEDMTPEKS
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1040_gi16764400 probable minor tail protein
MNNELHFVRQLAREFRRPDWRRMLDEMSSTELSEWADFFRENSFSDALLDAEFSTLKAQV
FMLVTGKEIDAADFSLLTLPGAVQSMTEQDLLEVAVGIPGGVRFEPESR
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1041_gi16764401 probable minor tail protein
MDSSDLVEKRIKRCMESSARSVAASAKSISAAMAQSQVATRTQSDAMAQLAREANEARER
AVDLNQKLRAEAAQAAVVAQAQDAAAAAFYRQIDSVKQLSGGLQELQRIQAQVRQAKGRG
DISQGDYLALVSEAAAKTRELTDAEALATQKKAQFIRRLKEQTAVQGLSRTELLRVKAAE
LGVSSAADVYIRKLDTATKSTHALGLKSAMARREIGVLIGELARGNFGALRGSGITLANR
AGWIEQLMSPKGMMLGGLVGGVAAAVYGLGKAYYEGAKESEEFNKQLILTGSYAGKTTGK
LNEMAKSLAGNGVTQHDAAGVLTQVVGSGAFTGQAVAMVSRTAARMQENVGQSVDETIRQ
FKRLQDDPVNAAKELDRALHFLTATQLEQIRVLGEQGRTADAAKIAMSAYSEEMNKRAGD
VHDNLGWIEKAWNAVGDAAKWAWDRMLDIGREDTLDEKIATLQEKIARARKTPWTVSSSQ
TEYDQQQLNELQEQKRQKDLLDAKAQAERNYQKTQKRRNEQNAALNRDNETESLRHQREV
ARITAMQYADAAVRNAALERENERHKKAMARQKEKPKAYYNDEAGRLLLQYSQQQAQTEG
LIAAAKLSTTEKMTEAHKQLLSFQQRIADLSGKKLTADEQSVLAHKDEIALALQKLDISQ
QDLQHQNAFNELKKKTLTLTSQLADEESRVRQQHALALATMGMGDQQRGRYEEHLKIQQH
YQEQLEQLKRDSKAKGTYGSDEYRQAEQELQASLDRRLAEWADYNAKVDAAQGDWTQGAS
RALDNFLAQGGNVAGMTENVFTNAFNGMADSIANFAVTGKGSFRSLTVSILADLAKMEAR
IAASKLLGSVLGMFVFGASAGGSTPSGAYSSAALSVIPNADGGVYRSAGLSQYSGSIVNR
PTFFAFARGAAVMGEAGPEAILPLRRGTDGKLGVVAAGSGGMAMFAPQYHIAISNTGPEL
TPQALKAVYDLGKKAAADFVQQQGRDGGRLSGAYR
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1042_gi16764402 probable minor tail protein
MMKTFHWKVDPDMGVDSEPQVSVVRFGDGYEQRRASGINNDLKKYSVTIRVDREDGPGLE
GFLSQHNGVKAFLWTPPYGYRQIKVVCRKWSVKAGLLKTTFTATFEQVIS
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1043_gi16764403 attachment/invasion protein
MKKIVVAVLVGLALGSIGVANAAGYKNTVSIGYAYTDLSGWLSGNANGANIKYNWEDLDS
GFGAMGSVTYTSADVNNYGYKVGDADYTSLLVGPSYRFNDYLNAYVMIGAANGHIKDNWG
NSDNKTAFAYGAGIQLNPVENIAVNASYEHTSFSTDADSDVKAGT
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-sodC_gi16764404 superoxide dismutase precursor superoxide dismutase cuzn precursor (sw sodc_salty) important virulence function implicated in defence against macrophage killing
MKYTILSLVAGALISCSAMAENTLTVKMNDALSSGTGENIGEITVSETPYGLLFTPHLNG
LTPGIHGFHVHTNPSCMPGMKDGKEVPALMAGGHLDPEKTGKHLGPYNDKGHLGDLPGLV
VNADGTATYPLLAPRLKSLSELKGHSLMIHKGGDNYSDKPAPLGGGGARFACGVIEK
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1045_gi16764405 probable minor tail protein
MQDIPQETLSETTKAEQSAKVDLWEFDLTAIGGERFFFCNEPNEKGEPLTWQGRQYEPYP
IQVQDFEMNGKGASPRPNLVVANLFGLVTGMAEDLQSLVGASVVRHQVYSKFLDAVNFSN
GNPDADPEQEAVARYNVEQLSELDSSTATIILASPAETDGSVVPGRTMLADSCPWDYRDE
NCGYDGPPVADEFDKPTSDPKKDKCSHCMKGCEMRNNLVNAGFFASINKLS
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1046_gi16764406 probable tail assembly protein
MINDDILAHARQCAPAESCGYVVRTAQGERYFPCENLSAEPTMYFRISPEDYLNARNRGD
IVALVHSHPDGKPCLSSADRTLQIQSGLDWWLVCDNRIHKFRCVPHLTGRQFEHGVTDCY
TLFRDAYHLAGIDMPDFDREDDWWSQGKSLYLDHLEAAGFYRVNPEDAQPGDVLICCFGS
PTPNHAAIYCGNGELLHHIPEQLSKREGYNDKWQRRTHSIWRHRQWCESAFTGIYNDLES
ASASA
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1047_gi16764407 probable tail assembly protein
MATTDTLNMAAPAMVRICLYGDLQRFGKRISLSIKTAAEGIHALAIQLPGFRQRMNEGWY
QVRIAGSDMAPDTLTARLNESLPPGAVVHIVPRMAGAKNGIWQVVAGAALIGASFIPGLN
AVAAAVLFSAGTSMALGGVAQMLTPVPKTPTVGQADNGKQNTYFSSLENMVAQGNPVPVL
YGEMKIGSRVISQMMSTRDESTSGKVVVIGSPLQANTTSRQDGGITRPSVVIRQ
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1048_gi16764408 host specificity protein j
MSKGGGKGHTPREAKDDLKSTQQLSVIDALSEGPIVGPVNGLQSVLINNTPVVDADGNSN
IHGVTVVYQVGETPQAPLEGFEASGAETVLGVEVKHDNPVTRTVVSENVDRLRFTFGVQM
LQETTDKGDRNPSSVNLLIQFQRSGIWNTEFDITINGKITTQYLASVVADNLPPRPFSVR
MVRVTPDSTTDRLQNKTLWSSYTEIIDIRQGYPGTAVAGLLVDAEQFGSQQVTRNYHLRG
RIFQVPSNYDPDTRTYTGLWDGAFKPAYTNNPAWCTMDKLTHPRYGLGRRIGGADVDKWA
LYAIAQYCDQPVPDGFGGTEPRMTLNAYITTQRKAYDVLADFCSVMRCMPVWNGRKMTFI
QDRPSDKAWTYTNGNVVGGRFKYSFSALKDRHNAVEVRYTDPLNGWQTSTELVEDHASQA
RYGRNLLKMDAFGCTSRGQAHRTGLWVMMTELLETQTVDFSVGAEGLRHTPGDIIEVCDN
DYAGASVGGRITDLDISTRTLTLDREITLPESGATTLNIVGPDGKPFSTEIQSQPAPDRV
VTKVLPETVQPYSIWGLKLPSLKRRLFRCVRIKENDDGTYAITALQHVPEKESIVDNGAH
FDPLPGTTNSIIPPAVQHLTVSTDNDSTLYQAKAKWGTPRVVKDVRFVVRLTTGSGNEGD
PVRLVTTATTSETEYAFHELPLGDYTLTVRAINGYGQQGEPASVAFSIQAPEAPSTIEMT
PGYFQITVTPHQTVYDASVQYEFWYSATQLATAADIQSKAQYLGVGSFWIKDGLKPLHDA
WFYVRSVNLAGKSVFAEASGRPGMTRKGIWIFLRD
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1048.1N_gi39546309 hypothetical protein
MLKKIDLTENNASKLQQFSKEWQDANDKWSATWGVKIEQTKDGKYYVAGLGLSMEDTPDG
KISQFLVAADRIAYINPANGNETPGFVMQGDQIIMNEAFLKYLSAPTITSGGNPPAFSLT
PDGKLTAKNADISGHINAVSGSFTGEINATSGKFSGVIEAREFVGDICGSKVMQGVSIRA
TNDERSTSTRYTDSATYQIGKTITVMANCERNGGSGAITVTININGQVKTAEVMPYTAGI
PAMYQTVVFSVYTTSPVVDISVSLRVRGQYTTSASVWPLVMVSRSGSNFTN
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1049_gi16764409 probable tail fiber protein
MPVLISGVLKDGTGTPVQNCTIQLKACRTSTTVVVNTVASENPDDAGRYSMDVEQGQYTV
TLLVDGYPPSHAGVITVYDDSKPGTLNDFLGAMTEDDVRPEALRRFEAMVEEVARQASEA
SRNATAAGQASEQAQTSAGQASESATAAVNAAGAAEASATQAASSAASAESSAGTATTKA
GEASASAASADTARTAAAASAAAAKTSEANADASRTAAGDSAAAAAASATAAQTSAERAG
ASETAAKTSETQAASSAGDAGASATAAAASEKAAAASAAAAKTSETNAATSASTAAASAT
AASSSASEASTHAAASDTSASLAAQSSTAAGAAATRAEDAAKRAEDIADVISLEDASLTK
KGIVKLSSATDSDSEALAATPKAVHAVMDEVQTKAPLDSPALTGTPTAPTPETAAAGIEI
ATAAFVAAKVAQLVGSAPETLDTLKELADALGNDPNFATTVLNKLAGKQPLDDTLTALSG
KSVDGLIEYVGLRETINHAADALLKSQNGGDIPEKPLFVQNIGALPASGTAVAANRLASR
GALPALTGATRGSDSGLIMGEVYNNGYPTQYGNILRLTGTGDGEILIGWSGTNGAPAPAY
IRSHRDTADAEWSEWAMLYTSLNPPPNSYPVGAAIAWPSDATPAGYALMQGQSFDKSAYP
LLAIAYPSGIIPDMRGWTIKGKPISGRAVLSQEMDGNKSHSHSARAQDTDLGTKSTSSFD
YGTKSTNTTGNHTHQFGGYINSYWGDSNHTSFQPGGGAWTQAAGDHAHTVYIGGHEHTMY
IGPHGHVVIVDADGNAETTVKNIAFNYIVRLA
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1050_gi16764410 tail fiber assembly like-protein
MTFKMSEQAQTIKIFNLRSDTNEFIGAGDAYIPPHTGLPANCTDIAPPDIPASHIAIFDA
ETQTWSLHEDHRGEMVYDTTTGNQVYISAPGPLPENVTSVSPGGEYQKWDGKAKVWVKDE
AAEKAAQLRQAEETKSRLLQMASEKIAPLQDAVDLGIATDDEKARLDEWKKYRVLVNRMD
TAAPDWPERPASQ
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-sseI_gi16764411 secreted effector protein secreted effector i (gi|8927434) secreted by spi-2 colocalizes with host polymerizing actin cytoskeleton
MPFHIGSGCLPAIISNRRIYRIAWSDTPPEMSSWEKMKEFFCSTHQAEALECIWTICHPP
AGTTREDVVSRFELLRTLAYDGWEENIHSGLHGENYFCILDEDSQEILSVTLDDVGNYTV
NCQGYSETHHLTMATEPGVERTDITYNLTSDIDAAAYLEELKQNPIINNKIMNPVGQCES
LMTPVSNFMNEKGFDNIRYRGIFIWDKPTEEIPTNHFAVVGNKEGKDYVFDVSAHQFENR
GMSNLNGPLILSADEWVCKYRMATRRKLIYYTDFSNSSIAANAYDALPRELESESMAGKV
FVTSPRWFNTFKKQKYSLIGKM
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1053_gi16764412 hypothetical protein
MCGRFAQAQTREEYLAYLADEADRNIAYDPQPIGRYNVAPGTKVLLLSERDEQLHLDPVI
WGYAPGWWDKAPLINARVATAASSRMFKPLWQHGRAICFADRWFEWKKEGDKKQPYFIHR
KDGKPIFMAAIGSTPFERGDEAEGFLIVTSAADKGLVDIHDRRPLALTPETARVWMRQFL
EPHSKSITYRVIPALTRPMMRKDTNPCQ
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1054_gi16764413 hypothetical protein
MYHFMIVNLYQDNYSGYRQHGGAEVQILSCRPKFPLKTSLSGLFFLWLNFLRGSYGVKLG
NKAVEIRRNLRLIHQFAHCSVSEFINTQIFISNYCINSL
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1055_gi16764414 hypothetical protein
MLRHIQNSLGSVYRSNTATPQGQIIHHRNFQSQFDTTGNTLYNNCWVCSLNVIKSRDGNN
YSALEDITSDNQAFNNILEGIDIIECENLLKEMNVQKIPESSLFTNIKEALQAEVFNSTV
EDDFESFISYELQNHGPLMLIRPSLGSECLHAECIVGYDSEVKKVLIYDSMNTSPEWQSN
IDVYDKLTLAFNDKYKNEDCSICGLYYDGVYEPKPLHSSSWKDWCTIL
>PFPROPHAGE_Salmonella_typhimurium_LT2_NC_003197_phi2_UNPUB-STM1056_gi16764415 msga-like protein
MFVELVYDKRNVEGLEGASEIILAELTKQVHQIFPDAEVRVKPMQANCLNSDTNKSDREN
LNR
