>STM4196_gi16767446 putative cytoplasmic protein
MIIVSVLRQSKDFTTKHAQWLHKQLKGYDSVCLTDALKIKGVNTAPLLYDWPGWWAKLEL
FNPLHPVLGNEDILYIDIDSVIVGDITPLTTMKKITLLNDFSQHGASVAPATGIMFIPAP
AKKNVWDEFMKNPEKEINAIRTPPYHGDQGFIGRICQDAERWQNILPGRIISYKANIATP
KMIGFNPELYDGTGNGKLPDGASIVCFHGSPRPWNTALPWVPYFSLKNTIQSKVKQYKLS
LR
>STM4197_gi16767447 putative inner membrane protein
MDYGNTGDMRLFAVMNFYYIDYLCRPITIAVVIFLFWWRLANEMYIYGSKKQKKTGLWIN
RKLNSKFGIDIELGAVKRGNKITPIPTLILYIPMKI
>STM4198_gi16767448 putative cytoplasmic protein
MSQTTVWLACYKGRSEHRGIARFADWLTRKVTRGIYSHCELAVAHGGNEYLCYSASFRDR
GVRGKIIPLPDDKWDKLPLKATLPEVEVFFRKHNGKRYDWQGALGIALYNRERKDRLFCS
EFCAEFLGLNDSWRYSPSHLYALVSSWQYDC
>STM4199_gi16767449 putative cytoplasmic protein
MTQYAYYHPATFEVIDWIDTSSLNVVLPDSVISIDDEQWRLRDKNCWVCLNPLEFITTPP
PGKFYRLQGNEWVYDEALFVSALESVKAQVIQAIKAHRDDVTADYIVIDGNHFHSDANSR
IQQMSLTRMGQAQQIPAGLMWQAKNNGLIELTNDIAAQFETVTMDHDMRLFANAQRHIAA
VEALEDIEAVLDYDYSTGWQP
>STM4200_gi16767450 putative phage tail fiber protein h
MDNEFYTLLTDRGMAKIASALADKKQLHLQKMAVGDGGGQYYEPIASQTKLRHEVWRGEM
NTLTVAPNNPNWLIAELVLPEDVGGWYVREVGVFDDEGELIAIGKFPESYKPLLPGGCGK
QVCIRLIMEVSNTTAVTLTVDPSIVLATRDYVDTRLDEHEHSTNHPDATLTQKGFTQLSN
ATDSDDETKAATPKAVKAAMAEARNHTHTWNQITGVPDGTLTQKGIVKLNSATDSTSTTE
AATPSAVKAAMDKANAAAPANHTHVWNQVTGVPDGTLAQKGIVKLNNATDSTSTTEAATP
SAVKAAMDKASAAAPARHTHAWGQITGAPDGTLTQKGIVKLNNATDSTSTTEAATPSAVK
AAYDKASAAAPANHSHYQFFTANGTFTVPDGVTQVFVEMLGGGGGGGGGGHTSNTDGLLY
CSGGNAGKSGEPEIAIVPVIAGNNYPVTVGAGGASGAGGVLPNSNPTGNVVQVGQAGNSG
INGGNSIFIDVVANGGAGGAGGIVQSRIPLSGFNQLDSISGGNAETTFFGKGGTGAVLSN
GSNASGYGAGGGGGASVNSLNIALSGYAGGRGSSGFVKISW
>STM4201_gi16767451 putative phage tail protein
MDKLLLPPPLASDERFSILANIAAERFAQIDLTALLVYLVDIVDASALPSLAEQFHVQGL
EGWLFAANEQEKRELIKQAIELHKYKGTPWAVRRVLEILSLPGTISEWFEYGGKAYFFKV
EIKLINQGMDENLFNNLVDLIHEYKNVRSKLEALIVWIINQSAIPVIGSALYGGEITTVL
PFQVLEVQQTKPIYFGTGQWSLEITSIYPE
>STM4202_gi16767452 putative phage baseplate protein
MAIAEPDFIDRDPAQITSEMIAQYEEASGKKLYPAQAERLLIDLFAYRENLVRIAIQEAA
KQNLVAYSRAPMLDYLGELVGVHRLPAQAAKTTLQFSVTQAAKSNLVIPQGTRASASDSV
MFATDEDVLLPAGSLSVAVTATCVVTGEPGNNWQPAQISALVDRVGNYDISVTNLTASSG
GCGEENDDALRKRIQLAPESFSNAGSYGAYRFHTLSVSQSIIDVAVLGPDEGLAEGCVEL
YPLTLNGLPGPELLAQIEREVSKEKKRPLTDKVSAKCSPRVAYQISARLTLFTTADQETT
LAAAREAINTWTRSRQTRLGQDIVPNQIIKVLQVDGVYDVALDMPAKKVLQAHEWAECTA
IDVTIAGVSDG
>STM4203_gi16767453 putative phage baseplate protein
MNTKTRPSTLHWQPALQRPEEYVCGLDDIHQAIHIILRTPRGSDPHRPLFGSNLWRYIDY
PIERAIPHVVRESVEAIRMWEPRCRLLKVTPTIDGEHLTLRVQWRAADGVINSTEVLWR
>STM4204_gi16767454 putative inner membrane protein
MNTMLKILPKTAMILLAFLAIFLIEWYTPIHSDDYRYYLLGISPESHFHHYMTWSGRIIA
DYTSALILYTRSQLVYSISAAVSTLVFCYFIVKTPSGTLRWNKSDYLLFPLIFFTYWISN
PNLGQTTFWIVGAANYLWTNLFVVAWLFFFYTITIKNSKAISPWVALLSFMAGCSNESVS
PFVSLISVLAIAYELWQNKSVSRNKIVYSLCAIAGSCVLILSPGNFIRASGKEFWYGRPI
FERIFIHLTERVHNHLALIWIAYVVLLLLVLLVIFNKQIRAKIDKTSLICAALVVCIGIG
TSLIMFASPSYPDRVMNGTFMFFLLAISFIAYALLKSGVKAGVVGVTAVTVLCGIVFLWS
YSLMLNGYKKTAGQEIVRQKIITKEIAAGKQKFIIPDYYFVKLQNSGGHFGLFHDPAVYG
EYYHVQAIFKKKVNFDYSVIANGAKHSLSNETTAYSNTRGDFAIISREQLTGSITLSVNG
RQKTIPVEKMKHAEINDEFWYYASVDKGEITAISF
>STM4205_gi16767455 putative phage glycosyltransferase
MKISLVVPVFNEEDAIPIFYKTVREYSSLKPYNVEIIFVNDGSHDATESIISALAVADPL
VVPISFTRNFGKEPALFAGLDHATGDVVIPIDVDLQDPIEVIPHLINKWQAGAEMVLAKR
IDRSTDGHLKRKSAEWFYRLHNKISTPKIEENVGDFRLMSREIVENIKLLPERNLFMKGI
LSWVGGQTDVVEYARAERVAGNSKFNGWKLWNLALEGITSFSTFPLRIWMYIGVSVSALS
LIYAMWMIIDKLMWGNPVPGYPSLMTAILFLGGIQLIGIGIMGEYIGRVYTEVKQRPRYI
VKNKKTMME
>STM4206_gi16767456 putative phage glucose translocase
MIKLFIKYVSIGVLNTALHWAIFALCVYGFQTSQALANVAGFAVAVSFSFFANARFTFGA
SVSTGRYLLYVGFMGVLSAVVGWTGDKCAMPPIFTLIVFSAISLICGFLYSRFIVFRNEK
>STM4207_gi16767457 putative phage baseplate component
MKGVTRQTGIISDIDEAVVRVRVTLPECDNLRSNWLAVLQRNTQDNKDYWLPDIGEQVEV
SLDDNGEDGVVLGAVYSSVDTAPLASRDKRYVQFSDGAAFEYDRALHQLTVNGGIEKIVI
EVKEHTQLTSPQVEVRAQHVTVISETVDVAATSVGVKAVDVNVEAPHTGIKALNVTVDAP
LSTFTGDVTVMKKLTWLGGMAGSGGVGNSAVITGNVNVLGNVNASGTLMDNGGNSNHHSH
>STM4208_gi16767458 putative cytoplasmic protein
MAEITVSGGVFATLTPIFTLWYGHKEITYDIAPYVTSISYSDSIKNESDVIAIALEDSTG
RWVNEWYPGKGDTLALRLGYQGEDLLDCGIYVIDKIDISAPPSTVNIDGIATSVSKALRT
KNSQGFEETTLSAIASRIAQKHGLTLAGKIAPLTIDRVTQYAETDVAFLKRLASEYGYTV
KVTATELIFSHLPTLRCLAPVKTLRRTDVSHYTFKDTINRIYKNATVQHQNSKQKELVIY
THDSQEKTSARGAATSADTLKINSRAPDTGAAQAKANAALDSHNEYQQTGTLSLMGCPQL
TAGNKIELSDFGVLSGQWLIDKSMHKLTRSGGYTTEIDISRGPATSQ
>STM4209_gi16767459 putative inner membrane protein
MRYLEHVTTDGERWDNLAWRYYGDALAYERIIAANPHVAIMPVLPSGVRLIIPVISVTQT
TPELPPWLR
>STM4210_gi16767460 putative methyl-accepting chemotaxis protein
MYAVLGEIEFDVVAYWDEFESTMGVDYTSHARIEGKPGVQFIGDKLDKITLKFNFHSQYC
QPTTELNRLREAMTAHQAMALVFGNGDYRGWFVITDLTATHQHTDPYGNVIAQGGSLSLQ
EYTGDPKSPLLPPAITTQEPNIDEMLDELPDVNDSWFDELLSVVEEGMREAKEMMDEVAD
AIDDIKKTIAQAKELVKEAKALKEKCGDIVDSLKKTMSSIDALFQQPLDLQTLAGLPKAL
AAKMQELIDSLPGIRECAGDAGTLIEHAESLFDAITSSVAEATYDSAATLVNQARGTLQT
SAPDVSQLAAADITRSL
>STM4211_gi16767461 putative phage tail protein
MANDIITQLQARNETLTQAIARYGSLNASTLHTLSFEQTKVTRLTQQLANSALRREENDK
QRAGLLEKTQTFAGQLGKLLNVETPDWKLPYEFQGNMVDMAAKGGMDNTARDALSLNIRD
WSLDFNQDQKDLQSTAATMIEGGVSALQDLSRYMPDIAKAATASRDSAQSWAQAALATRD
KLNIAPDDFRFAQNMLYSVAKSGGGSVAEQTQWINAFAGKTGAQGKEGIAELTATMQIAM
KNAPDAGAAAANFDHFLKSAFSKETDSWFARQGVDLQGSLLEHQQNGIGVTEAMTHIVQM
QLEKMNPQILDTFRQTMKIEDLSARGDALQAMVEKFNLGAMFGDAQTRDFLAPMLANMDE
YRQLKASAMQAAGQHVIDDDFAAKMTSPGEQTKALQLSLNDLWLTVGLELMPAIGELAQS
ITPLVRQFSAWLRENPALVQGVAKVVSVIWLFNGALNILRLGANLIASPFIRLIDIFLKV
KAGLALGGGSRALSVLKSFGNGAKSLTMLLGNGLIKGLRLVGQTFIWLGRALLMNPVGLT
ITAIAGAAYLLYRYWEPISGFFAGVWERIKTAFDGGIAGVTRLILDWSPLGLFYRAFAGV
LDWFGIELPASFSEFGGNILDSLINGILNALPFLSGAIEKIKALIPDWAKSALGISAEMP
SVAAAVPGIAGTMVAQQASAPLASGAKAVTTSAKTMASPQPVKTKSAATPPTPAALPGKS
GGKPYTLPSRAQSNVQVHFSPQVTVQGSGANAAKDINNVLSLSKRELERMINDVMAQQRR
REYA
>STM4212_gi16767462 putative phage tail core protein
MAGKIQINRITNANIYLDGNNLLGRASEIKLPDISMIMQEHKALGMVGKIELPAGFDKLE
GEIKWNSFYHDVMRKTANPWQAVALQCRSSIDCYNSQGKADQLALVTHMTVMFKKNPLGT
FKQNENPEFSSAFGCTYIKQVVDGETLLELDYLANIFRVNGADQLNAYRNNIGG
>STM4213_gi16767463 putative phage tail sheath protein
MAANYLHGVETIEIETGPRPVKAVKSAVIGLIGTAPCGPVNQPTLCLSESDAAQFGPGLA
NFTIPQALKAIYDHGAGTVVVINVLNPAVHKSTIPSETVKVDDNGQIQLKHGAVQTMNIG
RSTNAGNAYIKGTDYTIDMLTGKITCMGTNLKPGVQAYVNYTYADPTKVTAADIVGAVNT
AGDRTGMKLLQDTWNQFGFYAKILIAPVFCTQNSVAVKLIAQAEALGAITYIDAPIGTTF
QQVLAGRGPQGAINFNTSSDRARLCYPHVKVYDSATNSEVLEPLSSRAAGLRAKVDLEKG
FWWSNSNQEIQGITGVERSLSAMIDDPQTEVNQLNENGITTIFNSYGSGLRLWGNRTAAW
PTVTHMRNFENVRRTGDVINESIRYFSQQYMDMPINQALIDALTESVNTWGRKLIADGAL
LGFECWYDPARNEQTELAAGHLLLSYKFTPPPPLERLTFETEITSEYLVSLESNR
>STM4214_gi16767464 putative cytoplasmic protein
MKYIYSGPASGVTLADGQEVLLWPNSEISLPEDNEWVITMIARRHLVPVVTQEVETNEEE
IVHGS
>STM4215_gi16767465 putative cytoplasmic protein
METLSVIHTVANRLRELNPDMDIHISSTDAKVYIPTGQQVTVLIHYCGSVFAEPENTDAT
VQKQLIRISATVIVPQISDAINALDRLRRSLGGIELPDCDRPLWLESEKYIGDAANFCRY
ALDMTASTLFIAEQESKDSPLLTIVNYEEIQ
>STM4216_gi16767466 putative inner membrane protein
MKLSIDFWEVISLLLSFVGLMFAAGKLLLAQIEKRLNERFEALEAARRESEAGWSRLERE
FLEFRADLPLHYVRREDYLRGQAVLEAKLDALYSKIELIQRGNH
>STM4217_gi16767467 putative soluble lytic murein transglycosylase
MRYQYVCLVCAMTFLSADAAEPPRASLQWRNEVIRTAREIWGLNAPVADFAGQLHQESGW
APDALSPAGAQGMAQFMPATAKWVSQLYPELHENKPFNPAWAIRALVQYDRQLWKSVLAK
NSCQRMAFTLSAYNGGQGWVNRDKKLAAAKGLDASIWFEHVERVNAGRSAANWRENRHYP
KAILYQHAPRYLQWGRASCIH
>STM4218_gi16767468 putative inner membrane protein
MRKIIVPRLSGWLVASVVLFALIGWTSSAQIPVVIYKLSLVSLSAVLGYWLDRSLFPWAR
PDSFCPWEESLCCAAAMIRRAIIVAAICLAVALGL
>STM4219.S_gi39546392 putative cytoplasmic protein
MIQKSKEMVRLPEIINDLAFHASQVLIESMNIDSASAENAGQAIADRMMRNWGGQSIYFP
KGISGRASERDYQIYSECDGRNYAELAKKYNLTLQWIYKIVKRVHTEKQHQRRML
