#!/usr/bin/perl -w

use Bio::Perl;
use Bio::DB::GenBank;
#Usage: perl get_gbk.pl  <accession_number> 

$gb = Bio::DB::GenBank->new();
$seq= $gb->get_Seq_by_acc($ARGV[0]);

$out = Bio::SeqIO->new(-file => ">$ARGV[0].gbk", -format => 'Genbank');
$out->write_seq($seq);

exit;

