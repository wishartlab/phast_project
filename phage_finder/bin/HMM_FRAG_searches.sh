#!/bin/bash

home="/home/phast/public_html"
pepfile=$1
if [ ! -d FRAG_HMM_searches_dir ] # check if directory is present
then
    mkdir FRAG_HMM_searches_dir
else
    rm -rf FRAG_HMM_searches_dir/
    mkdir FRAG_HMM_searches_dir
fi
cd FRAG_HMM_searches_dir/
total=`cat $home/phage_finder/hmm_FRAG.lst | wc -l | sed 's/^ *//'`
for i in `cat $home/phage_finder/hmm_FRAG.lst`
do
  let "count=count+1"
  Result=`echo $count $total | awk '{printf( "%3.1f\n", (($1/$2) * 100))}'`
  echo -ne "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b$Result% complete"
  hmmsearch $home/phage_finder/PHAGE_FRAG_HMMs_dir/$i.FRAG $pepfile >> $i.FRAG.out
done
echo
cat *.out >> ../combined.hmm_FRAG
cd ..
