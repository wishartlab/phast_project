#!/bin/bash

home="/home/phast/public_html/"
pepfile=$1
if [ ! -d GLOCAL_HMM_searches_dir ] # check if directory is present
then
    mkdir GLOCAL_HMM_searches_dir
else
    rm -rf GLOCAL_HMM_searches_dir/
    mkdir GLOCAL_HMM_searches_dir
fi
cd GLOCAL_HMM_searches_dir/
total=`cat $home/phage_finder/hmm.lst | wc -l | sed 's/^ *//'`

for i in `cat $home/phage_finder/hmm.lst`
do
    let count=count+1
    Result=`echo $count $total | awk '{printf( "%3.1f\n", (($1/$2) * 100))}'`
    echo -ne "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b$Result% complete"
    hmmsearch   $home/phage_finder/PHAGE_HMMs_dir/$i.HMM $pepfile >> $i.out
done
echo
cat *.out >> ../combined.hmm_GLOCAL
cd ..
