#!/bin/bash

# Bash script to run the Phage_Finder pipeline

# Usage: findPhage.sh <prefix of .pep/.ffa, .ptt and .con/.fna file>

# NOTE: a phage_finder_info.txt file will be searched before a .ptt file
# .pep is the multifasta protein sequence file
# .ptt is a GenBank .ptt file that has the coordinates and ORF names with annotation
# .con is a file that contains the complete nucleotide sequence of the genome being searched
START1=$(date +%s)
home="/home/phast/public_html"
base=`pwd`
prefix=$1
num=$2
database=$3
strict_mode=$4
  if [ -s $base/$prefix.pep ] # check if .pep file is present
  then
      pepfile="$prefix.pep"
  elif [ -s $base/$prefix.faa ]
  then
      pepfile="$prefix.faa"
  else
     echo "Could not file $prefix.pep or $prefix.faa.  Please check to make sure the file is present and contains data"
     exit 1
  fi 
    if [ -s $base/phage_finder_info.txt ] # check for phage_finder info file and if it has contents
    then
        infofile="phage_finder_info.txt"
    elif [ -s $base/$prefix.ptt ]
    then
          infofile="$prefix.ptt"
    else
      echo "Could not find a phage_finder_info.txt file or $prefix.ptt file.  Please make sure one of these files is present and contains data."
      exit 1
    fi
    if [ ! -s $base/combined.hmm_GLOCAL ] # if GLOCAL HMM results not present, search
    then
        ## conduct GLOCAL HMM searches
        echo "  GLOCAL HMM searches ..."
	START=$(date +%s)
        HMM_searchs.sh $base/$pepfile
	END=$(date +%s)
	DIFF=$(( $END - $START ))
	echo "GLOCAL HMM searches took $DIFF seconds"
    fi
    if [ ! -s $base/combined.hmm_FRAG ] # if FRAGment HMM results not present, search
    then
        ## conduct FRAGment model HMM searches
	START=$(date +%s)
        echo "  FRAGment model HMM searches ..."
        HMM_FRAG_searches.sh $base/$pepfile
	END=$(date +%s)
	DIFF=$(( $END - $START ))
	echo "FRAGment model HMM searches took $DIFF seconds"
    fi

    if [ ! -e $base/ncbi.out ] # if BLAST results not present, search
    then
        ## do NCBI BLASTP searches
	START=$(date +%s)
        echo "BLASTing $pepfile against the Phage DB ..."
       # blast_v_dir="/home/prion/phage/tmp/$prefix/blast_v"
       # ssh -i /home/phast/.ssh/scp-key  prion\@botha1.cs.ualberta.ca  "mkdir -p $blast_v_dir"
       # scp -i /home/phast/.ssh/scp-key  $pepfile  prion\@botha1.cs.ualberta.ca:$blast_v_dir
       # ssh -i /home/phast/.ssh/scp-key  prion\@botha1.cs.ualberta.ca "cd $blast_v_dir; perl /home/prion/phage/cgi/call_blast_parallel.pl $blast_v_dir/$pepfile $database" >/dev/null
       # scp -i /home/phast/.ssh/scp-key  prion\@botha1.cs.ualberta.ca:$blast_v_dir/$pepfile\_blast_out  ncbi.out
	blastall -p blastp -d $home/phage_finder/DB/prophage_virus.db -m 8 -e 0.001 -i $pepfile -o ncbi.out -v 4 -b 4 -a 2 -F F
	END=$(date +%s)
	DIFF=$(( $END - $START ))
        echo "BLASTing took $DIFF seconds"
    fi
    if [ -s $base/$prefix.con ]
    then
        contigfile="$prefix.con"
    elif [ -s $base/$prefix.fna ]
    then
        contigfile="$prefix.fna"
    else
        echo "Could not find a phage_finder_info.txt file or $prefix.ptt file.  Please make sure one of these files is present and contains data.  In the meantime, I will go ahead and run phage_finder.pl without this information, but beware... NO att sites will be found!"
        contigfile=""
    fi
    if [ ! -e $base/tRNAscan.out ] && [ $base/$contigfile ] # if tRNAscan.out file not present, and contig file present, then search
    then
        ## find tRNAs
        echo "  find tRNA sequences ..."
	START=$(date +%s)
        tRNAscan-SE -B -o tRNAscan.out $base/$contigfile > /dev/null
	END=$(date +%s)
	 DIFF=$(( $END - $START ))
        echo "tRNAscan-SE took  $DIFF seconds"

    fi

    if [ ! -e $base/tmRNA_aragorn.out ] && [ -e $base/$contigfile ] # if tRNAscan.out file not present, and contig file present, then search
    then
        ## find tmRNAs
        echo "  find tmRNA sequences ..."
	START=$(date +%s)
        aragorn -m -o tmRNA_aragorn.out $base/$contigfile
	END=$(date +%s)
         DIFF=$(( $END - $START ))
        echo "aragorn took $DIFF seconds"
    fi

    ## find the phage
    echo "  searching for Prophage regions ..."
    START=$(date +%s)
    Phage_Finder.pl -t ncbi.out -i $infofile -r tRNAscan.out -n tmRNA_aragorn.out -A $contigfile -H $num  
    END=$(date +%s)
    DIFF=$(( $END - $START ))
    echo "Phage_Finder.pl took $DIFF seconds"

    END1=$(date +%s)
    DIFF1=$(( $END1 - $START1 ))
    echo "Whole phage_finder took $DIFF1 seconds"

