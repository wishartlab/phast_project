#!/usr/bin/perl

my $input=$ARGV[0];
my $output=$input;
$output =~s/\..*//;
$output.=".fna";
gbk2fna($input,$output);
exit;
	
sub gbk2fna{

	my ($data,$output)=@_;
	
	my @GenBankFile = (  );
	my $o=''; #organism
	my $d=''; #total sequence
	my $d1=''; #version+accession
	my $d4=''; #nucleic acid sequence
	my $d4t=''; #temp of d4
	my $true=0;
	
	@GenBankFile=get_file_data($data);
    
	foreach my $line (@GenBankFile) {
		if($line=~/^\/\/\n/){
			last;
		}
		elsif($line=~/^DEFINITION/){
			$line=~s/^DEFINITION (.+)\.\n$/$1/g;
			$o=$line;
		}
		elsif($line=~/^VERSION/){
			$line=~s/^VERSION\s+(.+)\s+GI:(.+)\n$/$2|gb|$1/g;
			$d1=$line;
			chop($d1);
        }
        elsif($line=~/^ORIGIN/){
        	$true=1;
        }
        elsif($true==1){
        	$d4t.=$line;
        }
    }
    
    $d4t=~s/[\s0-9]//g;
	$d4=uc($d4t);
	
	$d=">gi|".$d1."|".$o."\n";
	
	my $i=0;
	while($i<length($d4)) {
		$d.=substr($d4,$i,70)."\n";
		$i+=70;
	}

    open(FNA,">$output");
	print FNA $d;
	close FNA;
	return 1;
    
}

sub get_file_data{
	
	my ($file)=@_;
	use strict;
	use warnings;

	# Initialize variables
	my @filedata=();
	unless(open(GET_FILE_DATA,$file)) {
		print STDERR "Cannot open file \"$file\"\n\n";
		exit;
	}
	@filedata=<GET_FILE_DATA>;
	close GET_FILE_DATA;
	return @filedata;
}

