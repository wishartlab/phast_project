#!/bin/bash

# Bash script to run the Phage_Finder pipeline

# Usage: findPhage.sh <prefix of .pep/.ffa, .ptt and .con/.fna file>

# NOTE: a phage_finder_info.txt file will be searched before a .ptt file
# .pep is the multifasta protein sequence file
# .ptt is a GenBank .ptt file that has the coordinates and ORF names with annotation
# .con is a file that contains the complete nucleotide sequence of the genome being searched

home="/apps/phast/sub_programs"
base=`pwd`
prefix=$1
num=$2
database=$3
strict_mode=$4
  if [ -s $base/$prefix.pep ] # check if .pep file is present
  then
      pepfile="$prefix.pep"
  elif [ -s $base/$prefix.faa ]
  then
      pepfile="$prefix.faa"
  else
     echo "Could not file $prefix.pep or $prefix.faa.  Please check to make sure the file is present and contains data"
     exit 1
  fi 
    if [ -s $base/phage_finder_info.txt ] # check for phage_finder info file and if it has contents
    then
        infofile="phage_finder_info.txt"
    elif [ -s $base/$prefix.ptt ]
    then
          infofile="$prefix.ptt"
    else
      echo "Could not find a phage_finder_info.txt file or $prefix.ptt file.  Please make sure one of these files is present and contains data."
      exit 1
    fi
    #if [ ! -s $base/combined.hmm_GLOCAL ] # if GLOCAL HMM results not present, search
    #then
        ## conduct GLOCAL HMM searches
        #echo "  GLOCAL HMM searches ..."
        #HMM_searchs.sh $base/$pepfile
    #fi
    #if [ ! -s $base/combined.hmm_FRAG ] # if FRAGment HMM results not present, search
    #then
        ## conduct FRAGment model HMM searches
        #echo "  FRAGment model HMM searches ..."
        #HMM_FRAG_searches.sh $base/$pepfile
    #fi

    if [ ! -e $base/ncbi.out ] # if BLAST results not present, search
    then
        ## do NCBI BLASTP searches
        echo "  BLASTing $pepfile against the Phage DB ..."
        blast_v_dir="/home/prion/phage/tmp/$prefix/blast_v"
        ssh -i /apps/phast.ssh/scp-key  prion\@botha1.cs.ualberta.ca  "mkdir -p $blast_v_dir"
        scp -i /apps/phast/.ssh/scp-key  $pepfile  prion\@botha1.cs.ualberta.ca:$blast_v_dir
        ssh -i /apps/phast/.ssh/scp-key  prion\@botha1.cs.ualberta.ca "cd $blast_v_dir; perl /home/prion/phage/cgi/call_blast_parallel.pl $blast_v_dir/$pepfile $database" >/dev/null
        scp -i /apps/phast/.ssh/scp-key  prion\@botha1.cs.ualberta.ca:$blast_v_dir/$pepfile\_blast_out  ncbi.out
        #blastall -p blastp -d $home/phage_finder/DB/phage_06_06_05_release.db -m 8 -e 0.001 -i $pepfile -o ncbi.out -v 4 -b 4 -a 2 -F F
    fi
    if [ -s $base/$prefix.con ]
    then
        contigfile="$prefix.con"
    elif [ -s $base/$prefix.fna ]
    then
        contigfile="$prefix.fna"
    else
        echo "Could not find a phage_finder_info.txt file or $prefix.ptt file.  Please make sure one of these files is present and contains data.  In the meantime, I will go ahead and run phage_finder.pl without this information, but beware... NO att sites will be found!"
        contigfile=""
    fi
    if [ ! -e $base/tRNAscan.out ] && [ $base/$contigfile ] # if tRNAscan.out file not present, and contig file present, then search
    then
        # find tRNAs
        echo "  find tRNA sequences ..."
        tRNAscan-SE -B -o tRNAscan.out $base/$contigfile > /dev/null
    fi

    if [ ! -e $base/tmRNA_aragorn.out ] && [ -e $base/$contigfile ] # if tRNAscan.out file not present, and contig file present, then search
    then
        # find tmRNAs
        echo "  find tmRNA sequences ..."
        aragorn -m -o tmRNA_aragorn.out $base/$contigfile
    fi

    # find the phage
    echo "  searching for Prophage regions ..."
    pwd
    echo "Phage_Finder.pl -t ncbi.out -i $infofile -r tRNAscan.out -n tmRNA_aragorn.out -A $contigfile -H $num $strict_mode "
    START=$(date +%s)	
    Phage_Finder.pl -t ncbi.out -i $infofile -r tRNAscan.out -n tmRNA_aragorn.out -A $contigfile -H $num $strict_mode 
    END=$(date +%s)
    DIFF=$(( $END - $START ))
    echo "Phage_Finder.pl  took $DIFF seconds"
