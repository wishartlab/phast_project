#!/bin/bash

glimmer_dir=$1
num=$2

host="$HTTP_PHAST_CLUSTER_USERNAME@$HTTP_PHAST_CLUSTER_HOSTNAME"

ssh -i /apps/phast/.ssh/scp-key  $host  "mkdir -p  $glimmer_dir"
scp -i /apps/phast/.ssh/scp-key  $num.fna  $host:$glimmer_dir
ssh -i /apps/phast/.ssh/scp-key  $host  "perl  /home/prion/phage/cgi/call_glimmer.pl $glimmer_dir/$num.fna"
scp -i /apps/phast/.ssh/scp-key  $host:$glimmer_dir/$num.fna.predict  $num.predict
