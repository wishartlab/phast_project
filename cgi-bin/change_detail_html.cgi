#!/usr/bin/perl -w

# change detail.txt file into html format
use CGI;
use CGI::Carp qw(fatalsToBrowser);

my $base_root = "/var/www/html/phast/current/public";
my $q = new CGI;
my $dir = $q->param('num');
my $file = $q->param("file");
my $tmp1= $q->param("tmp");
my $tmp = (defined $tmp1 && $tmp1 ne'')?  "$base_root/$tmp1" : "$base_root/tmp";
my $detail_file = (defined $file && $file ne '')? $file : "detail.txt";
open(IN, "$tmp/$dir/$detail_file") or die "Cannot open $tmp/$dir/$detail_file";
my $header='';
my $flag=1;
my %hash =();
my $key='';
my $items = '';
while(<IN>){
	if ($flag ==1){
		$header= $_;
		$flag=0;
		next;
	}
	if ($_=~/CDS_POSITION/){
		$items= $_;
		next;
	}
	if ($_=~/#### region (\d+)/){
		$key = $1;
		$hash{$key}='';
		next;
	}
	if ($_=~/^\d+\.\.\d+/ or $_=~/^complement\(\d+\.\.\d+\)/i){
		$hash{$key} .= $_;
	}
}
close IN;
$header=~s/gi\|.*?\|ref\|.*?\|//;
$header=~s/gc%/GC%/;
print "content-type: text/html \n\n";
my $content="<p><b>$header</b></p>";

$content.="<br><br><a href='/tmp/$dir/$detail_file'>Text file for download</a><br><br>";
$content.= "<table> <tr><td style=\"background-color:#CCFF55;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Hits against Virus and prophage DB</td> </tr>".
	"<tr><td style=\"background-color:#E0E0E0;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Hits against Bacterial DB or GenBank file</td></tr></table><br>";
foreach (sort {$a<=>$b} keys %hash){
	$content .= get_content($_, $hash{$_}, $items);
}
print $content;
exit;

sub get_content{
	my ($key , $detail, $items)=@_;
	my @header = split(" ", $items);
	
	
	my @array=split("\n", $detail);
	my $a = scalar(@array);
	my $content = '';
	$content .= "<table width=1200 border='1' cellpadding='1' cellspacing='1' style='font-size:14;'>";	
	$content .="<tr><th style=\"background-color:#33FFCC;\">#</th>";
	foreach (@header) {
		if ($_=~/prophage_PRO_SEQ/){
			$_= "SEQUENCE";
		}
		if ($_=~/EVALUE/){
			$_="E-VALUE";
		}
		$content .= "<th style=\"background-color:#33FFCC;\">$_</th>";
	}
	$content .="</tr>";
	
	
	foreach my $i (0..$#array){
		my $color = "#E0E0E0";
		if ($array[$i]=~/PHAGE_|PATHOGENICITY_ISLAND|VIRULENCE_PROTEIN|PROTEIN_TOXIN|ANTIBIOTIC_RESISTANCE|TRANSCRIPTION_FACTOR|DIFFERENTIAL_GENE_REGULATION/){
			$color = "#CCFF55";
		}
		if ($array[$i]=~/attL/  or $array[$i]=~/attR/ or $array[$i]=~/tRNA/ or $array[$i]=~/tmRNA/){		
			$a -=1;
		}
		my @arr=split(/\s\s\s\s\s+/, $array[$i]);
		my $k= $i+1;
		$content .="<tr><td style=\"background-color:$color;\">$k</td>";
		foreach my $j(0..$#arr){
			if ($j!= $#arr){
				if ($arr[$j]=~/attR/ or $arr[$j]=~/attL/){
					$arr[$j].="&nbsp;&nbsp;&nbsp;&nbsp;$arr[$#arr]";
				}
				$arr[2]='0.0' if ($arr[2] eq 'N/A');
				$content .="<td style=\"background-color:$color;\">$arr[$j]&nbsp;</td>";
			}else{
				$content .="<td style=\"background-color:$color;\"><a href='/cgi-bin/fasta_seq.cgi?seq=$arr[$j]&amp;rec=$arr[0]'>Click</a></td>";
			}
		}
		$content .="</tr>";
	}
	$content .="</table><br>";
	$content ="<a name = $key></a>Region $key, total : $a CDS.<br>".$content;
	return $content;
}
