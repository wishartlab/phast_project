#!/bin/bash

dir=$1
host="$HTTP_PHAST_CLUSTER_USERNAME@$HTTP_PHAST_CLUSTER_HOSTNAME"

scp -i /apps/phast/.ssh/scp-key  -r   $host:phage/result_tmp/$dir/png_input  ./$dir/.
scp -i /apps/phast/.ssh/scp-key  -r   $host:phage/result_tmp/$dir/png_input_RNA  ./$dir/.

