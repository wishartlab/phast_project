#!/usr/bin/perl -w

# this program will initiate all the sub programs to finish 
# phage finder and filter's jobs.
# The code in the BEGIN block will be executed very early
# on, even before the rest of this script is parsed.
#
BEGIN {

   # Use the CGI::Carp module and import the carpout() function.
   #
   use CGI::Carp qw(carpout);

   # Send warnings and die messages to the browser.
   #
   carpout(STDOUT);
}
use File::Path;
use CGI;
use POSIX qw/ceil/;

my $BASE_ROOT="/var/www/html/phast/current/public";
my $tmp_dir = "$BASE_ROOT/tmp"; # this dir will cotain all the temp files and result files.
my $exec_dir = "$BASE_ROOT/cgi-bin";  # dir of executables.
my $min_DNA_length = 1500; # minimum length of DNA sequence
our $max_job_num = 10; #the maxmum of jobs that each external users can submit.


my $q=new CGI;
my $acc = $q->param('acc');
my $gi = $acc;
my $gbk = $q->param('gbk');
my $filename=$q->param('seq_file');
my $fasta_seq = $q->param("fasta_seq");
my $contig = $q->param("contig");
my $client_IP = $ENV{'REMOTE_ADDR'};

my $strict_mode = '';
$strict_mode = $q->param("strict");
my $queue_file = "$tmp_dir/queue.txt";

our $split_flag = 0;
our $split_count = 0;

our $MAX_length = 30000000; # the maximum length that the server will handle, if longer 
				# the server will cut the sequence into pieces based on this length.
our $max_FNA_file_size = 12; # 12M bp, max size of fna file.
$gi=~s/\s//g;
$acc=~s/\s//g;
$acc = uc($acc);
$filename=~s/\s//g;
my $f=0;
if ($gbk ne ''){
	$f+=1;
}
if ($acc ne ''){
	$f+=1;
}
if ($filename ne ''){
	$f+=1;
}
if ($fasta_seq ne '' && $fasta_seq !~/^\s+$/s){
	$f++;
}
&check_input($f);
 
my ($num, $flag);
my @nums =();
if ($gbk ne ''){
	$num = time;
	$flag = '-g';
}
elsif ($acc ne ''){
	$num = $acc;
	$flag = '-a';
	my $acc_cp = $acc;
	$acc_cp =~s/[A-Z]//ig;
	$acc_cp =~s/\d//g;
	$acc_cp =~s/[\.\_]//g;
	if ($acc_cp ne ''){
		HTML_error("That is not accession number or GI number. Please check!", '');
	}
	if (length($acc) >14){
		HTML_error("That is not one single accession number or GI number. Please check!", '');
	}
}else{
	$num=time;
	$flag = '-s';
}
#check if the cases exist already, if yes, show the result , if not, do the rest.
my $found = 0;
my $num1='';
my $num2='';
my $num_cp = $num;
my $num_cp1 ="ZZZZZZZZ";
if ($num_cp=~/[a-z]{4}00000000/i){
	$num_cp=~s/NZ_//i;
	$num_cp=uc($num_cp);
        if ($num_cp =~/\.\d+/){
                  $num_cp1 = $num_cp;
                  $num_cp1 =~s/\.\d+//;
	}        
}
my $ls =`ls $tmp_dir`;
foreach my $l (split("\n", $ls)){
	if ($num eq $l && (-e "$tmp_dir/$l/$l.done")){
		$num1 = $num;
		$found=1;
		last;	
	}
}
if ($found==0){
	if (-e "$tmp_dir/list"){
		my $list = `cat $tmp_dir/list`;
		my @list_lines = split("\n", $list);
		foreach (@list_lines){
			if ($_=~/^\s*$/){#empty line
				next;
			}
			my @temp=split(" ", $_);
			$num1 = $temp[0]; 
			$num2 = $temp[1];
			if($num1 eq $num_cp   or  $num2 eq $num){
				$found = 1;
				last;
			}
	
		}
	}
}
if ($found ==0 && $num_cp1=~/[a-z]{4}00000000/i){
	if (-e "$tmp_dir/list"){
        	my $list = `cat $tmp_dir/list`;
	        my @list_lines = split("\n", $list);
        	foreach (@list_lines){
	                if ($_=~/^\s*$/){#empty line
                	        next;
        	        }
	                my @temp=split(" ", $_);
                	$num1 = $temp[0];
        	        $num2 = $temp[1];
	                if( $num1 =~/$num_cp1/){
                        	$found = 1;
                	        last;
        	        }
    	
        	} 
	}
}

if ($found ==1){
	if (!(-d "$tmp_dir/$num1")){
		$num1=~s/\.\d+//;
	}
	if (-s "$tmp_dir/$num1/true_defective_prophage.txt"){
		&show_result($num1);
	}elsif( -s "$tmp_dir/$num2/true_defective_prophage.txt"){
		&show_result($num2);
	}
	exit;
}

my $log_file ="$tmp_dir/$num/$num.log";
system("rm -rf $tmp_dir/$num; mkdir -p $tmp_dir/$num; chmod -R 777 $tmp_dir/$num");
system("echo 'mkdir -p $tmp_dir/$num\n' 2>&1 |cat >> $log_file");
if ($flag eq "-g"){
	&GetRemoteGBK($gbk,$num,\@nums);
	if (!(-e "$tmp_dir/$num/$num.gbk")){
		HTML_error("Error: There is no gbk file uploaded into the server. Please inform the administrator of the server!", $num);
	}
	foreach  $num (@nums) {
		my $data = `cat $tmp_dir/$num/$num.gbk`;
		my $log_file ="$tmp_dir/$num/$num.log";
		if ($data !~/LOCUS.*ACCESSION.*ORIGIN/s){
			system("echo 'Error: Your case number is $num. There is not a GBK file. It must have 'LOCUS', 'ACCESSION', 'ORIGIN' characters in the file. Please check!' >>$log_file");
                        HTML_error("Error: Your case number is $num. There is not a GBK file. It must have 'LOCUS', 'ACCESSION', 'ORIGIN' characters in the file. Plese check!", $num);
                }

		if ($data !~/ORIGIN\s*\n\s*\d+\s\w+/s){
			system("echo 'Error: There is no DNA sequence in your GBK file. Please check!' >>$log_file");
			HTML_error("Error: There is no DNA sequence in your GBK file. Plese check!" , $num);
		}
		if ($data !~ /CDS\s+/s){
			system("echo 'Error: There is no CDS region in your GBK file. Please check!' >> $log_file");
			HTML_error("Error: There is no CDS region in your GBK file. Please check!", $num);	
		}
		if ($data !~ /translation=".+?"/s){
			system("echo 'Error: There is no translation in the CDS regions in your GBK file. Please check!' >> $log_file");
			HTML_error("Error: There is no translation in the CDS regions in your GBK file. Please check!", $num);
		}
	}
}elsif ($flag eq "-s"){
	if(length($filename)>0) {  
  		&GetRemoteFna($filename,$num, \@nums);
	}
	elsif (length($fasta_seq) > 0) {
		$fasta_seq =~s/\r//gs;
		open(OUT,">$tmp_dir/$num/$num.fna") or die "Cannot write $tmp_dir/$num/$num.fna";
		if ($fasta_seq !~/>/s){
			$fasta_seq = ">gi|00000000|ref|NC_000000| Genome; Raw sequence\n".$fasta_seq;
		}
		print OUT $fasta_seq;
		close OUT;
		split_file("$tmp_dir/$num/$num.fna", $num, \@nums);
	  	
	}
	if (!(-e "$tmp_dir/$num/$num.fna")){
		HTML_error("Error: There is no sequence file uploaded into the server. Please inform the administrator of the server!", $num);
	}
	my ($size_fna_file) = `du -sm $tmp_dir/$num/$num.fna` =~/^(\d+)/;
	if ($size_fna_file > $max_FNA_file_size) {
		HTML_error("Error: Your input file sequeence is too big. Please cut it into pieces each in less than 10M bp first and submit them into the server.", $num);
	}	
}else{ # $flag='-a'
	push @nums , $num;

}
&HTML_Page($num, scalar @nums);

# Fork the process so that the child process performs the structure prediction
# and the parent process prints a web page for the user.

$SIG{CHLD}='IGNORE';
	# Ignore child process (so page will be displayed before child process
	# finishes).
my $pid = fork();
if (not defined $pid) {
	print "Internal server error.\nPlease report problems using our " . '<a href="http://feedback.wishartlab.com?site=PHAST" target="_blank">feedback form</a>' . "\n";
	
	# Print error to the log file.
	open(OUT, ">>$log_file");
	print OUT "Cannot create fork";
	close(OUT);
	exit(1);
} elsif ($pid == 0) {
	# I'm the child process.
	close STDERR;
	close STDOUT;
	chdir $tmp_dir;
	# now add the new case to the queue file, the information addedd is the arguments for the phage.pl
	my $cnt=0;
	foreach $num (@nums){
		$cnt++;
		my $log_file ="$tmp_dir/$num/$num.log";
		system("echo  'write $flag   $num to $queue_file' >> $log_file");
		my $date = `date`;
		system("echo '$num $date' >> $tmp_dir/case_input.log");
		system("echo '$flag  $num' >> $queue_file");
		system("echo  'chmod 777 $queue_file' >> $log_file");
		system("chmod 777 $queue_file");
		system("echo  'chmod -R 777 $tmp_dir/$num' >> $log_file");
		system("chmod -R 777 $tmp_dir/$num");
		system("chmod 777 $tmp_dir/case_input.log");
		if ($cnt ==$max_job_num){
			last;
		}
	}
	my $ti = `date`;
	system("echo 'Case input time $ti' >> $log_file");
 	system("perl $exec_dir/phage.pl "); # if this line is commented out, it is so only temporarily by David Arndt
	exit(0);
}else{
	# I'm the parent process.
	exit(0);
}

exit;

# cleanup some unnessisary files
sub cleanup{
	system("rm -rf *.tab *.out *.seq* *.con $tmp_dir*.pep");
	system("rm -rf ../combined.hmm*  ../FRAG_HMM_searches_dir/  ../GLOCAL_HMM_searches_dir ../error.log  ../formatdb.log   ../ncbi.out ../phage_finder_info.txt  ../tmRNA_aragorn.out ../tRNAscan.out");
}

sub HTML_error{
	my $msg=shift;
	my $num=shift;
	$num=~s/_.*//;
	&HTML_Header_results;
	print "$msg";
	&HTML_Footer_results;
	if ($split_flag ==1){
		for(my $i=1; $i<=$split_count; $i++){
			rmtree("$tmp_dir/$num\_$i");
		#	print "rm $tmp_dir/$num\_$i<br>";
		}
	}
	exit(0);
	
}

sub show_result{
	my $num = shift;
	my $results_link = "Results.cgi?num=$num";
	print "content-type: text/html \n\n";
	my $fresh_time = 0;
	print <<HTML;
		<HTML>
		<HEAD>
		<TITLE>PHAST results</TITLE>
		<meta http-equiv="REFRESH" content="$fresh_time;url=$results_link">
		</HEAD>
		<BODY >
		</BODY>
	  	</HTML>
HTML

}

sub HTML_Page
{
	my $num=shift;
	my $multi = shift;
	
	my $results_link = "Results.cgi?num=$num&multi=$multi";
	print "content-type: text/html \n\n";
		
	my $inter_cont = "Loading the file now. This will take 2-20 seconds. Once completed, the calculation will be conducted...
		<br/><br/>
		You will automatically be redirected to your <a href=\"$results_link\">results/processing</a> page...<br>
		<br><br><br><br><br>";
	my $fresh_time = 5;
	
	
	print <<HTML

	
		<HTML>
		<HEAD>
		<TITLE>PHAST results</TITLE>
		<meta http-equiv="REFRESH" content="$fresh_time;url=$results_link">
		</HEAD>
		<BODY BGCOLOR="white">
		<font face="Arial">
		<CENTER>
		<IMG SRC="../image/phage.png" height=125 width=500><br><br>
		</CENTER>
		<center>
		<table align="center"  border="0" cellpadding="0" cellspacing="0" id="navigation" bgcolor='orange'>
        <tr width="100%" style="font-size:19;font-style:arial;">
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../index.html"><b>Home</b></a>&nbsp;&nbsp;&nbsp;</td>
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../documentation.html"  ><b>Documentation</b></a>&nbsp;&nbsp;&nbsp;</td>
	<td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../io.html"  ><b>Input</b></a>&nbsp;&nbsp;&nbsp;</td>
	<td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../io1.html"  ><b>Output</b></a>&nbsp;&nbsp;&nbsp;</td>
	  <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../how_to_use.html"  ><b>Instructions</b></a>&nbsp;&nbsp;&nbsp;</td>
	 <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../Download.html"  ><b>Databases</b></a>&nbsp;&nbsp;&nbsp;</td>
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../contact.html"  ><b>Contact</b></a>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        </table>
		</center>
		<LEFT>
		<br><br>
		<P ALIGN="LEFT">
			<IMG SRC="../image/blueline.gif" WIDTH="1240" HEIGHT="7"
			 SGI_SRC="../image/blueline.gif">
		</P>
		$inter_cont
		<P ALIGN="LEFT">
		<IMG SRC="../image/blueline.gif" WIDTH=1240 HEIGHT=7
			SGI_SRC=\"../image/blueline.gif\">
		</P>
		<font size=2>
		Please report bugs and send your comments using our
	    	<a href="http://feedback.wishartlab.com?site=PHAST" target="_blank">feedback form</a>
	    	</font>
		</LEFT>
		</font>
	  	</BODY>
	  	</HTML>
HTML

	
}

sub GetRemoteGBK
{
   	my ($infile, $num, $nums) =@_;
   	#print $input;
   	binmode $infile;

   	open(LF,">$tmp_dir/$num/$num.gbk");
   	binmode LF;
   	print LF <$infile>;
   	close(LF);
	my $data = `cat $tmp_dir/$num/$num.gbk`;
	$data =~s/\r//sg;
	open(LF,">$tmp_dir/$num/$num.gbk");
	print LF $data;
	close LF;
	open(LF, "$tmp_dir/$num/$num.gbk");
	my $count=0;
	while(<LF>){
		if ($_=~/^\/\//){
			$count++;
		}
	}
	close LF;
	if ($count>1){
		my $c=1;
		open(LF, "$tmp_dir/$num/$num.gbk");
		system("mkdir -p  $tmp_dir/$num\_$c");
                open(OUT,"> $tmp_dir/$num\_$c/$num\_$c.gbk");
		push @$nums, "$num\_$c";
		while(<LF>){
			if($_!~/^\/\//){
				print OUT $_;
			}else{
				print OUT $_;
				close OUT;
				$c++;
				if ($c <=$count){
					system("mkdir -p  $tmp_dir/$num\_$c");
                                	open(OUT,"> $tmp_dir/$num\_$c/$num\_$c.gbk");
					push @$nums, "$num\_$c";
				}
			}	
		}
		close OUT;
		close LF;
	}else{
		push @$nums, $num;
	}
}

sub GetRemoteFna
{
   	my ($infile, $num, $nums) =@_;
   	#print $input;
   	binmode $infile;

   	open(LF,">$tmp_dir/$num/$num.fna");
   	binmode LF;
   	print LF <$infile>;
   	close(LF);
	my $data=`cat $tmp_dir/$num/$num.fna`;
	$data=~s/\r//sg;
	 open(LF,">$tmp_dir/$num/$num.fna");
	print LF $data;
	close LF;
	split_file("$tmp_dir/$num/$num.fna", $num, $nums);
}

sub split_file{
  my ($file, $num, $nums)=@_;
  if ($contig  ne 'on'){
    system("echo 'NOT contig concatenation case!!!' >>$log_file");
    open (LF, $file);
    my $count=0;
    while(<LF>){
      $count++ if ($_=~/>/);
	}
	close LF;
    if ($count>1){
      $split_flag=1;
      $split_count=$count;
      system("echo 'Multiple cases ' >> $log_file");
      my $c=0;
      my $seq='';
      my $header='';
      open(LF, $file);
	  while(my $line=<LF>){
        chomp($line);
        if($line=~/>/){
		  if ($c != 0){
            create_sub_fna_file_push_into_array($seq, '', $c, $nums, $num, $header);
            $seq='';
            $header='';
          }
          $c++;
		  if ($c == $max_job_num){
			last;
		  }
          $header = $line;
	    }else{
          $line=~s/\s//g;
          $seq .=$line;
        }
	  }
      close LF;
	  if($c < $max_job_num){
        create_sub_fna_file_push_into_array($seq, '', $c, $nums, $num, $header);
	  }
	}else{
      $split_flag = 0;
      $split_count=$count;
      system("echo 'single case only' >> $log_file");
      handle_input_file_based_on_seq_length($file, $num, $nums, $contig);
    }
  }
  else{  #contig concatenate flag is on
      system("echo 'Contig file case! Concatenate all the sequences' >> $log_file");
      system("cp $file  $file.org");
	  handle_input_file_based_on_seq_length($file, $num, $nums, $contig);
  }
}

#handle the input file based on the sequence length. if lenght > $MAx_length, we cut it into pieces.
sub handle_input_file_based_on_seq_length{
        my ($file, $num, $nums, $contig) = @_;
        open(IN, $file) or die "Cannot open $file";
        my $seq='';
        my $header='';
        while (<IN>){
                chomp($_);
                if ($_!~/>/){
                        $seq .=$_;
                }else{
                        $header = $_ if ($contig ne 'on');
                }
        }
        close IN;
		if (length($header) > 1000) {
            HTML_error("Error: In your file's header, <br/>$header<br/>It is too long. Please check if the DNA seq is within the same line of header.", $num);
        }
        if (length($seq) <= $MAX_length){
                system("echo 'seuqnce is less than $MAX_length' >$log_file");
                create_sub_fna_file_push_into_array($seq, '', 0, $nums, $num, $header);
        }else{
                my $size = ceil(length($seq)/$MAX_length);
                system("echo 'sequence is longer than $MAX_length, cut into $size pieces with $MAX_length' >$log_file");
		for(my $i=1; $i <= $size; $i++) {
                        my $sub_seq = substr($seq, ($i-1)*$MAX_length, $MAX_length);
                       if (length($sub_seq)>0){
                                system("echo 'create NO. $i case' >> $log_file");
                                create_sub_fna_file_push_into_array($sub_seq, $MAX_length, $i, $nums, $num, $header);
                        }
                }
         }
}
# create sub sequence files and folders for each run. and push it into array $nums
sub create_sub_fna_file_push_into_array{
        my ($seq, $max_length, $i, $nums, $num, $header)=@_;
        if (length($seq) < $min_DNA_length){
                HTML_error("Error: In your file '$header', the length of DAN sequence is less than $min_DNA_length.<br>The sequence is too short to dectect phage region!<br>If your input file contains multiple contigs, please concatenate your sequence first <br>or use the contig file check box on the home phage.", $num);
        }
	 mkdir "$tmp_dir/$num\_$i";
        my $case='';
        if ($i==0){
                $case= $num;
        }else{
                $case= "$num\_$i";
        }
        if ($header eq ''){
                $header = "Genome; Raw sequence";
        }else{
                $header =~s/>//g;
        }
		if (length($header) > 1000) {
            HTML_error("Error: In your file's header, it is too long. Please check if the DNA seq is within the same line of header.", $num);
        }
        open(OUT, ">$tmp_dir/$case/$case.fna") or die "Cannot write $tmp_dir/$case/$case.fna";
        my @temp= $seq =~/(\w{0,70})/g;
        if ($max_length ne ''){
                 my $start= ($i-1) * $max_length +1;
                my $end = $start -1 + length($seq);
		if (length($header) > 1000) {
			HTML_error("Error: In your file's header, it is too long. Please check if the DNA seq is within the same line of header.", $num);
		}
		if ($header !~/gi\|\d+\|ref\|\S+\|/){
                	print OUT ">gi|00000000|ref|NC_000000| $header from $start to $end\n";
		}else{
			print OUT ">$header from $start to $end\n";
		}
	}else{
		if ($header !~/gi\|\d+\|ref\|\S+\|/){
                	print OUT ">gi|00000000|ref|NC_000000| $header\n";
		}else{
			print OUT ">$header\n";
		}
        }
        foreach (@temp){
              print OUT uc($_)."\n";
        }
        close OUT;
        system("echo 'Write $tmp_dir/$case/$case.fna.' >> $log_file");

        push @$nums , $case;

}


sub check_file{
	my $f = shift;
	$data = `cat $tmp_dir/$num/$f.gbk`;
	$data =~/\nORIGIN(.*)\/\//s;
	$data = $1;
	$data =~s/[\s\n\datgc]//si;
	if (length($data)!=0){
		print "<p> Your input file is not a DNA genome file. Please check! </P>\n";
	   	exit(1); 
	}
}

sub check_input{
	my $flag=shift;
	if ($flag ==0){
   		HTML_error("Error: one of accession number, GBK formatted file, or your FASTA sequence file must be input.", '');
	}
	if ($flag !=1){
   		HTML_error("Error: Only one of accession number, GBK formatted file, or your FASTA sequence file can be input.", '');
	}
}

sub get_gbk_file{
	my ($num, $flag, $log_file, $tmp_dir)= @_;
	
	system("perl $exec_dir/get_gbk.pl $num $flag $tmp_dir > $log_file.2"); # create $num.gbk file
	system("cat $log_file.2 >> $log_file; rm -rf $log_file.2");
	print_msg("$num.gbk", $log_file);
	if (!(-s "$num.gbk")) {
	   	system("echo $exec_dir : $num : $flag Cannot get back gkb file from Genbank! The case is terminated! >> $tmp_dir/$num/fail.txt");
	   	exit(1);
	}
	
}
# chech the list file if the number is there.
sub check_list_file{
	my $num=shift;
	my $data = `cat $tmp_dir/list`;
	my $found = 0;
	if ($data =~/$num/s){
		$found = 1;
	}
	return $found;
}

sub print_msg{
	my ($file, $log_file)= @_;
	if (-s $file ){
		system("echo  $file is created ! >> $log_file");
	}else{
		system("echo  $file is not created ! >> $log_file");
	}
}

sub HTML_Footer_results
{
	print <<HTML
	<br/><br/><br/><br/><br/><br/>
	<P ALIGN="LEFT">
		<IMG SRC="../image/blueline.gif" WIDTH="540" HEIGHT="7"
		 SGI_SRC="../image/blueline.gif">
	<br/>
	Problems? Suggestions? Please use our <a href="http://feedback.wishartlab.com?site=PHAST" target="_blank">feedback form</a>
	</P>
	
	</font>
	</BODY>
  	</HTML>
HTML
}

sub HTML_Header_results
{
	print "content-type: text/html","\n\n";
	print <<HTML
	<HTML>
  	<HEAD>
  	<TITLE>PHAST results </TITLE>
  	</HEAD>
	<BODY BGCOLOR="white">
	<CENTER>
	<IMG SRC="../image/phage.png" height=125 width=500>
	</Center>
	<BR><BR><BR><BR><BR>
	<P ALIGN="LEFT">
		<IMG SRC="../image/blueline.gif" WIDTH="540" HEIGHT="7"
		 SGI_SRC="../image/blueline.gif">
	</P>
  	<BODY>
HTML
}
