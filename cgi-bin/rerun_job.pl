#!/usr/bin/perl -w

#########################################################################
#
# Script to re-run a PHAST job. Doesn't entirely work since some temp files need to be deleted.
#
# Usage:
# $ perl rerun_job.pl <job_id> [--pre]
#
# David Arndt, Jan 2016
#
#########################################################################

use strict;

my $tmpdir = '/var/www/html/phast/current/public/tmp';


my $id = $ARGV[0];
my $prepend = 0;
if (defined($ARGV[1]) && $ARGV[1] eq '--pre') {
	$prepend = 1;
}

my $log = "$tmpdir/$id/$id.log";

my $job_string = '';

open (IN, "<", $log) or die "Could not open file '$log' $!";
while(<IN>) {
	my $line = $_;
	if ($line =~ m|^write +(-[a-z] +\w+) +to /var/www/html/phast/current/public/tmp/queue\.txt$|) {
		$job_string = $1;
	}
}
close(IN);

my $queue_file = "$tmpdir/queue.txt";
my $queue_backup = "$tmpdir/queue_bak.txt";
my $job_string_tmp = "$tmpdir/job_string_tmp.txt";

if ($job_string ne '') {
	
	# restore .fna file etc.
	system("cp -p $tmpdir/$id/$id\.fna\.org $tmpdir/$id/$id\.fna");
	system("rm $tmpdir/$id/$id\.process");
	system("rm $tmpdir/$id/tRNAscan.not_done");
	
	if ($prepend) {
		system("echo '$job_string' > $job_string_tmp");
		system("echo 'before:'");
		system("cat $queue_file");
		system("cp $queue_file $queue_backup");
		system("cat $job_string_tmp $queue_backup > $queue_file");
		system("echo 'after:'");
		system("cat $queue_file");
		system("rm $job_string_tmp");
	} else {
		# append
		
		system("echo 'before:'");
		system("cat $queue_file");
		system("cp $queue_file $queue_backup");
		system("echo '$job_string' >> $queue_file");
		system("echo 'after:'");
		system("cat $queue_file");
	}
	
} else {
	print STDERR "Could not read job string from $log\n";
	exit(1);
}
