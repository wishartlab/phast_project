#!/usr/bin/perl -w

# Script for phage to display a web page to the user with an update on the
# progress of their submission, and to display the results page when the
# submission is done processing.
#

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use strict;

my $q=new CGI;

# Get input arguments.
my $num=$q->param('num');
my $multi = $q->param('multi');
my $tmp1= $q->param('tmp');
my $type = $q->param('type');
my $tmp_dir = (defined $tmp1 && $tmp1 ne '')? "/var/www/html/phast/current/public/$tmp1":"/var/www/html/phast/current/public/tmp";
my $results_link = "/cgi-bin/Results.cgi?num=$num&multi=$multi";
my $result_url = "/tmp" ;
my $http_url = "/Plasmid/bin-release/Plasmid.html";
my $html_link = (defined $tmp1 && $tmp1 ne '')? "/cgi-bin/change_to_html.cgi?num=$num&tmp=$tmp1" : "/cgi-bin/change_to_html.cgi?num=$num";
$html_link .="&file=summary_pathogenicity.txt" if (defined $type && $type eq 'pathogenicity');
my $detail_link=  (defined $tmp1 && $tmp1 ne '')? "/cgi-bin/change_detail_html.cgi?num=$num&tmp=$tmp1" : "/cgi-bin/change_detail_html.cgi?num=$num";
$detail_link.="&file=detail_pathogenicity.txt" if (defined $type && $type eq 'pathogenicity');
my $domain = "http://phast.wishartlab.com";
my $max_job_num = 10; # maximum number of jobs for each user to submit each time.
my $link_flash="$http_url#id=$num&redirect=$domain";
my $rna_link_flash = "/cgi-bin/view.cgi?id=$num&type=RNA&file=png_input_RNA";
$link_flash = (defined $type && $type eq 'pathogenicity')?  "/cgi-bin/view.cgi?id=$num&file=png_input_pathogenicity" : $link_flash;

my $queue_file = "$tmp_dir/queue.txt";

my @running_array = ( "$tmp_dir/running1.txt",  "$tmp_dir/running2.txt", "$tmp_dir/running3.txt");# remember to change it in phage.pl

my $running_cases='';
my $running_files = '';
for (my $i=0; $i < scalar(@running_array); $i++){
	if (-e $running_array[$i]){
		$running_cases .= `cat $running_array[$i]` ;
		$running_files .= $running_array[$i]."<br>";
	}
}
# check the prvious records first
my $count = 0;
if ($running_cases =~ /$num/s){
		$count=0;
}else{
		$count++;
}
my $cc = $count;
if (-e $queue_file && $count!=0){
	foreach my $l (split "\n", `cat $queue_file`){
		next if ($l =~ /^\s*$/);
		if ($l !~ /$num/){
			$count++;
		}else{
			last;
		}
	}
}

my $process_file = "$tmp_dir/$num/$num.process";
if ($multi eq '' or $multi <=1){
	$multi=1;
}
if ($multi==1){
	if ((-e "$tmp_dir/$num/success.txt") or (-e "$tmp_dir/$num/fail.txt")){
		$count=0;
	}
	if ($count !=0 ){
		# Print the web page with the results.
		my $time = $count * 4;
		my $end_t = $count * 7;
		my $mseg="There are still $count cases in the queue before your case. Please wait for about $time-$end_t  minutes.";
	
		&HTML_Header_results_refresh("PHAST Result");
		print <<HTML;
		$mseg<br><br>
		This page will automatically refresh until the calculation is complete.<br>
		If you wish to view your result later, please copy or save this link: <a href='$results_link'>http://phast.wishartlab.com$results_link</a> <br><br><br><br>
		<font size=2>
		<a href="../index.html">Return to main page</a>
		</font>
HTML
		&HTML_Footer_results;
		exit;
	}

	my $desc='';
	# Check whether the submission has finished processing.
	my $done_data = `cat $tmp_dir/$num/$num.done`;

	if ($done_data =~/^(.*?)\n/){
		$desc=$1;
	}
	$desc=~s/\n//;
	if(-e "$tmp_dir/$num/success.txt"){
		my $count =0;
		my $mesg ='';
		my $da="";

		if (-e "$tmp_dir/$num/summary.txt"){
			$da = "<a href=\"$detail_link\">Detailed file</a> &nbsp<br>";
			$count++;
		}
		if ($count!=0){
			$mesg = "<b>$desc</b><BR><BR>
			<table  border='1' cellpadding='10' cellspacing='1'> <tr><td> <a href=\"$html_link\"><IMG SRC=\"../image/thum1.png\" height=50 width=80></a></td>
				<td><a href=\"$html_link\">Summary result file</a></td>
				</tr>
			        <tr><td> <a href=\"$detail_link\"><IMG SRC=\"../image/thum2.png\" height=50 width=80></a></a></td>
				<td><a href=\"$detail_link\">Detailed file</a></td>
				</tr>
               		        <tr><td> <a href=\"$link_flash\"><IMG SRC=\"../image/thum3.png\" height=50 width=80></a></a></td>
				<td><a href=\"$link_flash\">FLASH image of the result (Flash player 11 needed, the latest verson 17.0.0.188 not work well in some browsers)</a></td>
				</tr>
                                <tr>
                                  <td><a href=\"$rna_link_flash\"><IMG SRC=\"../image/thum3.png\" height=50 width=80></a></a></td>
                                  <td><a href=\"$rna_link_flash\">RNA info</a></td>
                                </tr>
			        <tr><td> <a href=\"$result_url/$num/image.png\"><IMG SRC=\"../image/thum4.png\" height=50 width=80></a></a></td>
				<td><a href=\"$result_url/$num/image.png\">Image in PNG format</a></td>
				</tr>
		          	</table>
			";
			
		}else{
			$mesg = "No prophage region is found! <br><br>";
		}


		# Print the web page with the results.
		&HTML_Header_results("PHAST Result");
		print <<HTML;
		$mesg
		<br><br>
		Thank you for using our server.<br><br><br><br>
HTML
		&HTML_Footer_results;
	
	}	
	elsif (-e "$tmp_dir/$num/fail.txt"){
		&HTML_Header_results("PHAST Result");
		my $da = `cat $tmp_dir/$num/fail.txt`;
		print <<HTML;
		Our server cannot handle this case because :<br>
		$da <br><br><br><br><br>
		Thank you for using our server.<br><br><br><br>
HTML
		&HTML_Footer_results;
    
	}elsif(!-d "$tmp_dir/$num") {
		 &HTML_Header_results_refresh("PHAST Result");
		print <<HTML;
		There is no this case in our result database. Please resubmit your input again. 
HTML
	
		&HTML_Footer_results;
	
	}else {
		# Submission has not finished processing. Print "in progress" page.
		&HTML_Header_results_refresh("PHAST Result");
		my $process ='';
		if (-s $process_file){
			$process = `cat $process_file`;
		}else{
			$process = "Start running ......";
		}
		print <<HTML;
		Query submitted.<br><br>
		Your query is being processed. This usually takes 2-10 minutes based on the length of the DNA sequence. A 5 Mb genome takes about 6-7 minutes.<br><br>
		<pre>$process</pre><br>
		This page will automatically refresh until the calculation is complete.<br>
		If you wish to view your result later, please copy or save this link: <a href='$results_link'>http://phast.wishartlab.com$results_link</a> <br><br><br><br>
HTML
		&HTML_Footer_results;
	
	}
}else{#$multi >1
	my $bunch_result='';
	my $flag = 0;
	if ($multi >$max_job_num){
		$multi = $max_job_num;
		$flag = 1;
	}
	$results_link=~s/&multi.*//;
	foreach my $c (1..$multi){
		$bunch_result .= "<a href=\"$results_link\_$c\">http://phast.wishartlab.com$results_link\_$c</a><br>";
	}
	my $notice = '';
	if ($flag ==1){
		$notice = "You jobs number is over $max_job_num. For being fair to other users, the server will handle first $max_job_num jobs. Please try to submit the rest of your case later. <br><br>";
	}
	&HTML_Header_results;
	print <<HTML;
	$notice 
	Please check these results later. Or please keep all these links in your local computer<br><br>
	$bunch_result <br><br>
	Thanks for using our server.
HTML
	&HTML_Footer_results;
}
exit;




sub HTML_Header_results
{
	print "content-type: text/html","\n\n";
	print <<HTML
	<HTML>
  	<HEAD>
  	<TITLE> </TITLE>
  	</HEAD>
	<BODY BGCOLOR="white">
	<font face="Arial">
	<CENTER>
	<IMG SRC="../image/phage.png" height=125 width=500><BR><BR>
	</Center>
	<center>
	<table align="center"  border="0" cellpadding="0" cellspacing="0" id="navigation" bgcolor='orange'>
        <tr width="100%" style="font-size:19;font-style:arial;">
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../index.html"><b>Home</b></a>&nbsp;&nbsp;&nbsp;</td>
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../documentation.html"  ><b>Documentation</b></a>&nbsp;&nbsp;&nbsp;</td>
	<td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../io.html"  ><b>Input</b></a>&nbsp;&nbsp;&nbsp;</td>
	<td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../io1.html"  ><b>Output</b></a>&nbsp;&nbsp;&nbsp;</td>
	  <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../how_to_use.html"  ><b>Instructions</b></a>&nbsp;&nbsp;&nbsp;</td>
	  <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../Download.html"  ><b>Databases</b></a>&nbsp;&nbsp;&nbsp;</td>
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../contact.html"  ><b>Contact</b></a>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        </table>
</center>
	<BR><BR>
	<P ALIGN="LEFT">
		<IMG SRC="../image/blueline.gif" WIDTH="1240" HEIGHT="7"
		 SGI_SRC="../image/blueline.gif">
	</P>
	</font>
  	<BODY>
HTML
}

sub HTML_Header_results_refresh
{
  print "content-type: text/html","\n\n";
  print "<HTML>\n";
  print "<HEAD>\n";
  print "<TITLE>","@_","</TITLE>\n";
  print '<meta http-equiv="refresh" content="5" />' . "\n"; # Refresh page every 30 seconds.
  print "</HEAD>\n";
  print "<BODY>\n";
  print <<HTML

	<font face="Arial">
	<center>
	<IMG SRC="../image/phage.png" height=125 width=500><br><br>
	</center>
	<center>
	<table align="center"  border="0" cellpadding="0" cellspacing="0" id="navigation" bgcolor='orange'>
        <tr width="100%" style="font-size:19;font-style:arial;">
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../index.html"><b>Home</b></a>&nbsp;&nbsp;&nbsp;</td>
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../documentation.html"  ><b>Documentation</b></a>&nbsp;&nbsp;&nbsp;</td>
	<td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../io.html"  ><b>Input</b></a>&nbsp;&nbsp;&nbsp;</td>
	<td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../io1.html"  ><b>Output</b></a>&nbsp;&nbsp;&nbsp;</td>
	  <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../how_to_use.html"  ><b>Instructions</b></a>&nbsp;&nbsp;&nbsp;</td>
	 <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../Download.html"  ><b>Databases</b></a>&nbsp;&nbsp;&nbsp;</td>
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../contact.html"  ><b>Contact</b></a>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        </table>
	</center>
	</font>
	<br><br>
	<P ALIGN="LEFT">
		<IMG SRC="../image/blueline.gif" WIDTH="1240" HEIGHT="7"
		 SGI_SRC="../image/blueline.gif">
	</P>
HTML
}

sub HTML_Footer_results
{
	print <<HTML

	<font face="Arial" size=2>
	<table align="center">
	<tr>
	<td>

  <div style="width: 500px; align: centre; margin: 5px; padding: 10px 40px 10px 40px; background-color: #ffe9ad; box-shadow: 5px 5px 3px #888888;">
		<table align="center"">
		<tr>
			<td style="background-color: #ff9621; padding: 10px; border: 2px solid white;">
				<img src="../image/phaster_phage_logo_white.png" style="width: 50px;">
			</td>
			<td style="padding: 0px 0px 0px 10px;">
					<p>
					<a href='http://phaster.ca' target='_blank' style='text-decoration: none;'>PHASTER</a> is the updated version of PHAST.
					It uses a more up-to-date database, has access to more computing resources, and is faster.
					</p>
				</td>
			</tr>
		</table>
	</div>

	</td>
	</tr>
	</table>
	</font>

	<font size=2>
	<P ALIGN="LEFT">
		<IMG SRC="../image/blueline.gif" WIDTH="1240" HEIGHT="7"
		 SGI_SRC="../image/blueline.gif">
	<br/>
	Problems? Suggestions? Please use our <a href="http://feedback.wishartlab.com?site=PHAST" target="_blank">feedback form</a>
	</P>
	</font>

	</font>
	</BODY>
  	</HTML>
HTML
}
