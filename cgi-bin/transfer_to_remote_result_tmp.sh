#!/bin/bash

dir=$1
num=$2
host="$HTTP_PHAST_CLUSTER_USERNAME@$HTTP_PHAST_CLUSTER_HOSTNAME"

ssh -i ~/.ssh/scp-key $host 'perl /home/prion/phage/cgi/cleanup.pl /home/prion/phage/result_tmp'
ssh -i ~/.ssh/scp-key $host 'perl /home/prion/phage/cgi/cleanup_tmp.pl /home/prion/phage/tmp'
scp -i ~/.ssh/scp-key  -r  $dir  $host:phage/result_tmp/.
#if  ! [ $num =~ '^1' ] ; then
#	ssh -i ~/.ssh/scp-key $host "perl /home/prion/phage/cgi/transfer_to_botha_w6.pl $num"
#fi
