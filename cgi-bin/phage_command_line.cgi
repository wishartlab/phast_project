#!/usr/bin/perl -w

# this program will initiate all the sub programs to finish 
# phage finder and filter's jobs.

use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $BASE_ROOT="/var/www/html/phast/current/public";
my $tmp_dir = "$BASE_ROOT/tmp"; # this dir will cotain all the temp files and result files.
my $exec_dir = "$BASE_ROOT/cgi-bin";  # dir of executables.

my $q=new CGI;
my $acc = $q->param('acc'); #access number or gi number
my $gbk = $q->param('gbk_file');
my $filename=$q->param('fasta_file');
my $fasta_seq = $q->param("fasta_seq");
$fasta_seq =~s/[\r\n]//gs;
my $strict_mode = '';
$strict_mode = $q->param("strict");
my $queue_file = "$tmp_dir/queue_command_line.txt";
my $client_IP = $ENV{'REMOTE_ADDR'};
my $SEQ_LEN_LIMIT = 100000; #0.1M basepair.
$acc=~s/\s//g;
$filename=~s/\s//g;
my $user_input ='';
my $f;
if ($gbk ne ''){
	$f+=1;
	$user_input = $gbk;
}
if ($acc ne ''){
	$f+=1;
	$user_input = $acc;
}
if ($filename ne ''){
	$f+=1;
	$user_input = $filename;
}
if ($fasta_seq ne''){
	$f++;
	$user_input = 'Fasta sequence';
}

&check_input($f);

 
my ($num, $flag);

if ($gbk ne ''){# gbk file submitted
	$num = time;
	$flag = '-g';
}
elsif ($acc ne ''){# gi number or access number
	$num = $acc;
	$flag = '-a';
	my $acc_cp = $acc;
        $acc_cp =~s/[A-Z]//ig;
        $acc_cp =~s/\d//g;
        $acc_cp =~s/[\.\_]//g;
        if ($acc_cp ne ''){
                HTML_error("That is not accession number or GI number. Please check!", '');
        }

}else{# fasta sequece or fasta sequence file.
	$num=time;
	$flag = '-s';
}
#check if the cases exist already, if yes, show the result , if not, do the rest.
my $found = 0;
my $num1='';
my $num2='';
if (-e "$tmp_dir/list"){
	my $list = `cat $tmp_dir/list`;
	my @list_lines = split("\n", $list);
	foreach (@list_lines){
		if ($_=~/^\s*$/){#empty line
			next;
		}
		my @temp=split(" ", $_);
		$num1 = $temp[0]; 
		$num2 = $temp[1];
		if($num1 eq $num or $num2 eq $num){
			$found = 1;
			last;
		}

	}
}
if ($found ==1){
	if (-d "$tmp_dir/$num1"){
		$num = $num1;
	}elsif( -d "$tmp_dir/$num2"){
		$num = $num2;
	}
}
print "content-type: text/html \n\n";
my $warning = "Warning: if you submit contig files through command line submission,".
		      " please concatenate all the contigs to make a single genome first,".
		      " then submit the genome. It will save you a lot of time. Command line mode accepts eqaul and longer than $SEQ_LEN_LIMIT base pair genomes only. If less than that number, your case will be terminated. For small genomes or contigs, please use home page.<br/><br/>\n";
my $links = "\nhttp://phast.wishartlab.com/cgi-bin/Results.cgi?num=$num<br/>\n".
		"http://phast.wishartlab.com/tmp/$num/summary.txt<br>\n".
			"http://phast.wishartlab.com/tmp/$num/detail.txt<br>\n".
			"http://phast.wishartlab.com/tmp/$num/image.png<br>\n";
if (-e "$tmp_dir/$num/success.txt"){
	print $warning;
	print "Result is ready for  $user_input. Please check the following files:<br>";
	print $links;
}elsif(-e "$tmp_dir/$num/fail.txt"){
	print $warning;
	print "Query failed for $user_input.<br>" ;
	my $err = `cat $tmp_dir/$num/fail.txt`;
	print "$err<br>";
}elsif((-d "$tmp_dir/$num") && !(-e "$tmp_dir/$num/success.txt") && !(-e "$tmp_dir/$num/fail.txt")) {
	print $warning;
	print "The query is running for $user_input. Please wait for a few minutes.<br>" ;
	print "Please check the following files after the time.<br>";
	print $links;
}else{# case is not there yet
	# Fork the process so that the child process performs the structure prediction
	# and the parent process prints a web page for the user.

	$SIG{CHLD}='IGNORE';
		# Ignore child process (so page will be displayed before child process
		# finishes).
	my $pid = fork();
	if ($pid == 0) {
		# I'm the child process.
		my $log_file ="$tmp_dir/$num/$num.log";
		system("rm -rf $tmp_dir/$num; mkdir -p $tmp_dir/$num");
		system("echo 'mkdir -p $tmp_dir/$num\n' 2>&1 |cat >> $log_file");

		if ($flag eq "-g"){
			&GetRemoteGBK($gbk,$num); # get gbk file from user's computer.
		}elsif ($flag eq "-s"){
			if(length($filename)>0) {  
		  		&GetRemote($filename,$num);
			}
			elsif (length($fasta_seq) > 0) {
				#	 $data =~s/\r//gs;
				open(OUT,">$tmp_dir/$num/$num.fna") or die "Cannot write $tmp_dir/$num/$num.fna";
				my @tmp = $fasta_seq =~/(\w{0,60})/g;
				print OUT join("\n", @tmp);
				close OUT;
			  	
			}
			if (length($fasta_seq) < $SEQ_LEN_LIMIT){
				open(OUT, ">$tmp_dir/$num/fail.txt") or die "Cannot write $tmp_dir/$num/fail.txt";
				print OUT 	"Your sequence is shorter than $SEQ_LEN_LIMIT base pairs. Our command line program cannot handle your case. Please make sure the sequence submitted is longer than $SEQ_LEN_LIMIT base pairs. \n";
				close OUT;
				exit(0);
			}
			open (IN, "$tmp_dir/$num/$num.fna");
			open (OUT, ">$tmp_dir/$num/$num.fna.tmp");
			my $line;
			my $data='';
			my $flg = 0;
			while(<IN>){
				if ($_=~/>(.*)/){
					$line=$1;	
					$line=~s/gi.*\|//s;
					if ($line eq '') {
						$line = "Genome; Raw sequence";
					}
					$data .= ">gi|00000000|ref|NC_000000|$line\n";
					$flg = 1;
				}else{
					$data .= $_;
				}
			}
			close IN;
			if ($flg ==0){ # no > line , so
				 $data = ">gi|00000000|ref|NC_000000| Genome; Raw sequence\n".$data;
			}
			print OUT $data;
			close OUT;
			system("mv -f $tmp_dir/$num/$num.fna.tmp  $tmp_dir/$num/$num.fna");
		}
		chdir $tmp_dir;
		# now add the new case to the queue file, the information addedd is the arguments for the phage.pl
		system("echo  'write $flag   $num to $queue_file' >> $log_file");
		system("echo '$flag  $num' >> $queue_file");
		system("echo  'chmod 777 $queue_file' >> $log_file");
		system("chmod 777 $queue_file");
		system("echo  'chmod -R 777 $tmp_dir/$num' >> $log_file");
		system("chmod -R 777 $tmp_dir/$num");
		my $date = `date`;
		system("echo '$num  $client_IP  $date' >> $tmp_dir/case_input.log");
		system("chmod 777 $tmp_dir/case_input.log");
		exit(0);
	}else{
		# I'm the parent process.
		print $warning;
		print "The query for $user_input is submitted to queue. When the query is completed depends on the server load.<br>";
		print "Normally your case will finish in 5 minutes to one hour.<br>";
		print "Please check the following files for your case after the time.<br>";
		print $links;
	}
		
}


exit;

# cleanup some unnessisary files
sub cleanup{
	system("rm -rf *.tab *.out *.seq* *.con $tmp_dir*.pep");
	system("rm -rf ../combined.hmm*  ../FRAG_HMM_searches_dir/  ../GLOCAL_HMM_searches_dir ../error.log  ../formatdb.log   ../ncbi.out ../phage_finder_info.txt  ../tmRNA_aragorn.out ../tRNAscan.out");
}

sub HTML_error{
	my $msg=shift;
	&HTML_Header_results;
	print "$msg";
	&HTML_Footer_results;
	exit(0);
	
}

sub HTML_Page
{
	my $num=shift;
	my $results_link = "Results.cgi?num=$num";
	print "content-type: text/html \n\n";
		
	my $inter_cont = "Loading the file now. This will take 2-20 seconds. Once completed, calculation will be conducted...
		<br/><br/>
		You will automatically be redirected to your <a href=\"$results_link\">results/processing</a> page...<br>
		<br><br><br><br><br>";
	my $fresh_time = 5;
	
	
	print <<HTML

	
		<HTML>
		<HEAD>
		<TITLE>PHAST results</TITLE>
		<meta http-equiv="REFRESH" content="$fresh_time;url=$results_link">
		</HEAD>
		<BODY BGCOLOR="white">
		<CENTER>
		<IMG SRC="http://184.73.211.12/phage/image/phage.png" height=125 width=500>
		</CENTER>
		<LEFT>
		<br><br><br><br><br>
		<P ALIGN="LEFT">
			<IMG SRC="http://184.73.211.12/phage/image/blueline.gif" WIDTH="540" HEIGHT="7"
			 SGI_SRC="$BASE_ROOT/image/blueline.gif">
		</P>
		$inter_cont
		<P ALIGN="LEFT">
		<IMG SRC="http://184.73.211.12/phage/image/blueline.gif" WIDTH=540 HEIGHT=7
			SGI_SRC=\"$BASE_ROOT/image/blueline.gif\">
		</P>
		<font size=2>
		Please report bugs and send your comments using our
	    	<a href="http://feedback.wishartlab.com?site=PHAST" target="_blank">feedback form</a>
	    	</font>
		</LEFT>
	  	</BODY>
	  	</HTML>
HTML

	
}
# get gbk file for user's hard disk
sub GetRemoteGBK
{
   	my $infile =shift;
  	 my $num = shift;
   	#print $input;
   	binmode $infile;

   	open(LF,">$tmp_dir/$num/$num.gbk");
   	binmode LF;
   	print LF <$infile>;
   	close(LF);

}

sub GetRemote
{
   	my $infile =shift;
  	 my $num = shift;
   	#print $input;
   	binmode $infile;

   	open(LF,">$tmp_dir/$num/$num.fna");
   	binmode LF;
   	print LF <$infile>;
   	close(LF);

}

sub check_file{
	my $f = shift;
	$data = `cat $tmp_dir/$num/$f.gbk`;
	$data =~/\nORIGIN(.*)\/\//s;
	$data = $1;
	$data =~s/[\s\n\datgc]//si;
	if (length($data)!=0){
		print "<p> Your input file is not a DNA genome file. Please check! </P>\n";
	   	exit(1); 
	}
}

sub check_input{
	my $flag=shift;
	if ($flag !=1){
   		HTML_error("Error: Only one of accession number, GBK formatted file, or your FASTA sequence file can be input.");
	}
}

sub get_gbk_file{
	my ($num, $flag, $log_file, $tmp_dir)= @_;
	
	system("perl $exec_dir/get_gbk.pl $num $flag $tmp_dir > $log_file.2"); # create $num.gbk file
	system("cat $log_file.2 >> $log_file; rm -rf $log_file.2");
	print_msg("$num.gbk", $log_file);
	if (!(-s "$num.gbk")) {
	   	system("echo $exec_dir : $num : $flag Cannot get back gkb file from Genbank! The case is terminated! >> $tmp_dir/$num/fail.txt");
	   	exit(1);
	}
	
}
# chech the list file if the number is there.
sub check_list_file{
	my $num=shift;
	my $data = `cat $tmp_dir/list`;
	my $found = 0;
	if ($data =~/$num/s){
		$found = 1;
	}
	return $found;
}

sub print_msg{
	my ($file, $log_file)= @_;
	if (-s $file ){
		system("echo  $file is created ! >> $log_file");
	}else{
		system("echo  $file is not created ! >> $log_file");
	}
}

sub HTML_Footer_results
{
	print <<HTML
	<br/><br/><br/><br/><br/><br/>
	<P ALIGN="LEFT">
		<IMG SRC="http://184.73.211.12/phage/image/blueline.gif" WIDTH="540" HEIGHT="7"
		 SGI_SRC="$BASE_ROOT/image/blueline.gif">
	<br/>
	Problems? Suggestions? Please use our <a href="http://feedback.wishartlab.com?site=PHAST" target="_blank">feedback form</a>
	</P>
	
	</font>
	</BODY>
  	</HTML>
HTML
}

sub HTML_Header_results
{
	print "content-type: text/html","\n\n";
	print <<HTML
	<HTML>
  	<HEAD>
  	<TITLE>hage Lookup results </TITLE>
  	</HEAD>
	<BODY BGCOLOR="white">
	<CENTER>
	<IMG SRC="http://184.73.211.12/phage/image/phage.png" height=125 width=500>
	</Center>
	<BR><BR><BR><BR><BR>
	<P ALIGN="LEFT">
		<IMG SRC="http://184.73.211.12/phage/image/blueline.gif" WIDTH="540" HEIGHT="7"
		 SGI_SRC="$BASE_ROOT/image/blueline.gif">
	</P>
  	<BODY>
HTML
}

