#!/usr/bin/perl
package Att;
# script scans genome for attachment site
sub new
{
    my $class = shift;
    my $self = {
    	_nt_file => shift,
		_tRNA_file => shift,
		_tmRNA_file => shift,
		_tRNA=> "",
		_tmRNA=> "",
		_seq => "",
		_header => "",
		_gc => 0
    };
	# read nucleotide file
	my $seq = "";
	open (R, $self->{_nt_file}) or die "cannot load nt file ",  $self->{_nt_file};
	while (my $line =<R>){
		if ($line =~m/^>/){
			chomp ($line);
			$line =~m/^>(.+)\s*$/;
			$self->{_header} = $1;
			next;
		}
		if ($line =~m/(\w+)/){
			$seq = $seq.$1;
		}
	}
	$self->{_seq} = $seq;
	close (R);
	my $matches = 0;
	while ($seq=~m/C/g){
		$matches++;
	}
	$matches *= 2;
	$self->{_gc} = sprintf "%4.2f\%", $matches * 100 / length($seq);

	# read tmRNA file
	my @tm = ();
	open (R, $self->{_tmRNA_file}) or die "cannot load tmRNA file ",  $self->{_tmRNA_file};
	while (my $line=<R>){
		if ($line=~m/Location c\[(\d+),(\d+)\]/){
			my $entry = {_from=>$1, _to=>$2};
			push @tm, $entry;
		}
	}
	$self->{_tmRNA} = \@tm;
	close (R);
	
	# read tRNA file
	my @t = ();
    open (R, $self->{_tRNA_file}) or die "cannot load tmRNA file ",  $self->{_tmRNA_file};
	while (my $line=<R>){
		chomp($line);
		if ($line=~m/gi\|(\d+)\|\S+\s+\d+\s+(\d+)\s+(\d+)\s+(\S+)\s+(\S+).+\s(\S+)\s*$/){
#			print "$1 $2 $3 $4 $5 $6\n";
			my $entry = {_gi=>$1, _from=>$2, _to=>$3, _type=>$4, _codon=>$5, _cove=>$6};
			push @t, $entry;
		}
	}
	$self->{_tRNA} = \@t;
	close (R);
	bless $self, $class;
    return $self;
}
sub gc_rate {
	my ($self, $from, $len) = @_;
	my $target = substr($self->{_seq}, $from-1, $len);
	my $matches = 0;
	while ($target=~m/C/g){
		$matches++;
	}
	$matches *= 2;
	return sprintf "%4.2f\%", $matches * 100 / $len;
}
sub tmRNA{
	my ($self) = @_;
	return $self->{_tmRNA};
}
sub tRNA {
	my ($self) = @_;
	return $self->{_tRNA};
}
sub total_len {
	my ($self) = @_;
	return length ($self->{_seq});
}
sub print_RNA {
	my ($self) = @_;
	my $rna = $self->{_tmRNA};
	foreach (@$rna){
		print "from ", %$_->{_from}," to ", %$_->{_to},"\n";
	}
	$rna = $self->{_tRNA};
	foreach (@$rna){
		print sprintf ("gi %8d from %8d to %8d type %5s codon %5s\n", %$_->{_gi}, %$_->{_from}, %$_->{_to},
		%$_->{_type}, %$_->{_codon}, %$_->{_cover});
	}
}
sub att_scan {
	my ($self, $l1, $l2, $r1, $r2, $minlen) = @_;
	my %hash = ();
	my $i;
	my @pairs;
	my $lastpair = "";
#	print "sequence read = ", length ($self->{_seq}),"\n";
	my $key, $value;
	for ($i = $l1; $i <= $l2-$minlen+1; $i++){
		$key = substr($self->{_seq}, $i-1, $minlen);
		$value = $i;
		$hash{$key} = $value;
	}
	for ($i = $r1; $i <= $r2-$minlen+1; $i++){
		$key = substr($self->{_seq}, $i-1, $minlen);
		$value = $i;
		if (exists $hash{$key}){
#			print sprintf("found (%8d, %8d) '%s'\n", $hash{$key}, $value, $key);
			if ($lastpair eq ""){
				$lastpair = {_left=>$hash{$key},
					_right=>$value,
					_size=>$minlen,
					_core=>$key};
			}
			elsif ($hash{$key} - $lastpair->{_left} == $lastpair->{_size} - $minlen+1){
				$lastpair->{_core} = $lastpair->{_core}.substr($key, -1);
				$lastpair->{_size} += 1;
			}
			else{
				push (@pairs, $lastpair);
				$lastpair = {_left=>$hash{$key},
					_right=>$value,
					_size=>$minlen,
					_core=>$key};
			}
		}
	}
	if ($lastpair ne ""){
		push (@pairs, $lastpair);
	}
	foreach (@pairs){
#		print $_->{_left}, " ", $_->{_right}, " ", $_->{_size}, " ", $_->{_core}, "\n";
	}
	return \@pairs;
}

sub print_seq {
	my ($self, $from, $len) = @_;
	if ($from < 0 or $from+$len-1 > length($self->{_seq})){
		print "excessed sequence boundary\n";
		return 0;
	}
	print ">";
	print substr($self->{_seq}, $from-1, $len);
	print "< (from $from, $len nts)\n";
}

sub scan_print {
	my ($self, $seq);
	my $i = 0;
	while ($self->{_seq} =~m/$seq/g){
		print sprintf ("%4d  [%8d, %8d]\n", $-[0], $+[0]);
	}
}
1;
