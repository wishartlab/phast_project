#!/usr/bin/perl -w

use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $sub_program_dir= "/var/www/html/phast/current/public/sub_programs";
my $base_root="/var/www/html/phast/current/public";
my $q = new CGI;
my $dir = $q->param('num');
my $num = $q->param("number");
my $str= `cat $base_root/tmp/$dir/region_DNA.txt`;
my $DNA_str='NA';
if ($str=~/(>$num.*?\n\n)/s){
	$DNA_str=$1;
}
$DNA_str=~s/\n/<br>/gs;
print "content-type: text/html \n\n";
print $DNA_str;
exit;
