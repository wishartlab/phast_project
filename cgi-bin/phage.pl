#!/usr/bin/perl -w

# this program will initiate all the sub programs to finish 
# phage finder and filter's jobs.
use Cwd;
use Mail::Sendmail;


my $mirror=0; #0 is on main server. 1 is on botha1 VM

my $sub_program_dir='';
my $BASE_ROOT="";
my $tmp_dir = "";
my $exec_dir="";

if ($mirror==1){#on botha1 VM
        $ENV{'PATH'} .=":/usr/local/bin:/home/prion/bin";
        $sub_program_dir= "/cs23d/PHAST/sub_programs";
        $BASE_ROOT="/var/www/html/PHAST";
        $tmp_dir = "$BASE_ROOT/tmp"; # this dir will cotain all the temp files and result files.
        $exec_dir="/var/www/cgi-bin/PHAST";
}else{ # on main server phast@phast.wishartlab.com
        $ENV{'PATH'} .=":/usr/local/bin:/apps/phast/bin";
        $BASE_ROOT="/var/www/html/phast/current/public";
		$sub_program_dir= "$BASE_ROOT/sub_programs";
        $tmp_dir = "$BASE_ROOT/tmp"; # this dir will cotain all the temp files and result files.
        $exec_dir = "$BASE_ROOT/cgi-bin";  # dir of executables.
}
our $CURRENT_PID = $$;
our $cleanup_days = 3; # cleanup all files in these days back from now.
our $no_space_cleanup_days = 2; #cleanup all files in these days back from now for no space emergency.
my $change_to_protein_exec =  $exec_dir."/change_to_protein_seq.pl";
my $change_to_ptt_exec = $exec_dir."/change_to_ptt_format.pl";
# my $nr_database = "$BASE_ROOT/databases/nrfilt";

my $bac_database="bacteria_all_select.db"; # on the cluster side
my $bac_header_database="bacteria_all_select_header_lines.db";
my $cluster_side_bac_database = "/home/prion/phage/DB/$bac_database";
my $local_bac_database = "$sub_program_dir/phage_finder/DB/$bac_database";
my $local_bac_header_database = "$sub_program_dir/phage_finder/DB/$bac_header_database";
my $virus_database="prophage_virus.db"; # virus db name
my $virus_header_database= "prophage_virus_header_lines.db";
my $cluster_side_virus_database = "/home/prion/phage/DB/$virus_database";
my $local_virus_database = "$sub_program_dir/phage_finder/DB/$virus_database";
my $local_virus_header_database = "$sub_program_dir/phage_finder/DB/$virus_header_database";
my $email_receiver="phast\@wishartlab.com";# when cluster side not working, the receiver of the mail
my $email_sender="phast\@phast.wishartlab.com";#when cluster side not working, the sender of the mail
my $CLUSTER_DISK = $ENV{HTTP_PHAST_CLUSTER_DISK};

my $HOSTNAME = $ENV{HTTP_PHAST_CLUSTER_HOSTNAME};
my $USERNAME = $ENV{HTTP_PHAST_CLUSTER_USERNAME};
my $host = $USERNAME . '@' . $HOSTNAME;
my $ssh_key = "~/.ssh/scp-key";

my $start_time = time;
my $time;

my $queue_file = "$tmp_dir/queue.txt";
my @running_array = ( "$tmp_dir/running1.txt",  "$tmp_dir/running2.txt", "$tmp_dir/running3.txt"); # if update this array, update it in Results.cgi either

my $last_case_tolerate_time=180 ; #min
my $Phage_Finder_tolerate_time=180 ; #min
my $tRNAscan_tolerate_time=60 ;#min
our $running_case_file = find_running_case_file(\@running_array);

if ( $running_case_file eq '' ){
	# stop, all cases are running
	print "all running files are running!\n";
	system("kill -9 $CURRENT_PID");
	exit;
}
my $first_record='';
my $rest='';
my $ff = 1;
open (IN, $queue_file) or die "Cannot open $queue_file";
while(<IN>){
	if ($ff==1){
		$first_record=$_;
		chomp($first_record);
		$ff=0;
	}else{
		$rest .=$_;
	}
}
close IN;
if ($first_record eq ''){
	#nothing to do
	exit;
}
if ($rest ne '' && $rest ne "\n" ){
	open(OUT, "> $queue_file");
	print OUT $rest;
	close OUT;
	system("chmod 777 $queue_file");
}else{
	system("rm $queue_file; touch $queue_file; chmod 777 $queue_file");
}

system("echo '$first_record' > $running_case_file");
print "$first_record\n";
my @arr=split(" ", $first_record);
my $num = $arr[1];
my $flag = $arr[0];
my $log_file="$tmp_dir/$num/$num.log";
my $ti = `date`; $ti=~s/\n//;
system("echo '\nCase start running ,time=$ti' >> $log_file");
if (!-d "$tmp_dir/$num"){
        system("echo 'phage.cgi cannot  make $tmp_dir/$num' >> $log_file");
        system("mkdir $tmp_dir/$num");
	if(!-d "$tmp_dir/$num"){
	#sometimes there is over the file limit in a folder. it is ext2 format file system. folder in a dir cannot be over 320000.
		my $cm1 = "perl $exec_dir/cleanup.pl  $tmp_dir $no_space_cleanup_days";
    		system("echo '$cm1' >> $log_file");	
		system($cm1);
		system("mkdir $tmp_dir/$num");			
	}
}
system("echo 'Case $num is running. $running_case_file is generated!' >>$log_file" );
unlink "$tmp_dir/$num/fail.txt" if (-e "$tmp_dir/$num/fail.txt");
unlink "$tmp_dir/$num/success.txt" if (-e "$tmp_dir/$num/success.txt");
unlink "$tmp_dir/$num/$num.done" if (-e "$tmp_dir/$num/$num.done");

my $process_file ="$tmp_dir/$num/$num.process";
my $process='';
my $process_fail='';

chdir "$tmp_dir/$num";
my $cluster_dir="/home/prion/phage/tmp/$num";
system("$exec_dir/create_remote_dir.sh  $cluster_dir   2>&1 |cat >> $log_file");

my $cluster_sp = `ssh -i $ssh_key $host 'df -h' `;
if ($cluster_sp =~/(\d+)% $CLUSTER_DISK/s){
	my $perc= $1;
	if ($perc ==100){
		$process= "The cluster side hard drive is $perc percent full!";
		$process_fail= "The cluster side hard drive is $perc percent full! There is no space left in the cluster. Please inform the administrator.\n";
		check_success_fail_exit($num, "NA_cluster_hard_drive_full", "$process\n", $process_fail, $process);	
	}
}elsif(!defined $cluster_sp or $cluster_sp eq ''){
	$process= "The cluster side of the back end is not connected!";
    $process_fail= "The cluster side is not connected. Please inform the administrator.\n";
	check_success_fail_exit($num, "NA_cluster_not_connect", "$process\n", $process_fail, $process); 
}

if($flag eq '-a'){# we have gi number or accession number or GBK file
		if (!(-s "$num.gbk")){
			&get_gbk_file($num, $log_file, $tmp_dir );
			$process = "Genbank file has been acquired!\n";
			$process_fail = "Genbank file has not been acquired! Case failed!\n";
			check_success_fail_exit($num, "$num.gbk", $process, $process_fail, "Cannot get back the GBK file of $num  from Genbank! <br/>This case is terminated!");
		}else{
			system("echo '$num.gbk exist!' >> $log_file");
		}		
}elsif($flag eq '-g') {# we have GBK file
		system("echo '$num.gbk from Input!' >> $log_file");
}

if (-s "$num.gbk"){
	my $has_translation = check_gbk_file("$num.gbk", $num);
	if ($has_translation ==0){
		$flag = '-s';
		system("echo '$num.gbk has no translation area, change it into DNA sequence only, flag=-s' >> $log_file");
	}
}


if ($flag eq '-s'){ # have fasta seq , use glimmer to predict ORF and create .ppt .fna .faa files;
	 if (!-s "$num.fna"){
                 $process=  "There is no DNA sequence in .fna file. Program terminated!\n";
                 $process_fail=$process;
                 check_success_fail_exit($num, "NA_no_seq_in_fna_file", $process, $process_fail, $process_fail);
        }
	#rewrite fna file, if not seq in multiple lines, tRNAscan will get trouble.
        open(IN, "$num.fna");                                                  
        my $head = ''; my $seq_tmp = '';
        while(<IN>){
                if ($_=~/>/){
                        $head = $_;
                }else{
                        chomp($_);
                        $seq_tmp .= uc($_);
                }       
        }
        close IN;
        my @tmp = $seq_tmp =~/(\w{0,60})/g; 
        open(OUT, ">$num.fna");
        print OUT $head;
        print OUT join("\n", @tmp);
        close OUT; 
	system("echo XXXXX running glimmer XXXXXX >> $log_file");
	my $start_glimmer_time = time;
	$time = $start_glimmer_time - $start_time;
	system("echo 'Start Glimmer at $time sec' >> $log_file");

	$process="Glimmer-3.02 is running......\n";
	write_process_file($process_file,$process );

	system("echo XXXXX parallel running glimmer XXXXXX >> $log_file");
	my $glimmer_dir = "$cluster_dir/glimmer";
	my $cm = "$exec_dir/call_glimmer_parallel.sh  $glimmer_dir  $num";
	system("echo '$cm' >> $log_file");
	system("$cm   2>&1 |cat >> $log_file");
	
	system("echo 'XXXXX running glimmer finished XXXXXX\n' >> $log_file");
	my $end_glimmer_time = time;
	my $glimmer_time = $end_glimmer_time - $start_glimmer_time;
	system("echo 'Glimmer run time = $glimmer_time sec' >> $log_file");
	$process_fail = "Glimmer does not work!\n";
	$process = "Running Glimmer-3.02 is done!\n";
	check_success_fail_exit($num, "$num.predict", $process, $process_fail, "Glimmer does not work! This case is terminated!");
	
	my $ppt_start_time = time;
	write_process_file($process_file, "Generating ptt file....\n");
	$cm = "perl $change_to_ptt_exec $num.fna $num.predict  $num.ptt";
	system("echo '$cm' >> $log_file");
	system("$cm");
	$process_fail = "Ptt file is not generated! Case failed!\n";
	$process = "Ptt file has been generated!\n";
	check_success_fail_exit($num, "$num.ptt", $process, $process_fail, "No CDS position is detected! Please check your gbk file or FASTA sequence file! <br/>This case is terminated! <br/>You can try FASTA file if the file you submitted is in gbk format.");
	my $ptt_end_time = time;
	$time = $ptt_end_time - $ppt_start_time;
	system("echo 'Generating ppt file took $time sec' >> $log_file");

	my $faa_start_time = time;
	write_process_file($process_file, "Generating faa file......\n");
	system("perl $change_to_protein_exec $num.fna $num.predict $num.faa");
	$process_fail = "Faa file is not generated! Case failed!\n";
	$process = "Faa file has been generated!\n";
	check_success_fail_exit($num, "$num.faa", $process, $process_fail, "No CDS position is detected! Please check your gbk file or FASTA sequence file! <br/>This case is terminated! <br/>You can try FASTA file if the file you submitted is in gbk format.");
	my $faa_end_time = time;
        $time = $faa_end_time - $faa_start_time;
        system("echo 'Generating faa file took $time sec' >> $log_file");
	
}else{ # we have gi number or accession number or GBK file
	

	write_process_file($process_file, "Generating fna file....\n");
	if (!(-s "$num.fna")){
		my $comm = "perl $exec_dir/gbk2fna.pl $num.gbk ";
		system("echo '$comm' >> $log_file");
		system($comm) ; #create $num.fna file
		$process_fail = "Fna file has not been generated! Case failed!\n";
		$process = "Fna file has been generated!\n";
		check_success_fail_exit($num, "$num.fna", $process, $process_fail, "No nucleotide sequence is detected! Please check your gbk file or FASTA sequence file! <br/>This case is terminated!");
	}else{
		system("echo '$num.fna exist!' >> $log_file");
	}
	

	write_process_file($process_file, "Generating ptt file....\n");
	if (!(-s "$num.ptt")){
		my $comm = "perl $exec_dir/gbk2ptt.pl $num.gbk > $num.ptt";
		system("echo '$comm' >> $log_file");
		system($comm);
		if (! -s "$num.ptt"){
			# sometimes Bio::SeqIO does not work
			system("echo 'No $num.ptt file generated locally! turn to remote.' >> $log_file");
			$comm = "scp -i $ssh_key $num.gbk $host:/tmp/.";
			system("echp '$comm' >> $log_file ");
			system($comm);
			$comm = "ssh -i $ssh_key $host \"perl  /home/prion/phage/cgi/gbk2ptt.pl  /tmp/$num.gbk\" > $num.ptt";
			system("echo '$comm' >> $log_file");
			system($comm);
			$comm = "ssh -i $ssh_key $host \"rm /tmp/$num.gbk\"";
			system("echo '$comm' >> $log_file");
			system($comm);
		}
		$process_fail = "Ptt file has not been generated! Case failed!\n";
		$process = "Ptt file has been generated!\n";
		check_success_fail_exit($num, "$num.ptt", $process, $process_fail, "No .ptt file is detected! Please check your gbk file or FASTA sequence file! <br/>This case is terminated!");
		$comm = "perl $exec_dir/clean_empty_PID_lines.pl $num.ptt  $num.fna ";
		system("echo '$comm' >> $log_file");
		system($comm);
	}else{
		system("echo '$num.ptt exist!' >> $log_file");
	}
	
	
		
	write_process_file($process_file, "Generating faa file....\n");
	if (!(-s "$num.faa")){
		my $comm = "perl $exec_dir/gbk2faa.pl $num.gbk $num.fna";
		system("echo '$comm' >> $log_file");
		system("$comm >> $log_file 2>&1") ; #create $num.faa file
		$process_fail = "Faa file has not been generated! Case failed!\n";
		$process = "Faa file has been generated!\n";
		check_success_fail_exit($num, "$num.faa", $process, $process_fail, "No CDS position is detected! Please check your gbk file or FASTA sequence file! <br/>This case is terminated!");
	}else{
		system("echo '$num.faa exist!' >> $log_file");
	}
}

my $phage_finder_start_time = time;
$time = $phage_finder_start_time - $start_time;
system("echo 'Elapsed before tRNA scanning = $time sec' >> $log_file");
system("echo '\nXXXXX running phage finder: Phage_Finder.sh  $num XXXXX' >> $log_file");
write_process_file($process_file, "BLASTing against virus database......\nRunning tRNAscan-SE......\nRunning aragorn......\n");
system("pwd >> $log_file");
my @childs=();
for(my $i=1; $i<=4; $i++){
	my $pid = fork();
	if (not defined $pid) {
	      system("echo Cannot fork!! >>$log_file");
	}elsif ($pid ==0){
		# i am a child
		if ($i ==1){
			system("pwd >> $log_file");
			system("echo '".`date`."' >>$log_file.2");
       		system("Phage_Finder.sh  $num  $cluster_side_virus_database  2>&1 |cat >> $log_file.2");
			if (-e "ncbi.out"){
				system("touch ncbi.done");
			}
			exit;
		}
		if ($i==2){
			tRNA_tmRNA($num, $log_file);
			exit;
		}
		if ($i==3){
			# sometimes tRNAscan-SE runs forever. here sets up a time counter.
			# if time is over, kill the process of tRNAscan-SE
			counter($tRNAscan_tolerate_time, "tRNAscan-SE", "tRNAscan.not_done", "tRNAscan.done", "$log_file.3");
			exit;
		}
		if ($i==4){
			#sometimes Phage_Finder.sh runs forever on the cluster side. here set up a time counter
			# fi time is over, kill the process of Phage_Finder.sh
			counter($Phage_Finder_tolerate_time, "Phage_Finder\.sh", "ncbi.not_done", "ncbi.done", "$log_file.3");
			exit;
		}		
		exit(0);
	}else{
		# I am parent
		push @childs, $pid;
	}
}
foreach my $c (@childs){
	waitpid($c, 0);
}

my $done_phage_finder_time = time;
$time = $done_phage_finder_time - $start_time;
system("echo 'Phage_Finder.sh and tRNA_tmRNA() done at $time sec' >> $log_file");
$time = $done_phage_finder_time - $phage_finder_start_time;
system("echo 'Phage_Finder.sh and tRNA_tmRNA() took $time sec' >> $log_file");

if (-e "ncbi.not_done"){
	$process_fail = `date`."Time is over $Phage_Finder_tolerate_time min. There is no BLAST output ncbi.out of case $num from cluster. Please inform the administrator of the server.\nProgram terminated!\n";
        $process = "";
	system("echo '$process_fail' >> $log_file");
	my %mail = (To => $email_receiver,
		    From => $email_sender,
		    Message => $process_fail
		   );
	sendmail(%mail) or system("echo '".$Mail::Sendmail::error. "' >> $log_file");
	put_back_case_clean_process_exit($num,  $process_fail, $running_case_file); 
}


#system("perl $exec_dir/get_integrase.pl");
system("cat $log_file.2 >> $log_file; rm -rf $log_file.2");


if (!(-e "tRNAscan.out")){
	system("echo No tRNAscan.out >> $log_file");
}
$process_fail = "There is no output from tRNAscan-SE!\n";
$process = "Running tRNAscan-SE is done!\n";
if (-e "tRNAscan.not_done"){
	$process = "Running tRNAscan-SE is not done!\n";
}
check_wirte_process_file("tRNAscan.out", $process_file, $process, $process_fail);

if (!(-e "tmRNA_aragorn.out")){
	system("echo No tmRNA_aragorn.out >> $log_file");
}
$process_fail = "There is no output from aragorn!\n";
$process = "Running aragorn is done!\n";
check_wirte_process_file("tmRNA_aragorn.out", $process_file, $process, $process_fail);

system("echo 'BEFORE Phage_Finder1.sh' >> $log_file");
system("echo 'XXXXX finish running phage finder  XXXXX\n' >> $log_file");

my $phage_finder_time = time;
$time = $phage_finder_time - $start_time;
system("echo 'phage_finder and checks done at $time sec' >> $log_file");

if (!(-s "ncbi.out")){
	$process_fail = "There is not BLAST hit found in the virus database!\n";
	$process = "BLASTing against virus databse is done!\n";
	check_wirte_process_file("ncbi.out", $process_file, $process, $process_fail);
}

my $NC='N/A';
my $gi='N/A';
if (-e "$num.gbk"){
	my $data =`cat $num.gbk`;
	if($data=~/\nVERSION\s+(\S+)\s+GI:(\d+)\n/s){
		$gi = $2;
		$NC=$1;
	}
}else{
	$NC="NC_000000";
}
if ($NC eq 'N/A' or $NC eq ''){
	$NC="NC_000000";
}
system("echo 'NNNNNN NC=$NC' >> $log_file");
if (!(-d "$tmp_dir/$num/$NC\_dir")){
	system("mkdir -p $tmp_dir/$num/$NC\_dir");
}
chdir "$tmp_dir/$num/$NC\_dir";
##### Rah's part###########
my $start_blast_time = time;
$time = $start_blast_time - $start_time;
system("echo 'scan.pl started at $time sec' >> $log_file");
write_process_file($process_file, "Looking for phage-like regions......\n");
#my $cm= "perl $exec_dir/scan.pl  ../$num.fna  ../tRNAscan.out ../tmRNA_aragorn.out  ../$num.faa ../ncbi.out  ../$num.ptt  $local_virus_database ";
my $cm = "perl $exec_dir/scan.pl -n ../$num.fna  -a ../$num.faa  -t  ../tRNAscan.out -m ../tmRNA_aragorn.out  -b ../ncbi.out  -p ../$num.ptt  -use 5 -db $local_virus_database";
$cm .= " -g ../$num.gbk" if (-e "../$num.gbk");
system("echo '$cm'>> $log_file");
system("$cm >$NC\_phmedio.txt  2>>$log_file")==0 or system("echo $! >> $log_file");

$process_fail = "There is no phage-like regions found!\n";
$process = "Looking for regions is done!\n";
check_success_fail_exit($num, "$NC\_phmedio.txt" , $process, $process_fail, "No prophage region detected!");	

system("perl  $exec_dir/non_hit_region_pro_to_ptt.pl ../$num.faa  $NC\_phmedio.txt ../$num.faa.non_hit_pro_region  2>&1 |cat >> $log_file")==0 or system("echo $! >> $log_file");

my $done_blast_time = time;
$time = $done_blast_time - $start_blast_time;
system("echo 'scan.pl took $time sec' >> $log_file");
$time = $done_blast_time - $start_time;
system("echo 'scan.pl done at $time sec' >> $log_file");


# if the input is raw fasta sequence only, we have to get back annotations for phage finder's output result.

if ($flag eq '-s' && !-e "../ncbi.out.non_hit_pro_region"){
	# we need to get the blast result for non hit region proteins first
	write_process_file($process_file, "BLASTing non-phage-like proteins of hit regions against bacterial database......\n");
	system("echo Parallel BLASTing on bacterial database for non-hit-region-proteins... >> $log_file");
	
	my $start_non_hit_time = time;
	$time = $start_non_hit_time - $start_time;
	system("echo 'Start parallel BLASTing non-hit regions at $time sec' >> $log_file");

my $blast_b_dir="$cluster_dir/blast_b";
	system("$exec_dir/call_remote_blast.sh  $blast_b_dir  ../$num.faa.non_hit_pro_region  $cluster_side_bac_database ../ncbi.out.non_hit_pro_region  2>&1 |cat >> $log_file");
	system("touch ../ncbi.out.non_hit_pro_region");  
	#system("blastall -p blastp -d  $nr_database  -m 8 -e 0.001 -i ../$num.faa.non_hit_pro_region  -o ../ncbi.out.non_hit_pro_region -v 1 -b 1 -a 2 -F F");
	$process_fail = "There is no BLAST hit for non-phage-like proteins in the bacterial database!\n";
	$process = "BLASTing non-phage-like proteins against bacterial database is done!\n";
	check_wirte_process_file("../ncbi.out.non_hit_pro_region", $process_file, $process, $process_fail);
	
        my $done_non_hit_time = time;
        $time = $done_non_hit_time - $done_blast_time;
        system("echo 'Parallel BLASTing non-hit regions took $time sec' >> $log_file");
	$time = $done_non_hit_time - $start_time;
        system("echo 'Parallel BLASTing non-hit regions done at $time sec' >> $log_file");
}

my $annotation_start_time = time;
write_process_file($process_file, "Annotating proteins in regions found ......\n");
$cm = "perl $exec_dir/annotation.pl $NC $num  $local_virus_header_database  $local_bac_header_database ../ncbi.out.non_hit_pro_region $NC\_phmedio.txt $flag";
while(1){
	if (`ps -x` =~/annotation.pl/s ){# cannot stand for two annotation.pl running because of lack of memory.
		sleep(3);
	}else{
		last;
	}
}
system("echo '$cm' >> $log_file");
system("$cm  2>&1 |cat >> $log_file");	
$process = "Annotating proteins in regions found  is done!\n";
write_process_file($process_file, $process);
my $done_annotation_time = time;
$time = $done_annotation_time - $annotation_start_time;
system("echo 'annotation.pl plus waiting took $time sec' >> $log_file");
$time = $done_annotation_time - $start_time;
system("echo 'annotation.pl done at $time sec' >> $log_file");

my $extract_protein_start_time = time;
$cm="perl $exec_dir/extract_protein.pl $num  $NC\_phmedio.txt  extract_result.txt";
system("$cm"); #create file 'extract_result.txt' and "NC_XXXX.txt";
system("echo '$cm' >>$log_file");
print_msg("extract_result.txt", $log_file);
my $extract_protein_end_time = time;
$time = $extract_protein_end_time - $extract_protein_start_time;
system("echo 'extract_protein.pl took $time sec' >> $log_file");


#now use filter to get result files.
my $get_true_region_start_time = time;
write_process_file($process_file, "Generating summary file ......\n");
#system("perl $exec_dir/region_phage_percentage.pl  $NC >/dev/null"); # create region_phage_percentage.txt
#print_msg("region_phage_percentage.txt", $log_file);
$cm = "perl $exec_dir/get_true_region.pl $NC\_phmedio.txt  extract_result.txt  true_defective_prophage.txt";
system("echo '$cm' >> $log_file");
system($cm)==0 or system("echo $! >> $log_file"); # create true_defective_prophage.txt
system("cp true_defective_prophage.txt  ../.");
system("cp true_defective_prophage.txt  ../summary.txt");
$process_fail = "There is no summary file generated!\n";
$process = "Summmary file is generated!\n";
check_success_fail_exit($num, "true_defective_prophage.txt" , $process, $process_fail, "There is no summary file generated!");	
my $get_true_region_end_time = time;
$time = $get_true_region_end_time - $get_true_region_start_time;
system("echo 'get_true_region.pl took $time sec' >> $log_file");


# get image
### make png format image
if (-s 'extract_result.txt'){
	my $png_start_time = time;
	system("cp extract_result.txt ../detail.txt");
	write_process_file($process_file, "Generating image file......\n");
	$cm="perl $exec_dir/make_png.pl extract_result.txt  true_defective_prophage.txt  png_input";
	system("echo '$cm' >> $log_file");
	system("$cm  >> $log_file"); # create file png_input  && 'image.png'
	print_msg("image.png", $log_file);
	system("cp png_input  image.png  ../.");
	
	$process_fail = "There is no image file generated!\n";
	$process = "Image file is generated!\n";
	check_wirte_process_file("image.png", $process_file, $process, $process_fail);
	
	my $png_end_time = time;
	$time = $png_end_time - $png_start_time;
	system("echo 'make_png.pl took $time sec' >> $log_file");
}
=pod 
### make cgview part files
if (-e "$NC.txt"){
	system("perl $exec_dir/region_to_ptt.pl  ../$num.ptt  $NC.txt  png_input ../region.ptt");
	print_msg("../region.ptt", $log_file);
	chdir "$tmp_dir/$num";
	system("java -jar /home/phast/public_html/cgview/cgview.jar -i region.ptt -s region_series -A 10");
	print_msg("region_series/index.html", $log_file);
	system("perl $exec_dir/change_link.pl  $tmp_dir/$num ");
}
=cut

#mark finished 
if (!(-e "$tmp_dir/$num/fail.txt") && (-e "$tmp_dir/$num/true_defective_prophage.txt")){
	system("touch $tmp_dir/$num/success.txt");
	my $date = `date`;
	if (-e "$tmp_dir/$num/tRNAscan.not_done"){
		system("echo '$num  $date  fail  tRNAscan not working' >> $tmp_dir/case_record.log");
	}else{
		system("echo '$num  $date  success' >> $tmp_dir/case_record.log");
	}
	
};
#cleanup
cleanup($num, $flag);
system("echo 'Program exit!' >> $log_file");
exit;

sub cleanup{
	my $num=shift;
	my $flag = shift;
	make_done_file($num);
	build_png_input_png_input_RNA($num); # create png_input file
	make_region_DNA($num); # create region DNA file "region_DNA.txt"
	my $last_time = time;
	$time = $last_time - $start_time;
	system("echo  'Program finished, taking $time seconds!' >> $log_file");
	write_process_file($process_file, "Program finished!\n");

	system("echo 'rm -rf $running_case_file' >>$log_file");
	unlink  $running_case_file;
	system("echo '$exec_dir/transfer_to_remote_result_tmp.sh   $tmp_dir/$num' >> $log_file");
	system("$exec_dir/transfer_to_remote_result_tmp.sh   $tmp_dir/$num $num");
	#make_browse($num);
	chdir "$tmp_dir/$num";
	system("echo 'In $tmp_dir/$num, rm *gbk  *ptt *fna *faa* *ncbi.out*  tRNA*  tmRNA*  *_dir *gbk_combine tmp_$num' >> $log_file");
	system("rm  -rf *gbk  *ptt *fna *faa* *ncbi.out*  tRNA*  tmRNA*  *_dir *gbk_combine tmp_$num");
	my $cm = "perl $exec_dir/cleanup.pl  $tmp_dir $cleanup_days";
	system("echo '$cm' >> $log_file");
	system("$cm 2>&1|cat >/dev/null");
}

sub make_browse{
	my $num =shift;
	if ($num !~ /^1/ ||  length($num) !=10){
    	unlink "$tmp_dir/make_browse_list";
	    system("echo 'remove $tmp_dir/make_browse_list' >> $log_file") if (!-e "$tmp_dir/make_browse_list");
    	system("echo 'perl $exec_dir/make_browse.pl' >> $log_file");
	    system("perl $exec_dir/make_browse.pl >> $log_file")==0 or system("echo 'Error:$!' >> $log_file"); # make browse.html
	}
}	

sub make_done_file{
	my $num = shift;
	if (-e "$tmp_dir/$num/$num.fna"){
 	   	my $content=`cat $tmp_dir/$num/$num.fna`;
    	if ($content=~/>(.*?)\n/s){
        	my $desc=$1;
	        if ($desc=~/gi\|(\d+)\|ref\|(.*?)\|/){
    	        $NC= $2;
        	    $gi = $1;
	        }
    	    $desc =~s/DEFINITION//si;
        	$desc =~s/gi\|\d+\|\s*//si;
	        $desc =~s/ref\|(.*?)\|\s*//si;
    	    system("echo '$desc' >  $tmp_dir/$num/$num.done");
        	system("echo 'ACCESSION: $NC' >> $tmp_dir/$num/$num.done");
	        system("echo 'GI: $gi' >> $tmp_dir/$num/$num.done");
    	}
	}
	if (-e "$tmp_dir/$num/$num.done"){
		system("echo '$tmp_dir/$num/$num.done generated' >>$log_file");
	}else{
		system("echo '$tmp_dir/$num/$num.done Not generted' >> $log_file");
	}
}
sub get_gbk_file{
    my ($num,  $log_file, $tmp_dir)= @_;
    my $tmp = `ssh -i $ssh_key $host "ls /home/prion/phage/result_tmp/$num/$num.gbk" `;
    if (defined $tmp && $tmp ne ''){
        `scp -i $ssh_key $host:/home/prion/phage/result_tmp/$num/$num.gbk .`;
        system("echo 'XXXX Got $num.gbk from $host' >> $log_file");
    }else{
        system("echo 'call remote get_gbk.pl to get gbk file' >> $log_file");
        system("echo 'ssh -i  $ssh_key $host \"perl /home/prion/phage/cgi/get_gbk.pl $num /tmp\"' >> $log_file");
	system("ssh -i  $ssh_key $host \"perl /home/prion/phage/cgi/get_gbk.pl $num /tmp\" ");
	system("scp -i  $ssh_key $host:/tmp/$num.gbk $tmp_dir/$num/.");
	system("ssh -i  $ssh_key $host \"rm /tmp/$num.gbk");
       # system("echo 'perl $exec_dir/get_gbk.pl $num  $tmp_dir/$num' >> $log_file");
       # system("perl $exec_dir/get_gbk.pl $num  $tmp_dir/$num 2>&1|cat >> $log_file.2"); # create $num.gbk file
		# sometimes BioPERL not work,, so we use cluster's program
		if (! -s "$tmp_dir/$num/$num.gbk"){
			system("echo 'Cannot get $num.gbk from remote get_gbk.pl' >> $log_file.2");
			system("ssh -i  $ssh_key $host \"perl /home/prion/phage/cgi/get_gbk.pl $num /tmp\" ");
			system("scp -i  $ssh_key $host:/tmp/$num.gbk $tmp_dir/$num/.");
			system("ssh -i  $ssh_key $host \"rm /tmp/$num.gbk");
			if (! -s "$tmp_dir/$num/$num.gbk"){
				system("echo 'Not Get $num.gbk from remote get_gbk.pl' >> $log_file.2");
			}else{
				system("echo 'Get $num.gbk from remote get_gbk.pl' >> $log_file.2");
			}
		}
        system("cat $log_file.2 >> $log_file; rm -rf $log_file.2");
    }
	if (`cat $num.gbk`=~/\nWGS(_SCAFLD)*\s+(.*?)-(.*?)\n/s){
        system("perl $exec_dir/get_combine.pl $num");
		system("echo 'Combine contigs, $num.gbk_combine generated' >> $log_file") if (-s "$num.gbk_combine");
     }else{
		system("echo 'gbk file is not contig file' >> $log_file");
	}

    print_msg("$num.gbk", $log_file);

}

sub print_msg{
	my ($file, $log_file)= @_;
	if (-s $file ){
		system("echo  $file is created ! >> $log_file");
	}else{
		system("echo  $file is not created ! >> $log_file");
	}
}



sub check_success_fail_exit{
	my ($num, $file,$process, $process_fail,  $msg)=@_;
	print_msg($file, $log_file);
	check_wirte_process_file($file, $process_file, $process, $process_fail);
	if (!(-s $file)){
		system("echo '$msg' >> $tmp_dir/$num/fail.txt");
		system("touch $tmp_dir/$num/summary.txt");
		system("touch $tmp_dir/$num/detail.txt");
		my $date = `date`;
		system("echo '$num  $date fail $msg' >> $tmp_dir/case_record.log");
		if (defined $running_case_file && $running_case_file ne ''){
			system("echo 'rm -rf $running_case_file' >>$log_file");
    		unlink  $running_case_file;
		}
		system("echo 'Program exit!' >> $log_file");
		exit;
	}
}
# output process file for tracing the process of the case. 
sub write_process_file{
	my ($process_file, $msg)=@_;
	open(OUT, ">>$process_file") or die "Cannot write $process_file";
	print OUT $msg;
	close OUT;
}

# check file's existence, and output process file
sub check_wirte_process_file{
	my ($file, $process_file, $success_msg, $fail_msg)=@_;
	if (-s $file) {
		write_process_file($process_file, $success_msg);
	}else{
		write_process_file($process_file, $fail_msg);	 
	}
}

sub make_region_DNA{
	my $num=shift;
	my $cur_dir = getcwd;
	chdir "$tmp_dir/$num";
	my $DNA_seq= `cat $num.fna`;
	$DNA_seq=~s/^>.*?\n//s; $DNA_seq=~s/[\s\n]//gs;
	my $sum_file_content=`cat summary.txt`;
	$sum_file_content=~s/.*---------------+\n//s;
	my @pos=();
	foreach my $line(split "\n", $sum_file_content){
		my @tmp=split " ", $line;
		push @pos, $tmp[4];
	}	
	if (@pos !=0){
		open(OUT, ">region_DNA.txt") or die "Cannot write region_DNA.txt";
		for(my $i=0; $i <=$#pos; $i++){
			my ($start, $end)= $pos[$i]=~/(\d+)-(\d+)/;
			print OUT ">".($i+1)."\t $pos[$i]\n";
			my $seq= substr($DNA_seq, $start-1, $end-$start+1);
			my @tmp=$seq=~/(\w{0,70})/gs;
			foreach my $l(@tmp){
				print OUT $l."\n";
			}
			print OUT "\n";
		}
		close OUT;
	}
	chdir $cur_dir;
}

# if png_input file is empty, create one
sub build_png_input_png_input_RNA{
	my $num=shift;
	my $empty=0;
	my $cur_dir = getcwd;
	chdir "$tmp_dir/$num";
	if(-e "png_input"){
		my $data =`cat png_input`;
		$data=~s/\n\s//g;
		if ($data !~/section/ ){
			$empty=1;
		}
	}else{
		$empty = 1;
	}
	system("echo 'empty=$empty' >> $log_file");
	my $head_line = '';
	if ((-e "$num.fna") && $empty==1){
		system("echo 'Rebuild png_input' >> $log_file");
		open (IN98, "$num.fna");
		my $header = '';
		my $seq ='';
		while (<IN98>){
			chomp($_);
			if ($_=~/^>gi\|.*?\|ref\|(\S+?)\|(.*) /){
				my $NC=$1;
				my $name = $2;
				$NC=~s/\..*//;
				$name=~s/[\r\n]//g;
				$header = ">$name [asmbl_id: $NC]. "; 
				$head_line = $header;
			}else{
				$seq .= $_;
			}				
		}
		close IN98;
		$seq =~s/\s//g;
		$header .= length($seq);
		open (OUT11, ">png_input");
		print OUT11 $header."\n";
		print OUT11 "         from       to      strand      match      protein_name     EVALUE     protein_sequence\n";
		close OUT11;

	}
	if (!(-e "png_input_RNA") && (-e "$num.fna")){
		open(OUT, ">png_input_RNA");
		print OUT $head_line."\n";
		print OUT "         from       to      strand      match      RNA_name     EVALUE     match_sequence\n";
		close OUT;
	}
	if (!(-e "$num.fna")){
		system("echo 'No fna file, no png_input and png_input_RNA created!'>> $log_file");
	}
	chdir $cur_dir;
}

# check the gbk file. if there is CDS and translation sections, we keep it.
# if there is CDS and no translation sections , we make translation section into it.
# if there is no CDS and no translation sections, we make fna file .
sub check_gbk_file{
	my $gbk_file = shift;
	my $num = shift;
	open (IN, $gbk_file);
	my $translation_flag  = 0;
	my $db_xref_flag =0;
	my $CDS_flag= 0;
	my $seq_flag =0;
	my $seq ='';
	my %hash =();
    my @keys=();
	my $desc = '';
	my $NC='';
	my $GI='';
	while (<IN>){
		chomp($_);
		if ($_=~/^VERSION\s+(\S+)\s+GI:(\d+)/){
			$GI = $2;
			$NC = $1
		}
		if ($_=~/^DEFINITION\s+(.*)/){
			$desc=$1;
		}
		if ($_=~/^\s*CDS\s+(\S+)/ ){
            push @keys, $1;
			$hash{$1}='';
			$CDS_flag = 1;
		}elsif($_=~/^\s+\/db_xref="GI:/){
			$db_xref_flag = 1;
		}elsif($_=~/^\s+\/translation="/){
			$translation_flag = 1;	
		}elsif($_=~/^ORIGIN/){
			$seq_flag = 1;
			next;
		}elsif($_=~/^\/\//){
			$seq_flag = 0;
		}		
		if ($seq_flag==1){
			$_=~s/[\d\s\n]//g;
 			$_=uc($_);
			$seq .= $_;
		}
	}
	close IN;
	if ($seq ne ''){
		$seq_flag=1;
	}
	my $DNA_seq = $seq;
	$DNA_seq =~s/[ACGTRYKMSWBDHVN]//sig;
	if ($DNA_seq ne ''){
		$DNA_seq=~s/(\w{0,60})/$1\n/gs;
		$process_fail = "Wrong DNA seq = $DNA_seq in gbk file! We accept 'ACGTRYKMSWBDHVN' only.\nProgram terminated!\n";
		$process = "";
		check_success_fail_exit($num, "NA_wrong_seq", $process, $process_fail, $process_fail);	
	}
	system("echo 'seq_flag=$seq_flag, CDS_flag=$CDS_flag, db_xref_flag=$db_xref_flag, translation_flag=$translation_flag, NC=$NC, GI=$GI, desc=$desc' >> $log_file");
	if ($seq_flag ==0){
		my $content= `cat $gbk_file`;
		$process_fail = "There is no ORIGIN part in GBK file. That is weird.\n\n<pre>$content</pre>";
	        $process = "There is DNA part in GBK file.\n";
        	check_success_fail_exit($num, "NA_no_seq", $process, $process_fail, $process_fail);

	}	
	elsif ($seq_flag==1 && $CDS_flag ==1 && ($db_xref_flag==0 or $translation_flag ==0) ){ # there is CDS section
		system("echo 'Generate translation section for gbk file' >> $log_file");
		foreach my $k (@keys){
			$hash{$k} = get_seq(\$seq, $k);
		}
		open (IN, $gbk_file);
		open (OUT, ">$gbk_file.tmp");
		my $key = '';
		my $flag =0;
		my $gi_count=0;
		while(<IN>){
			if ($_=~/^\s*CDS\s+(\S+)/ ){
				$key = $1;
                if ($key =~/complement\((\d+)\.\.(\d+)\)/ || $key =~/(\d+)\.\.(\d+)\)/){
					if ($2<$1){
						$start= $1; $end=$2;
						$key =~s/\d+\.\.\d+/$end\.\.$start/;
					    $_=~s/CDS\s+\S+/CDS             $key/;	
					}
				}
				$flag = 1;	 
				print OUT $_;
				if ($db_xref_flag==0){ # no db_xref
                    $gi_count++;
                    print OUT "                     \/db_xref=\"GI:$gi_count\"\n";
                }
				next;
			}
			if ($_!~/^\s+\// && $key ne '' && $flag ==1){
				if ($translation_flag ==0){# no translation
					print OUT "                     \/translation=\"$hash{$key}\"\n";
				}
				$key ='';
				$flag =0;
			}
			print OUT $_;
		}
		close IN;
		close OUT;
		system("cp -f $gbk_file.tmp $gbk_file");
		$translation_flag = 1;
	}
	elsif ($seq_flag==1 && $CDS_flag ==0 && $translation_flag ==0){# no translation and no CDS
		system("echo 'Make fna file from gbk file' >> $log_file");
		$seq = uc($seq);
		my @arr= $seq =~/\w{0,60}/g;
		open (OUT, ">$num.fna");
		chomp($desc);
		print OUT ">gi|$GI|ref|$NC| $desc\n";
		foreach my $l (@arr){
			print OUT $l."\n";
		}
		close OUT;
	}else{
		system("echo 'Keep gbk file intact' >> $log_file");
	}
	return $translation_flag;
}

# get translation sequence--protein_seq for each CDS 
sub  get_seq{
	my ($seq, $locations )=@_;
	my $seq3 = '';
	my $start_num='';
	my $end_num = '';
	my $len='';
	my @arr =();
	if ($locations=~/complement\(\d+\.\.\d+\)/){
		@arr= $locations=~/(complement\(\d+\.\.\d+\))/g;
	}
	elsif ($locations=~/\d+\.\.\d+/){
		@arr= $locations=~/(\d+\.\.\d+)/g;
	}
	
	foreach my $l (@arr){
		my $seq2 = '';
		if ($l =~/complement\((\d+)\.\.(\d+)\)/){
			$start_num = $1;
			$end_num = $2;	
            if ($start_num > $end_num){
				my $tmp= $end_num;
                $end_num=$start_num;
                $start_num = $tmp;
            }
			$len = $end_num-$start_num+1;
			$seq2 = substr($$seq, $start_num-1, $len);
		
			$seq2 =~ tr/[A,T,C,G,a,t,c,g]/[T,A,G,C,t,a,g,c]/;
			$seq2 = scalar reverse($seq2);
		
		}elsif($l =~/(\d+)\.\.(\d+)/){
			$start_num = $1;
			$end_num = $2;		
            if ($start_num > $end_num){  
                my $tmp= $end_num;
                $end_num=$start_num;
                $start_num = $tmp;
            }
			$len = $end_num-$start_num+1;
			$seq2 = substr($$seq, $start_num-1, $len);
		}
		$seq3 .= $seq2;
	}
	$seq3 = change_to_aa ($seq3);
	return $seq3;
}

# change nucleotide acid to amino acid.
sub change_to_aa{
	my $seq = shift;
	my @arr = $seq=~/\w{0,3}/g;
	foreach my $i (0..$#arr){
		if ($arr[$i]=~/\w\w\w/){
			$arr[$i] =~s/TTT/F/i;$arr[$i] =~s/TTC/F/i;$arr[$i] =~s/TTA/L/i;$arr[$i] =~s/TTG/L/i;$arr[$i] =~s/CTT/L/i;$arr[$i] =~s/CTC/L/i;
			$arr[$i] =~s/CTA/L/i;$arr[$i] =~s/CTG/L/i;$arr[$i] =~s/ATT/I/i;$arr[$i] =~s/ATC/I/i;$arr[$i] =~s/ATA/I/i;$arr[$i] =~s/ATG/M/i;
			$arr[$i] =~s/GTT/V/i;$arr[$i] =~s/GTC/V/i;$arr[$i] =~s/GTA/V/i;$arr[$i] =~s/GTG/V/i;$arr[$i] =~s/TCT/S/i;$arr[$i] =~s/TCC/S/i;
			$arr[$i] =~s/TCA/S/i;$arr[$i] =~s/TCG/S/i;$arr[$i] =~s/CCT/P/i;$arr[$i] =~s/CCC/P/i;$arr[$i] =~s/CCA/P/i;$arr[$i] =~s/CCG/P/i;
			$arr[$i] =~s/ACT/T/i;$arr[$i] =~s/ACC/T/i;$arr[$i] =~s/ACA/T/i;$arr[$i] =~s/ACG/T/i;$arr[$i] =~s/GCT/A/i;$arr[$i] =~s/GCC/A/i;
			$arr[$i] =~s/GCA/A/i;$arr[$i] =~s/GCG/A/i;$arr[$i] =~s/TAT/Y/i;$arr[$i] =~s/TAC/Y/i;$arr[$i] =~s/TAA//i;$arr[$i] =~s/TAG//i;
			$arr[$i] =~s/CAT/H/i;$arr[$i] =~s/CAC/H/i;$arr[$i] =~s/CAA/Q/i;$arr[$i] =~s/CAG/Q/i;$arr[$i] =~s/AAT/N/i;$arr[$i] =~s/AAC/N/i;
			$arr[$i] =~s/AAA/K/i;$arr[$i] =~s/AAG/K/i;$arr[$i] =~s/GAT/D/i;$arr[$i] =~s/GAC/D/i;$arr[$i] =~s/GAA/E/i;$arr[$i] =~s/GAG/E/i;
			$arr[$i] =~s/TGT/C/i;$arr[$i] =~s/TGC/C/i;$arr[$i] =~s/TGA//i;$arr[$i] =~s/TGG/W/i;$arr[$i] =~s/CGT/R/i;$arr[$i] =~s/CGC/R/i;
			$arr[$i] =~s/CGA/R/i;$arr[$i] =~s/CGG/R/i;$arr[$i] =~s/AGT/S/i;$arr[$i] =~s/AGC/S/i;$arr[$i] =~s/AGA/R/i;$arr[$i] =~s/AGG/R/i;
			$arr[$i] =~s/GGT/G/i;$arr[$i] =~s/GGC/G/i;$arr[$i] =~s/GGA/G/i;$arr[$i] =~s/GGG/G/i;
		}else{
			$arr[$i]='';
		}
	}
	return join('',@arr); 

}

sub put_back_case_clean_process_exit{
	my ($num,  $msg, $running_file)=@_;
	my $queue_data= `cat $queue_file`;
	open(OUTQ, ">$queue_file");
    print OUTQ `cat $running_file`;
    print OUTQ $queue_data;
    close OUTQ;
    unlink $running_file;
	if (-s $running_file){
		system("echo 'Cannot remove $running_file' >> $tmp_dir/case_record.log");
		print "Cannot remove $running_file\n";
	}else{
		system("echo 'Remove $running_file' >> $tmp_dir/case_record.log");
  		print "Remove $running_file\n";
	}
	$queue_data= `cat $queue_file`;
	if ($queue_data =~/$num/s){
		system("echo '$num is back to $queue_file' >> $tmp_dir/case_record.log");
		print "$num is back to $queue_file\n";
	}else{
		system("echo '$num is NOT back to $queue_file' >> $tmp_dir/case_record.log");
		print "$num is NOT back to $queue_file\n";
	}
	system("echo '$msg' >> $tmp_dir/case_record.log");
	print "$msg\n";
	chdir "$tmp_dir/$num";
	system("rm  -rf *done *ptt *faa* *ncbi.out*  tRNA*  tmRNA* *process *_dir");
    my $process = `ps x`;
    foreach my $ps (split("\n", $process)){
	    if ($ps =~/^\s*(\d+) .*?$num/){
			system("echo 'kill -9 $1 for $num' >> $tmp_dir/$num/$num.log.3");
			system("echo 'kill -9 $1' >>$tmp_dir/case_record.log");
        	system("kill -9 $1");
		        
	    }else{
			if ($ps=~/$num/){
				system("echo 'No $ps captured' >> $tmp_dir/case_record.log");
			}
		}
    }
	system("echo 'kill -9 $CURRENT_PID for $num 's phage.pl' >> $tmp_dir/$num/$num.log.3");
    system("echo 'kill -9 $CURRENT_PID of phage.pl' >>$tmp_dir/case_record.log");
    system("kill -9 $CURRENT_PID");
    exit;
	
}

sub counter{
    my ($time, $program, $not_done_file, $done_file, $log_file)=@_;
    my $count=0;
    while(1){
        sleep(3);
        $count++;
        if ($count==$time*60/3){ #5 minutes
            system("touch $not_done_file");
            my $process = `ps -x`;
            foreach my $ps (split("\n", $process)){
                if ($ps =~/^\s*(\d+) .*?$program/){
                    system("kill -9 $1");
                    system("echo 'kill -9 $1 for $program' >> $log_file");
                }
            }
            last;
        }
        if (-e $done_file){
            last;
        }
    }
}
sub tRNA_tmRNA{
    my ($num, $log_file)=@_;
    system("echo   'find tRNA sequences ...' >> $log_file");
    
    my $trnascan_start = time;

    system("tRNAscan-SE -B -o tRNAscan.out $num.fna 2>&1 |cat >> $log_file")==0 or system("echo $! >>$log_file") if (!-e "tRNAscan.out");
    
    my $trnascan_done = time;
    my $time = $trnascan_done - $trnascan_start;
    system("echo 'tRNAscan-SE took $time s' >> $log_file");

    system("touch tRNAscan.done") if (-e "tRNAscan.out");

    system("echo   'find tmRNA sequences ...' >> $log_file");
    system("aragorn -m -o tmRNA_aragorn.out $num.fna  2>&1 |cat >> $log_file")==0 or system("echo $! >>$log_file") if (!-e "tmRNA sequences");
    
    my $aragorn_done = time;
    $time = $aragorn_done - $trnascan_done;
    system("echo 'aragorn took $time s' >> $log_file");

    #extract RNA
    system("echo '$exec_dir/extract_RNA.pl $num  extract_RNA_result.txt.tmp' >> $log_file");
    system("perl $exec_dir/extract_RNA.pl $num  extract_RNA_result.txt.tmp");
    system("echo '$exec_dir/make_RNA_png_input.pl  extract_RNA_result.txt.tmp  png_input_RNA' >> $log_file");
    system("perl $exec_dir/make_RNA_png_input.pl  extract_RNA_result.txt.tmp  png_input_RNA");
}

sub find_running_case_file{
	my ($running_array) = @_;
	print "length of running_array= ". scalar(@$running_array). "\n";
	my $running_file = '';
	for(my $i=0; $i < scalar(@$running_array); $i++){
		my $running_file = $$running_array[$i];
		print "$running_file here\n";
		if (-e $running_file){
    	# check the status of running.txt in tolerate time , if over , kill it
    		my $data = `cat $running_file`;
    		my ($last_num) = $data=~/\S+\s+(\S+)/s;
    		my $time= `ls -l $running_file`;
    		my $now_time = `date`;
    		my ($hour, $min)=$time=~/ (\d+):(\d+)/s;
    		my ($hour1,$min1)=$now_time=~/ (\d+):(\d+)/s;
    		$time=$hour*60+$min; $now_time=$hour1*60+$min1;
			$now_time += 24*60  if ($time > $now_time);
    		if ($now_time - $time >$last_case_tolerate_time){
        		my $msg="Over $last_case_tolerate_time mins, h:$hour, m:$min, h1:$hour1, m1:$min1, old_time:$time, now_time:$now_time, $last_num is got stuck. kill it ";
        		put_back_case_clean_process_exit($last_num,  $msg, $running_file);
    		}
		}
		if (-e $running_file){
    		#  some case is running, check on next running file
    		print "$running_file exist!!Next\n";
			$running_file = '';
    		next;
		}else{
			print "$running_file can be used now\n";
			return $running_file;
		}

	}# end of for  
	return $running_file;
}

