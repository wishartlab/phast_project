#!/usr/bin/perl -w

# this program will change the true_defective_prophage.txt into 
# html since  txt format on different browsers shows weird.
use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $base_root="/var/www/html/phast/current/public";
my $q = new CGI;
my $dir = $q->param('num');
my $file = $q->param('file');
my $tmp1=$q->param('tmp');
my $sum_file = (defined $file && $file ne '')? $file : "summary.txt";
my $tmp = (defined $tmp1 && $tmp1 ne '')? "$base_root/$tmp1" : "$base_root/tmp";
my $rec= $q->param('rec');


open (IN, "$tmp/$dir/$sum_file" ) or die "Cannot open $tmp/$dir/$sum_file";
my @arr=(); # header
my @arr1=(); #data
my $flag=0;
my $flag1= 0;
my $specie='';
while(<IN>){
	if ($_ =~ /^Totally/){
		$flag1=1;
		next;
	}
	if($_=~/REGION\s+REGION_LENGTH/){
		$flag1=0;
		@arr=split(/\s\s\s+/, $_);
		$flag=1;
	}
	if ($flag==1 && $_=~/^\s+$rec\s+/){
		@arr1=split(/\s\s\s+/, $_);
		$flag =0;
	}
	if ($flag1 ==1 && $_ !~/^\s*$/){
		chomp($_);
		$_=~s/^\s*//;
		$species .= "$_ ";
	}
}
close IN;

my $note = "<font size=3><b>Legend:</b></font><br>".
        "<font size=2><b>REGION</b>: the number assigned to the region<br>".
        "<b>REGION_LENGTH</b>: the length of the sequence of that region (in bp)<br>".
        "<b>PREDICT_INTACT_OR_INCOMPLETE (score)</b>: a prediction of whether the region contains an intact or incomplete prophage based on the above criteria (with score in brackets)<br>".
        "<b>SPECIFIC_KEYWORD</b>: the specific phage-related keyword(s) found in protein name(s) in the region<br>".
        "<b>REGION_POSITION</b>: the start and end positions of the region on the bacterial chromosome<br>".
        "<b>TRNA_NUM</b>: the number of tRNA genes present in the region<br>".
        "<b>TOTAL_PROTEIN_NUM</b>: the number of ORFs present in the region<br>".
        "<b>PHAGE_HIT_PROTEIN_NUM</b>: the number of proteins in the region with matches in the phage protein database<br>".
        "<b>HYPOTHETICAL_PROTEIN_NUM</b>: the number of hypothetical proteins in the region without a match in the database<br>".
        "<b>PHAGE+HYPO_PROTEIN_PERCENTAGE</b>: the combined percentage of phage proteins and hypothetical proteins in the region<br>".
        "<b>BACTERIAL_PROTEIN_NUM</b>: the number of proteins in the region with matches in the nrfilt database<br>".
        "<b>ATT_SITE_SHOWUP</b>: the putative phage attachment site<br>".
        "<b>PHAGE_SPECIES_NUM</b>: the number of different phages that have similar proteins to those in the region<br>".
        "<b>MOST_COMMON_PHAGE_NAME</b>: the phage with the highest number of proteins most similar to those in the region<br>".
        "<b>MOST_COMMON_PHAGE_NUM</b>: the number of phages with the highest number of proteins most similar to those in the region<br>".
        "<b>MOST_COMMON_PHAGE_PERCENTAGE</b>: the percentage of proteins in PHAGE_HIT_PROTEIN_NUM that are most similar to MOST_COMMON_PHAGE_NAME proteins<br>".
        "<b>GC_PERCENTAGE</b>: the percentage of gc nucleotides of the region<br></font>";



print "content-type: text/html \n\n";
print "<b><font size=3>$species</font></b><br>";
print "<table border='1' cellpadding='1' cellspacing='1' style='font-size:14;'>";

my $str ='';
foreach my $i (0..$#arr){
	next if ($arr[$i] eq '');
	$arr1[$i]=~s/,/<br>/gs if ($i==14);
	$str.="<tr><td>$arr[$i]</td><td>$arr1[$i]</td></tr>";
}
$str .="</table>";
print $str;
print $note;

exit;

