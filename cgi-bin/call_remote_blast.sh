#!/bin/bash

blast_b_dir=$1
input_file=$2
bac_database=$3
output_file=$4
input_basename=`basename $input_file|tr -d "\n"`
key='/apps/phast/.ssh/scp-key'
host="$HTTP_PHAST_CLUSTER_USERNAME@$HTTP_PHAST_CLUSTER_HOSTNAME"

pre_time=$(date +"%s")
ssh -i $key  $host  "mkdir -p $blast_b_dir"
post_time=$(date +"%s")
elapsed=$((post_time-pre_time))
echo "call_remote_blast.sh: mkdir on cluster via SSH took $elapsed sec"

pre_time=$(date +"%s")
scp -i $key  $input_file  $host:$blast_b_dir
post_time=$(date +"%s")
elapsed=$((post_time-pre_time))
echo "call_remote_blast.sh: copy $input_file to cluster took $elapsed sec"

pre_time=$(date +"%s")
command="perl /home/prion/phage/cgi/call_blast_parallel.pl $blast_b_dir/$input_basename  $bac_database"
echo $command
ssh -i $key  $host "$command"
post_time=$(date +"%s")
elapsed=$((post_time-pre_time))
echo "call_remote_blast.sh: run call_blast_parallel.pl on cluster via SSH took $elapsed sec"

pre_time=$(date +"%s")
scp -i $key  $host:$blast_b_dir/$input_basename\_blast_out $output_file
post_time=$(date +"%s")
elapsed=$((post_time-pre_time))
echo "call_remote_blast.sh: copy $output_file from cluster took $elapsed sec"

