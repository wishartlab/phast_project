#!/usr/bin/perl -w

# this program will initiate all the sub programs to finish 
# phage finder and filter's jobs.
use Cwd;
use File::Path;
use File::Copy;
use File::Basename;

$ENV{'PATH'} .=":/usr/local/bin:/apps/phast/bin";
my $sub_program_dir= "/var/www/html/phast/current/public/sub_programs";
my $BASE_ROOT="/var/www/html/phast/current/public";
my $tmp_dir = "$BASE_ROOT/tmp_basys"; # this dir will cotain all the temp files and result files.
my $exec_dir = "$BASE_ROOT/cgi-bin";  # dir of executables.
my $change_to_protein_exec =  $exec_dir."/change_to_protein_seq.pl";
my $change_to_ptt_exec = $exec_dir."/change_to_ptt_format.pl";
# my $nr_database = "$BASE_ROOT/databases/nrfilt";

my $bac_database="bacteria_all_select.db"; # on the cluster side
my $cluster_side_bac_database = "/home/prion/phage/DB/$bac_database";
my $local_bac_database = "$sub_program_dir/phage_finder/DB/$bac_database";

my $virus_database="prophage_virus.db"; # virus db name
my $cluster_side_virus_database = "/home/prion/phage/DB/$virus_database";
my $local_virus_database = "$sub_program_dir/phage_finder/DB/$virus_database";

my $pathogenicity_database="virulenceDB.protein.fasta.filter";
my $cluster_side_pathogenicity_db="/home/prion/basys2-backend/pathogenicity/$pathogenicity_database";
my $local_pathogenicity_database= "$sub_program_dir/phage_finder/DB/$pathogenicity_database";
my $pathogenicity_match_table ="$sub_program_dir/phage_finder/DB/completeMvirDBTable.txt.tmp";

my $CLUSTER_DISK = $ENV{HTTP_PHAST_CLUSTER_DISK};

my $HOSTNAME = $ENV{HTTP_PHAST_CLUSTER_HOSTNAME};
my $USERNAME = $ENV{HTTP_PHAST_CLUSTER_USERNAME};
my $host = $USERNAME . '@' . $HOSTNAME;
my $ssh_key = "~/.ssh/scp-key";

my $Phage_Finder_tolerate_time=15; #min
my $tRNAscan_tolerate_time=5;#min

if(scalar @ARGV!=2 && scalar @ARGV!=4){
	print STDERR "Error: Usage : perl phage_for_basys.pl [-a|-g|-s] <case_number> [-gap <gap_number>]\nWith no -gap option, default gap is 3000 bp used in DBscan algorithm in scan.pl\n";
	exit(-1);
}

my $start_time = time;
my $time;

my $flag = $ARGV[0];
my $num= $ARGV[1];
my $gap=3000; #default for scan.pl
foreach my $i (0..$#ARGV){
	if ($ARGV[$i] eq '-gap'){
		if (defined $ARGV[$i+1]){
			$gap= $ARGV[$i+1];
		}else{
			print STDERR "Error: correct Usage: perl phage_for_basys.pl <-a|-g|-s> <NC_number> [-gap  <gap_number>]\n";
			exit(-1);
		}
	}
}
if (!(-d "$tmp_dir/$num")){
        mkdir "$tmp_dir/$num";
}
my $log_file="$tmp_dir/$num/$num.log";
my $ti = `date`;
system("echo 'Case start running ,time=$ti' >> $log_file");


my $process_file ="$tmp_dir/$num/$num.process";
my $process='';
my $process_fail='';
my $msg='';

chdir "$tmp_dir/$num";
my $cluster_dir="/home/prion/phage/tmp/$num";
system("$exec_dir/create_remote_dir.sh  $cluster_dir   2>&1 |cat >> $log_file");

my $cluster_sp = `ssh -i $ssh_key  $host  'df -h' `;
if ($cluster_sp =~/(\d+)% $CLUSTER_DISK/s){
	my $perc= $1;
	if ($perc ==100){
		$process= "The cluster side hard drive is $perc percent full!\n";
		$process_fail= "The cluster side hard drive is $perc percent full! There is no space left in the cluster. Please inform the administrator.\n";
		$msg="The cluster side hard drive is $perc percent full!";
		check_success_fail_exit($num, "NA", $process, $process_fail, $process_file, $log_file, $msg);	
	}
}

if($flag eq '-a'){# we have gi number or accession number or GBK file
		if (!(-s "$num.gbk")){
			&get_gbk_file($num, $flag, $log_file, $tmp_dir);
			$process = "Genbank file has been acquired!\n";
			$process_fail = "Genbank file has not been acquired! Case failed!\n";
			$msg ="Cannot get back the GBK file of $num  from Genbank! This case is terminated!";
			check_success_fail_exit($num, "$num.gbk", $process, $process_fail, $process_file, $log_file, $msg);
		}else{
			system("echo '$num.gbk exist!' >> $log_file");
		}		
}elsif($flag eq '-g') {# we have GBK file
		#system("echo '$num.gbk from Input!' >> $log_file");
}

if (-s "$num.gbk"){
	my $has_translation = check_gbk_file("$num.gbk", $num, $process_file);
	if ($has_translation ==0){
		$flag = '-s';
		system("echo '$num.gbk has no translation area, change it into DNA sequence only' >> $log_file");
	}
}


if ($flag eq '-s'){ # have fasta seq , use glimmer to predict ORF and create .ppt .fna .faa files;
	if (!-s "$num.fna"){
		 $process=  "There is no DNA sequence .fna file. Program terminated!\n";
		 $process_fail=$process;
         check_success_fail_exit($num, "NA", $process, $process_fail,$process_file, $log_file, $process_fail);
	}
	if (!-e "$num.ptt"){
		system("echo XXXXX running glimmer XXXXXX >> $log_file");
		my $start_glimmer_time = time;

		$process="Glimmer-3.02 is running......\n";
		write_process_file($process_file,$process );

		system("echo XXXXX parallel running glimmer XXXXXX >> $log_file");
		my $glimmer_dir = "$cluster_dir/glimmer";
		system("$exec_dir/call_glimmer_parallel.sh  $glimmer_dir  $num   2>&1 |cat >> $log_file");
	
		system("echo 'XXXXX running glimmer finished XXXXXX\n' >> $log_file");
		my $end_glimmer_time = time;
		my $glimmer_time = $end_glimmer_time - $start_glimmer_time;
		system("echo 'Glimmer run time = $glimmer_time sec' >> $log_file");
		$process_fail = "Glimmer does not work! This case is terminated!\n";
		$process = "Running Glimmer-3.02 is done!\n";
		check_success_fail_exit($num, "$num.predict", $process, $process_fail, $process_file, $log_file, $process_fail);
	
		write_process_file($process_file, "Generating ptt file....\n");
		system("perl $change_to_ptt_exec $num.fna $num.predict  $num.ptt");
		$process_fail = "Ptt file is not generated! Case failed!\n";
		$process = "Ptt file has been generated!\n";
		$msg="No CDS position is detected! Please check your gbk file or FASTA sequence file! This case is terminated! You can try FASTA file if the file you submitted is in gbk format.";
		check_success_fail_exit($num, "$num.ptt", $process, $process_fail, $process_file, $log_file, $msg);
	}else{
		system("echo '$num.ptt file is there already' >>$log_file");		
	}
	if (!-e "$num.faa"){
		write_process_file($process_file, "Generating faa file......\n");
		system("perl $change_to_protein_exec $num.fna $num.predict $num.faa");
		$process_fail = "Faa file is not generated! Case failed!\n";
		$process = "Faa file has been generated!\n";
		$msg="No CDS position is detected! Please check your gbk file or FASTA sequence file! This case is terminated! You can try FASTA file if the file you submitted is in gbk format.";
		check_success_fail_exit($num, "$num.faa", $process, $process_fail, $process_file, $log_file, $msg);
	}else{
		system("echo '$num.faa file is there already' >> $log_file");
	}
	
}else{ # we have gi number or accession number or GBK file
	write_process_file($process_file, "Generating fna file....\n");
	if (!(-s "$num.fna")){
		system("perl $exec_dir/gbk2fna.pl $num.gbk ") ; #create $num.fna file
		$process_fail = "Fna file has not been generated! Case failed!\n";
		$process = "Fna file has been generated!\n";
		$msg="No nucleotide sequence is detected! Please check your gbk file or FASTA sequence file! This case is terminated!";
		check_success_fail_exit($num, "$num.fna", $process, $process_fail, $process_file, $log_file, $msg);
	}else{
		system("echo '$num.fna exist!' >> $log_file");
	}
	

	write_process_file($process_file, "Generating ptt file....\n");
	if (!(-s "$num.ptt")){
		system("perl $exec_dir/gbk2ptt.pl $num.gbk > $num.ptt");
		system("perl $exec_dir/clean_empty_PID_lines.pl $num.ptt  $num.fna ");
		$process_fail = "Ptt file has not been generated! Case failed!\n";
		$process = "Ptt file has been generated!\n";
		$msg ="No CDS position is detected! Please check your gbk file or FASTA sequence file! This case is terminated!";
		check_success_fail_exit($num, "$num.ptt", $process, $process_fail, $process_file, $log_file, $msg);
	}else{
		system("echo '$num.ptt exist!' >> $log_file");
	}
	
	
		
	write_process_file($process_file, "Generating faa file....\n");
	if (!(-s "$num.faa")){
		system("perl $exec_dir/gbk2faa.pl $num.gbk >/dev/null") ; #create $num.faa file
		$process_fail = "Faa file has not been generated! Case failed!\n";
		$process = "Faa file has been generated!\n";
		$msg ="No CDS position is detected! Please check your gbk file or FASTA sequence file! This case is terminated!";
		check_success_fail_exit($num, "$num.faa", $process, $process_fail, $process_file, $log_file, $msg);
	}else{
		system("echo '$num.faa exist!' >> $log_file");
	}
}

	
system("echo '\nXXXXX running phage finder: Phage_Finder.sh  $num XXXXX' >> $log_file");
write_process_file($process_file, "BLASTing against virus database......\nRunning tRNAscan-SE......\nRunning aragorn......\n");
system("pwd >> $log_file");
my @childs=();
for(my $i=1; $i<=6; $i++){
	my $pid = fork();
	if (not defined $pid) {
	      system("echo Cannot fork!! >>$log_file");
	}elsif ($pid ==0){
		# i am a child
		if ($i ==1){
			system("pwd >> $log_file");
       		system("Phage_Finder.sh  $num  $cluster_side_virus_database  2>&1 |cat >> $log_file.2") if (!-e  "ncbi.out");
			system("touch ncbi.done") if (-e "ncbi.out");
			exit;
		}
		if ($i==2){
			tRNA_tmRNA($num, $log_file);
			exit;
		}
		if ($i==3){
            # sometimes tRNAscan-SE runs forever. here sets up a time counter.
            # if time is over, kill the process of tRNAscan-SE
			counter($tRNAscan_tolerate_time, "tRNAscan-SE", "tRNAscan.not_done", "tRNAscan.done", "$log_file.3");
            exit;
        }

		if ($i==4){
            #sometimes Phage_Finder.sh runs forever on the cluster side. here set up a time counter
            # fi time is over, kill the process of Phage_Finder.sh
			counter($Phage_Finder_tolerate_time, "Phage_Finder\.sh", "ncbi.not_done", "ncbi.done", "$log_file.3");
            exit;
        }	
		if ($i==5){
			system("pathogenicity.sh $num  $cluster_side_pathogenicity_db 2>&1|cat >$log_file.4");
			system("touch  pathogenicity.done ") if (-e "pathogenicity.out");
			exit;
		}	
		if ($i==6){
			# pathogenicity  runs forever on the cluster side. here set up a time counter
            # fi time is over, kill the process
			counter($Phage_Finder_tolerate_time, "pathogenicity\.sh", "pathogenicity.not_done", "pathogenicity.done", "$log_file.3");
            exit;
		}
		exit(0);
	}else{
		# I am parent
		push @childs, $pid;
	}
}
foreach my $c (@childs){
	waitpid($c, 0);
}
#system("perl $exec_dir/get_integrase.pl");
system("cat  $log_file.2 >> $log_file; rm -rf $log_file.2") if (-e "$log_file.2");
system("cat  $log_file.4 >> $log_file; rm -rf $log_file.4") if (-e "$log_file.4");

check_tRNA_tmRNA_output($log_file, "tRNAscan.out", "tRNAscan.not_done", "tmRNA_aragorn.out");

system("echo 'XXXXX finish running phage finder  XXXXX\n' >> $log_file");
my $phage_finder_time = time;
$time = $phage_finder_time - $start_time;
system("echo  When finish phage_finder, time = $time >> $log_file");

if (!(-s "ncbi.out")){
	$process_fail = "There is not BLAST hit found in the virus database!\n";
	$process = "BLASTing against virus databse is done!\n";
	check_wirte_process_file("ncbi.out", $process_file, $process, $process_fail);
}

my $NC='N/A';my $gi='N/A';
if (-e "$num.gbk"){
	my $data =`cat $num.gbk`;
	if($data=~/\nACCESSION\s+(\S+)\nVERSION\s+\S+\s+GI:(\d+)/s){
		$NC=$1;$gi=$2;
	}
}else{
	$NC="NC_000000";
}
if (!(-d "$tmp_dir/$num/$NC\_dir")){
	system("mkdir -p $tmp_dir/$num/$NC\_dir");
}
chdir "$tmp_dir/$num/$NC\_dir";
system("echo 'Now fork phage and pathogenicity parts, log files are .log.phage and .log.pathogenicity' >> $log_file");
@childs=();
for(my $i=1; $i<=4; $i++){
    my $pid = fork();
    if (not defined $pid) {
          system("echo Cannot fork!! >>$log_file");
    }elsif ($pid ==0){
		# i am a child
        if ($i ==1){
			look_for_phage($num, $flag, "$log_file.phage", "$process_file.phage");
			exit;
		}
		if ($i==2){
			look_for_pathogenicity($num, $flag, "$log_file.pathogenicity", "$process_file.pathogenicity");
			exit;
		}
	}else{
		# I am parent
        push @childs, $pid;
	}
}
foreach my $c (@childs){
    waitpid($c, 0);
}


if (-e "$tmp_dir/$num/$num.fna"){
    my $content=`cat $tmp_dir/$num/$num.fna`;
    if ($content=~/>(.*?)\n/s){
        my $desc=$1;
        if ($desc=~/gi\|(\d+)\|ref\|(.*?)\|/){
            $NC= $2;
            $gi = $1;
        }
        $desc =~s/DEFINITION//si;
        $desc =~s/gi\|\d+\|\s*//si;
        $desc =~s/ref\|(.*?)\|\s*//si;
        system("echo '$desc' >  $tmp_dir/$num/$num.done");
        system("echo 'ACCESSION: $NC' >> $tmp_dir/$num/$num.done");
        system("echo 'GI: $gi' >> $tmp_dir/$num/$num.done");
    }
}

#mark finished 
if (!(-e "$tmp_dir/$num/fail.txt") && (-e "$tmp_dir/$num/true_defective_prophage.txt")){
    system("touch $tmp_dir/$num/success.txt");
    my $date = `date`;
    if (-e "$tmp_dir/$num/tRNAscan.not_done"){
        system("echo '$num  $date  success  tRNAscan not working' >> $tmp_dir/case_record.log");
    }else{
        system("echo '$num  $date  success' >> $tmp_dir/case_record.log");
    }

};
#cleanup
#cleanup($num);
system("echo 'Program exit!' >> $log_file");

exit;

sub cleanup{
    my $num=shift;
    make_done_file($num);
    build_png_input_png_input_RNA($num); # create png_input file
    my $last_time = time;
    $time = $last_time - $start_time;
    system("echo  Normal time = $time >> $log_file");
    write_process_file($process_file, "Program finished!\n");

    system("echo 'rm -rf $running_case_file' >>$log_file");
    unlink  $running_case_file;
    make_browse($num);
    system("echo '$exec_dir/transfer_to_remote_result_tmp.sh   $tmp_dir/$num' >> $log_file");
    system("$exec_dir/transfer_to_remote_result_tmp.sh   $tmp_dir/$num $num");
    chdir "$tmp_dir/$num";
    system("echo 'In $tmp_dir/$num, rm *gbk  *ptt *fna *faa* *ncbi.out*  tRNA*  tmRNA*  *_dir' >> $log_file");
    system("rm  -rf *gbk  *ptt *fna *faa* *ncbi.out*  tRNA*  tmRNA*  *_dir");
    system("perl $exec_dir/cleanup.pl  $tmp_dir 2>&1|cat >>$log_file");
}

sub make_browse{
    my $num =shift;
    if ($num !~ /^1/ ||  length($num) !=10){
        unlink "$tmp_dir/make_browse_list"; 
        system("echo 'remove $tmp_dir/make_browse_list' >> $log_file") if (!-e "$tmp_dir/make_browse_list");
        system("echo 'perl $exec_dir/make_browse.pl' >> $log_file");
        system("perl $exec_dir/make_browse.pl >> $log_file")==0 or system("echo 'Error:$!' >> $log_file"); # make browse.html
    }
}   
    
sub make_done_file{
    my $num = shift;
    if (-e "$tmp_dir/$num/$num.fna"){
        my $content=`cat $tmp_dir/$num/$num.fna`;
        if ($content=~/>(.*?)\n/s){
            my $desc=$1;
            if ($desc=~/gi\|(\d+)\|ref\|(.*?)\|/){
                $NC= $2;
                $gi = $1;
            }
            $desc =~s/DEFINITION//si;
            $desc =~s/gi\|\d+\|\s*//si;
            $desc =~s/ref\|(.*?)\|\s*//si;
            system("echo '$desc' >  $tmp_dir/$num/$num.done");
            system("echo 'ACCESSION: $NC' >> $tmp_dir/$num/$num.done");
            system("echo 'GI: $gi' >> $tmp_dir/$num/$num.done");
        }
    }
    if (-e "$tmp_dir/$num/$num.done"){
        system("echo '$tmp_dir/$num/$num.done generated' >>$log_file");
    }else{
        system("echo '$tmp_dir/$num/$num.done Not generted' >> $log_file");
    }
}

sub look_for_pathogenicity{
	my ($num, $flag, $log_file, $process_file)=@_;
	my $t1=time;
	my $diff='';
	my $cm = "perl $exec_dir/scan_old.pl -n ../$num.fna  -a ../$num.faa  -t  ../tRNAscan.out -m ../tmRNA_aragorn.out  -b ../pathogenicity.out  -p ../$num.ptt  -use 5 -db $local_pathogenicity_database -Deps $gap"; 
    $cm .= " -g ../$num.gbk" if (-e "../$num.gbk");
    system("echo '$cm'>> $log_file");
    system("$cm >$NC\_pathogenicity.txt  2>>$log_file")==0 or system("echo $! >> $log_file");
	my $t2 = time;
    $diff = $t2 - $t1;
    system("echo  'done scan_old.pl, tim = $diff' >> $log_file");

	my $process_fail = "There is no pathogenicity island regions found!\n";
    my $process = "Looking for pathogenicity is done!\n";
    check_success_fail_exit($num, "$NC\_pathogenicity.txt" , $process, $process_fail, $process_file, $log_file, "No pathogenicity island detected!");
	
	system("perl  $exec_dir/non_hit_region_pro_to_ptt.pl ../$num.faa  $NC\_pathogenicity.txt ../$num.faa.non_hit_pro_pathogenicity  2>&1 |cat >> $log_file")==0 or system("echo $! >> $log_file");

	if ($flag eq '-s' && !-e "../pathogenicity.out.non_hit_pro_region"){
        # we need to get the blast result for non hit region proteins first
        write_process_file($process_file, "BLASTing non-hit proteins of pathogenicity island against bacterial database......\n");
        system("echo Parallel BLASTing on bacterial database for non-hit proteins... >> $log_file");
        my $blast_b_dir="$cluster_dir/blast_pb";
		$cm ="$exec_dir/call_remote_blast.sh  $blast_b_dir  ../$num.faa.non_hit_pro_pathogenicity  $cluster_side_bac_database ../pathogenicity.out.non_hit_pro_region ";
		system("echo '$cm' >> $log_file");
		system("$cm  2>&1 |cat >> $log_file");
        $process_fail = "There is no BLAST hit for non-hit proteins in the bacterial database!\n";
        $process = "BLASTing non-hit proteins against bacterial database is done!\n";
        check_wirte_process_file("../pathogenicity.out.non_hit_pro_region", $process_file, $process, $process_fail);
		my $t3 = time;
	    $diff = $t3 - $t2;
    	system("echo  'done call_remote_blast.sh, tim = $diff' >> $log_file");
	}	
	my $t3_1=time;
	write_process_file($process_file, "Annotating proteins in regions found ......\n");
	my $command="perl $exec_dir/annotation.pl $NC $num  $local_pathogenicity_database  $local_bac_database ../pathogenicity.out.non_hit_pro_region $NC\_pathogenicity.txt $flag";
    system("echo '$command' >> $log_file");
    system("$command  2>&1 |cat >> $log_file");
    $process = "Annotating proteins in regions found  is done!\n";
    write_process_file($process_file, $process);
	my $t4 = time;
    $diff = $t4 - $t3_1;
    system("echo  'done call_remote_blast.sh, tim = $diff' >> $log_file");

	system("perl $exec_dir/extract_protein.pl  $num  $NC\_pathogenicity.txt  extract_result_pathogenicity.txt"); #create file 'extract_result.txt' and "NC_XXXX.txt";
    print_msg("extract_result_pathogenicity.txt", $log_file);

	#now use filter to get result files.
    write_process_file($process_file, "Generating summary file ......\n");
	$command = "perl $exec_dir/get_true_region.pl $NC\_pathogenicity.txt  extract_result_pathogenicity.txt  true_pathogenicity.txt";
    system("echo '$command' >> $log_file");
    system($command)==0 or system("echo $! >> $log_file"); # create true_defective_prophage.txt
    system("cp true_pathogenicity.txt  ../summary_pathogenicity.txt");
    $process_fail = "There is no summary file generated!\n";
    $process = "Summmary file is generated!\n";
    check_success_fail_exit($num, "true_pathogenicity.txt" , $process, $process_fail, $process_file, $log_file, "There is no summary file generated!");
	my $t5 = time;
    $diff = $t5 - $t4;
    system("echo  'done extract_protein.pl get_true_region.pl, tim = $diff' >> $log_file");

    # get image
    ### make png format image
    if (-s 'extract_result_pathogenicity.txt'){
        system("cp extract_result_pathogenicity.txt ../detail_pathogenicity.txt");
        write_process_file($process_file, "Generating image file......\n");
		my $cm ="perl $exec_dir/make_png.pl extract_result_pathogenicity.txt  true_pathogenicity.txt  png_input_pathogenicity";
		system("echo '$cm' >> $log_file");
        system("$cm  >> $log_file"); # create file png_input  && 'image.png'
        system("cp png_input_pathogenicity   ../.");
		my $t6 = time;
        $diff = $t6 - $t5;
        system("echo  'done make_png.pl, tim = $diff' >> $log_file");

	}
	my $t7 = time;
	$diff= $t7 - $t1;
	system("echo  'totally done this part, tim = $diff' >> $log_file");
}

sub look_for_phage{
	my ($num, $flag, $log_file, $process_file)=@_;
	my $t1=time;
	my $diff='';
	##### Rah's part###########
	write_process_file($process_file, "Looking for phage-like regions......\n");
	my $cm = "perl $exec_dir/scan.pl -n ../$num.fna  -a ../$num.faa  -t  ../tRNAscan.out -m ../tmRNA_aragorn.out  -b ../ncbi.out  -p ../$num.ptt  -use 5 -db $local_virus_database" ;
	$cm .= " -g ../$num.gbk" if (-e "../$num.gbk");
	system("echo '$cm'>> $log_file");
	system("$cm >$NC\_phmedio.txt  2>>$log_file")==0 or system("echo $! >> $log_file");
	my $t2 = time;
    $diff = $t2 - $t1;
    system("echo  'done scan.pl, tim = $diff' >> $log_file");

	my $process_fail = "There is no phage-like regions found!\n";
	my $process = "Looking for regions is done!\n";
	check_success_fail_exit($num, "$NC\_phmedio.txt" , $process, $process_fail, $process_file, $log_file, "No prophage region detected!");	

	system("perl  $exec_dir/non_hit_region_pro_to_ptt.pl ../$num.faa  $NC\_phmedio.txt ../$num.faa.non_hit_pro_region  2>&1 |cat >> $log_file")==0 or system("echo $! >> $log_file");


# if the input is raw fasta sequence only, we have to get back annotations for phage finder's output result.

	if ($flag eq '-s' && !-e "../ncbi.out.non_hit_pro_region"){
		# we need to get the blast result for non hit region proteins first
		write_process_file($process_file, "BLASTing non-phage-like proteins of hit regions against bacterial database......\n");
		system("echo Parallel BLASTing on bacterial database for non-hit-region-proteins... >> $log_file");
		my $blast_b_dir="$cluster_dir/blast_b";
		system("$exec_dir/call_remote_blast.sh  $blast_b_dir  ../$num.faa.non_hit_pro_region  $cluster_side_bac_database ../ncbi.out.non_hit_pro_region  2>&1 |cat >> $log_file");
		#system("blastall -p blastp -d  $nr_database  -m 8 -e 0.001 -i ../$num.faa.non_hit_pro_region  -o ../ncbi.out.non_hit_pro_region -v 1 -b 1 -a 2 -F F");
		$process_fail = "There is no BLAST hit for non-phage-like proteins in the bacterial database!\n";
		$process = "BLASTing non-phage-like proteins against bacterial database is done!\n";
		check_wirte_process_file("../ncbi.out.non_hit_pro_region", $process_file, $process, $process_fail);
		my $t3 = time;
	    $diff = $t3 - $t2;
    	system("echo  'done call_remote_blast.sh, tim = $diff' >> $log_file");

	}	
	my $t3_1=time;
	write_process_file($process_file, "Annotating proteins in regions found ......\n");
	my $command="perl $exec_dir/annotation.pl $NC $num  $local_virus_database  $local_bac_database ../ncbi.out.non_hit_pro_region $NC\_phmedio.txt $flag";
	system("echo '$command' >> $log_file");
	system("$command  2>&1 |cat >> $log_file");	
	$process = "Annotating proteins in regions found  is done!\n";
	write_process_file($process_file, $process);
	my $t4 = time;
    $diff = $t4 - $t3_1;
    system("echo  'done call_remote_blast.sh, tim = $diff' >> $log_file");

	system("perl $exec_dir/extract_protein.pl  $num  $NC\_phmedio.txt  extract_result.txt"); #create file 'extract_result.txt' and "NC_XXXX.txt";
	print_msg("extract_result.txt", $log_file);
	
	#now use filter to get result files.
	write_process_file($process_file, "Generating summary file ......\n");
	$cm = "perl $exec_dir/get_true_region.pl $NC\_phmedio.txt  extract_result.txt  true_defective_prophage.txt";
	system("echo '$cm' >> $log_file");
	system($cm)==0 or system("echo $! >> $log_file"); # create true_defective_prophage.txt
	system("cp true_defective_prophage.txt  ../.");
	system("cp true_defective_prophage.txt  ../summary.txt");
	$process_fail = "There is no summary file generated!\n";
	$process = "Summmary file is generated!\n";
	check_success_fail_exit($num, "true_defective_prophage.txt" , $process, $process_fail, $process_file,$log_file, "There is no summary file generated!");
	my $t5 = time;
    $diff = $t5 - $t4;
    system("echo  'done extract_protein.pl and get_true_region.pl, tim = $diff' >> $log_file");

	# get image
	### make png format image
	if (-s 'extract_result.txt'){
		system("cp extract_result.txt ../detail.txt");
		write_process_file($process_file, "Generating image file......\n");
		my $cm = "perl $exec_dir/make_png.pl extract_result.txt  true_defective_prophage.txt  png_input";
		system("echo '$cm' >>$log_file");
		system("$cm  >> $log_file"); # create file png_input  && 'image.png'
		print_msg("image.png", $log_file);
		system("cp png_input  image.png  ../.");
	
		$process_fail = "There is no image file generated!\n";
		$process = "Image file is generated!\n";
		check_wirte_process_file("image.png", $process_file, $process, $process_fail);

		#system("perl $exec_dir/make_png.pl png_input1  1 >/dev/null"); #create file 'image1.png'
		#print_msg("image1.png", $log_file);
		#system("perl $exec_dir/parse_true_prophage.pl "); # create file 'true_prophage.txt';
		#print_msg("true_prophage.txt", $log_file);
		#system("perl $exec_dir/get_protein_protein.pl "); # create file 'true_prophage_phage_protein_ORF_position_compare.txt';
		#if (-z 'true_prophage_phage_protein_ORF_position_compare.txt') {
		#	system("echo No true prophage regions detected! No ORF comparion! >> true_prophage_phage_protein_ORF_position_compare.txt");
		#}
		#print_msg("true_prophage_phage_protein_ORF_position_compare.txt", $log_file);
		my $t6 = time;
	    $diff = $t6 - $t5;
    	system("echo  'done make_png.pl, tim = $diff' >> $log_file");
		 
	}

=pod 
	### make cgview part files
	if (-e "$NC.txt"){
		system("perl $exec_dir/region_to_ptt.pl  ../$num.ptt  $NC.txt  png_input ../region.ptt");
		print_msg("../region.ptt", $log_file);
		chdir "$tmp_dir/$num";
		system("java -jar /home/phast/public_html/cgview/cgview.jar -i region.ptt -s region_series -A 10");
		print_msg("region_series/index.html", $log_file);
		system("perl $exec_dir/change_link.pl  $tmp_dir/$num ");
	}
=cut
	my $t7 = time;
    $diff = $t7 - $t1;
    system("echo  'totally done this part, tim = $diff' >> $log_file");

}

sub HTML_error{
	my $msg=shift;
	&HTML_Header_results;
	print "$msg";
	&HTML_Footer_results;
	exit(0);
	
}

sub HTML_Page
{
	my $num=shift;
	my $found = shift;
	my $results_link = "Results.cgi?num=$num";
	print "content-type: text/html \n\n";
	my $inter_cont = '';
	my $fresh_time='';
	if ($found ==0){
		$inter_cont = "Loading the file now. This will take 2-20 seconds. Once completed, calculation will be conducted...
		<br/><br/>
		You will automatically be redirected to your <a href=\"$results_link\">results/processing</a> page...<br>
		<br><br><br><br><br>";
		$fresh_time = 5;
	}else{
		$inter_cont ="Result found in the previous case!<br><br><br><br><br>";
		$fresh_time = 3;
	}
	
	print <<HTML

	
		<HTML>
		<HEAD>
		<TITLE>PHAST results</TITLE>
		<meta http-equiv="REFRESH" content="$fresh_time;url=$results_link">
		</HEAD>
		<BODY BGCOLOR="white">
		<CENTER>
		<IMG SRC="/phage/image/phage.png" height=125 width=500>
		</CENTER>
		<LEFT>
		<br><br><br><br><br>
		<P ALIGN="LEFT">
			<IMG SRC="/phage/image/blueline.gif" WIDTH="540" HEIGHT="7"
			 SGI_SRC="$BASE_ROOT/image/blueline.gif">
		</P>
		$inter_cont
		<P ALIGN="LEFT">
		<IMG SRC="/phage/image/blueline.gif" WIDTH=540 HEIGHT=7
			SGI_SRC=\"$BASE_ROOT/image/blueline.gif\">
		</P>
		<font size=2>
		"Please report bugs and send your comments to:
	    	"<a href=\"mailto:info\@gchelpdesk.ualberta.ca\">Canadian Bioinformatics Help Desk</a>  
	    	</font>
		</LEFT>
	  	</BODY>
	  	</HTML>
HTML

	
}

sub GetRemote
{
   	my $infile =shift;
  	 my $num = shift;
   	#print $input;
   	binmode $infile;

   	open(LF,">$tmp_dir/$num/$num.fna");
   	binmode LF;
   	print LF <$infile>;
   	close(LF);

}

sub check_file{
	my $f = shift;
	$data = `cat $tmp_dir/$num/$f.gbk`;
	$data =~/\nORIGIN(.*)\/\//s;
	$data = $1;
	$data =~s/[\s\n\datgc]//si;
	if (length($data)!=0){
		print "<p> Your input file is not a DNA genome file. Please check! </P>\n";
	   	exit(1); 
	}
}

sub check_input{
	my $flag=shift;
	if ($flag !=1){
   		HTML_error("Error: Only one of accession number, gi number, or your file can be input once.");
	}
}

sub get_gbk_file{
    my ($num, $flag, $log_file, $tmp_dir)= @_;
    my $tmp = `ssh -i $ssh_key $host "ls /home/prion/phage/result_tmp/$num/$num.gbk"`;   
    if (defined $tmp && $tmp ne ''){
        `scp -i $ssh_key $host:/home/prion/phage/result_tmp/$num/$num.gbk .`;   
        system("echo 'Got $num.gbk from $host' >> $log_file");
    }else{  
        system("perl $exec_dir/get_gbk.pl $num  $tmp_dir/$num 2>&1|cat >> $log_file.2"); # create $num.gbk file
        system("cat $log_file.2 >> $log_file; rm -rf $log_file.2") if (-e "$log_file.2");
    }
    print_msg("$num.gbk", $log_file);
    
}

# chech the list file if the number is there.
sub check_list_file{
	my $num=shift;
	my $data = `cat $tmp_dir/list`;
	my $found = 0;
	if ($data =~/$num/s){
		$found = 1;
	}
	return $found;
}

sub print_msg{
	my ($file, $log_file)= @_;
	if (-s $file ){
		system("echo  $file is created ! >> $log_file");
	}else{
		system("echo  $file is not created ! >> $log_file");
	}
}

sub HTML_Footer_results
{
	print <<HTML
	<br/><br/><br/><br/><br/><br/>
	<P ALIGN="LEFT">
		<IMG SRC="/phage/image/blueline.gif" WIDTH="540" HEIGHT="7"
		 SGI_SRC="$BASE_ROOT/image/blueline.gif">
	<br/>
	Problems? Suggestions? Please contact <A
	 HREF="mailto:info\@gchelpdesk.ualberta.ca">Canadian Bioinformatics Help Desk</A>
	 <!--or&nbsp; <A
	 HREF="mailto:david.wishart\@ualberta.ca">David Wishart
	 </A>-->
	</P>
	
	</font>
	</BODY>
  	</HTML>
HTML
}

sub HTML_Header_results
{
	print "content-type: text/html","\n\n";
	print <<HTML
	<HTML>
  	<HEAD>
  	<TITLE>hage Lookup results </TITLE>
  	</HEAD>
	<BODY BGCOLOR="white">
	<CENTER>
	<IMG SRC="/phage/image/phage.png" height=125 width=500>
	</Center>
	<BR><BR><BR><BR><BR>
	<P ALIGN="LEFT">
		<IMG SRC="/phage/image/blueline.gif" WIDTH="540" HEIGHT="7"
		 SGI_SRC="$BASE_ROOT/image/blueline.gif">
	</P>
  	<BODY>
HTML
}

sub check_success_fail_exit{
    my ($num, $file,$process, $process_fail,  $msg)=@_;
    print_msg($file, $log_file);
    check_wirte_process_file($file, $process_file, $process, $process_fail);
    if (!(-s $file)){
        system("echo '$msg' >> $tmp_dir/$num/fail.txt");
        system("touch $tmp_dir/$num/summary.txt");
        system("touch $tmp_dir/$num/detail.txt");
        my $date = `date`;
        system("echo '$num  $date fail $msg' >> $tmp_dir/case_record.log");
        #cleanup($num);
        system("echo 'Program exit!' >> $log_file");
        exit;
    }
}
# output process file for tracing the process of the case. 
sub write_process_file{
	my ($process_file, $msg)=@_;
	open(OUT, ">>$process_file") or die "Cannot write $process_file";
	print OUT $msg;
	close OUT;
}

# check file's existence, and output process file
sub check_wirte_process_file{
	my ($file, $process_file, $success_msg, $fail_msg)=@_;
	if (-s $file) {
		write_process_file($process_file, $success_msg);
	}else{
		write_process_file($process_file, $fail_msg);	 
	}
}

# if png_input file is empty, create one
sub build_png_input_png_input_RNA{
	my $num=shift;
	my $log_file =shift;
	my $empty=0;
	my $cur_dir = getcwd;
	chdir "$tmp_dir/$num";
	if(-e "png_input"){
		my $data =`cat png_input`;
		$data=~s/\n\s//g;
		if ($data !~/section/ ){
			$empty=1;
		}
	}else{
		$empty = 1;
	}
	system("echo 'empty=$empty' >> $log_file");
	my $head_line = '';
	if ((-e "$num.fna") && $empty==1){
		system("echo 'Rebuild png_input' >> $log_file");
		open (IN98, "$num.fna");
		my $header = '';
		my $seq ='';
		while (<IN98>){
			chomp($_);
			if ($_=~/^>gi\|.*?\|ref\|(\S+?)\|(.*) /){
				my $NC=$1;
				my $name = $2;
				$NC=~s/\..*//;
				$name=~s/[\r\n]//g;
				$header = ">$name [asmbl_id: $NC]. "; 
				$head_line = $header;
			}else{
				$seq .= $_;
			}				
		}
		close IN98;
		$seq =~s/\s//g;
		$header .= length($seq);
		open (OUT11, ">png_input");
		print OUT11 $header."\n";
		print OUT11 "         from       to      strand      match      protein_name     EVALUE     protein_sequence\n";
		close OUT11;

	}
	if (!(-e "png_input_RNA") && (-e "$num.fna")){
		open(OUT, ">png_input_RNA");
		print OUT $head_line."\n";
		print OUT "         from       to      strand      match      RNA_name     EVALUE     match_sequence\n";
		close OUT;
	}
	if (!(-e "$num.fna")){
		system("echo 'No fna file, no png_input and png_input_RNA created!'>> $log_file");
	}
	chdir $cur_dir;
}

# check the gbk file. if there is CDS and translation sections, we keep it.
# if there is CDS and no translation sections , we make translation section into it.
# if there is no CDS and no translation sections, we make fna file .
sub check_gbk_file{
	my ($gbk_file, $num, $process_file)=@_;
	open (IN, $gbk_file);
	my $translation_flag  = 0;
	my $db_xref_flag =0;
	my $CDS_flag= 0;
	my $seq_flag =0;
	my $seq ='';
	my %hash =();
	my $desc = '';
	my $NC='';
	my $GI='';
	while (<IN>){
		chomp($_);
		if ($_=~/^ACCESSION\s+(.*)/){
			$NC=$1;
		}
		if ($_=~/^VERSION.*?GI:(\d+)/){
			$GI = $1;
		}
		if ($_=~/^DEFINITION\s+(.*)/){
			$desc=$1;
		}
		if ($_=~/^\s*CDS\s+(\S+)/ ){
			$hash{$1}='';
			$CDS_flag = 1;
		}elsif($_=~/^\s+\/db_xref="GI:/){
			$db_xref_flag = 1;
		}elsif($_=~/^\s+\/translation="/){
			$translation_flag = 1;	
		}elsif($_=~/^ORIGIN/){
			$seq_flag = 1;
			next;
		}elsif($_=~/^\/\//){
			$seq_flag = 0;
		}		
		if ($seq_flag==1){
			$_=~s/[\d\s\n]//g;
			$seq .= $_;
		}
	}
	close IN;
	if ($seq ne ''){
		$seq_flag=1;
	}

	my $DNA_seq = $seq;
	$DNA_seq =~s/[ACGTRYKMSWBDHVN]//sig;
	if ($DNA_seq ne ''){
		$DNA_seq=~s/(\w{0,60})/$1\n/gs;
		$process_fail = "Wrong DNA seq = $DNA_seq in gbk file! We accept 'ACGTRYKMSWBDHVN' only.\nProgram terminated!\n";
		$process = "";
		check_success_fail_exit($num, "NA", $process, $process_fail, $process_file, $log_file, $process_fail);	
	}

	system("echo 'seq_flag=$seq_flag, CDS_flag=$CDS_flag, translation_flag=$translation_flag' >> $log_file");
	if ($seq_flag ==0){
		my $content= `cat $gbk_file`;
		$process_fail = "There is no ORIGIN part in GBK file. That is weird.\n\n<pre>$content</pre>";
	        $process = "There is DNA part in GBK file.\n";
        	check_success_fail_exit($num, "NA", $process, $process_fail, $process_file, $log_file, $process_fail);

	}	
	elsif ($seq_flag==1 && $CDS_flag ==1 && ($db_xref_flag==0 or $translation_flag ==0) ){ # there is CDS section
		system("echo 'Generate translation section for gbk file' >> $log_file");
		$seq = uc($seq);	
		foreach my $k (keys %hash){
			$hash{$k} = get_seq(\$seq, $k);
		}
		open (IN, $gbk_file);
		open (OUT, ">$gbk_file.tmp");
		my $key = '';
		my $flag =0;
		my $gi_count=0;
		while(<IN>){
			if ($_=~/^\s*CDS\s+(\S+)/ ){
				$key = $1;
				$flag = 1;	 
				print OUT $_;
				if ($db_xref_flag==0){ # no db_xref
                                        $gi_count++;
                                        print OUT "                     \/db_xref=\"GI:$gi_count\"\n";
                                }
				next;
			}
			if ($_!~/^\s+\// && $key ne '' && $flag ==1){
				if ($translation_flag ==0){# no translation
					print OUT "                     \/translation=\"$hash{$key}\"\n";
				}
				$key ='';
				$flag =0;
			}
			print OUT $_;
		}
		close IN;
		close OUT;
		system("cp -f $gbk_file.tmp $gbk_file");
		$translation_flag = 1;
	}
	elsif ($seq_flag==1 && $CDS_flag ==0 && $translation_flag ==0){# no translation and no CDS
		system("echo 'Make fna file from gbk file' >> $log_file");
		$seq = uc($seq);
		my @arr= $seq =~/\w{0,60}/g;
		open (OUT, ">$num.fna");
		chomp($desc);
		print OUT ">gi|$GI|ref|$NC| $desc\n";
		foreach my $l (@arr){
			print OUT $l."\n";
		}
		close OUT;
	}else{
		system("echo 'Keep gbk file intact' >> $log_file");
	}
	return $translation_flag;
}

sub  get_seq{
	my ($seq, $locations )=@_;
	my $seq3 = '';
	my $start_num='';
	my $end_num = '';
	my $len='';
	my @arr =();
	if ($locations=~/complement\(\d+\.\.\d+\)/){
		@arr= $locations=~/complement\(\d+\.\.\d+\)/g;
	}
	if ($locations=~/\d+\.\.\d+/){
		@arr= $locations=~/\d+\.\.\d+/g;
	}
	
	foreach my $l (@arr){
		my $seq2 = '';
		if ($l =~/complement\((\d+)\.\.(\d+)\)/){
			$start_num = $1;
			$end_num = $2;		
			$len = $2-$1+1;
			$seq2 = substr($$seq, $start_num-1, $len);
		
			$seq2 =~ tr/[A,T,C,G,a,t,c,g]/[T,A,G,C,t,a,g,c]/;
			$seq2 = scalar reverse($seq2);
		
		}elsif($l =~/(\d+)\.\.(\d+)/){
			$start_num = $1;
			$end_num = $2;		
			$len = $2-$1+1;
			$seq2 = substr($$seq, $start_num-1, $len);
		}
		$seq3 .= $seq2;
	}
	
	$seq3 = change_to_aa ($seq3);

	return $seq3;
}

# change nucleotide acid to amino acid.
sub change_to_aa{
	my $seq = shift;
	my @arr = $seq=~/\w{0,3}/g;
	foreach my $i (0..$#arr){
		if ($arr[$i]=~/\w\w\w/){
			$arr[$i] =~s/TTT/F/i;$arr[$i] =~s/TTC/F/i;$arr[$i] =~s/TTA/L/i;$arr[$i] =~s/TTG/L/i;$arr[$i] =~s/CTT/L/i;$arr[$i] =~s/CTC/L/i;
			$arr[$i] =~s/CTA/L/i;$arr[$i] =~s/CTG/L/i;$arr[$i] =~s/ATT/I/i;$arr[$i] =~s/ATC/I/i;$arr[$i] =~s/ATA/I/i;$arr[$i] =~s/ATG/M/i;
			$arr[$i] =~s/GTT/V/i;$arr[$i] =~s/GTC/V/i;$arr[$i] =~s/GTA/V/i;$arr[$i] =~s/GTG/V/i;$arr[$i] =~s/TCT/S/i;$arr[$i] =~s/TCC/S/i;
			$arr[$i] =~s/TCA/S/i;$arr[$i] =~s/TCG/S/i;$arr[$i] =~s/CCT/P/i;$arr[$i] =~s/CCC/P/i;$arr[$i] =~s/CCA/P/i;$arr[$i] =~s/CCG/P/i;
			$arr[$i] =~s/ACT/T/i;$arr[$i] =~s/ACC/T/i;$arr[$i] =~s/ACA/T/i;$arr[$i] =~s/ACG/T/i;$arr[$i] =~s/GCT/A/i;$arr[$i] =~s/GCC/A/i;
			$arr[$i] =~s/GCA/A/i;$arr[$i] =~s/GCG/A/i;$arr[$i] =~s/TAT/Y/i;$arr[$i] =~s/TAC/Y/i;$arr[$i] =~s/TAA//i;$arr[$i] =~s/TAG//i;
			$arr[$i] =~s/CAT/H/i;$arr[$i] =~s/CAC/H/i;$arr[$i] =~s/CAA/Q/i;$arr[$i] =~s/CAG/Q/i;$arr[$i] =~s/AAT/N/i;$arr[$i] =~s/AAC/N/i;
			$arr[$i] =~s/AAA/K/i;$arr[$i] =~s/AAG/K/i;$arr[$i] =~s/GAT/D/i;$arr[$i] =~s/GAC/D/i;$arr[$i] =~s/GAA/E/i;$arr[$i] =~s/GAG/E/i;
			$arr[$i] =~s/TGT/C/i;$arr[$i] =~s/TGC/C/i;$arr[$i] =~s/TGA//i;$arr[$i] =~s/TGG/W/i;$arr[$i] =~s/CGT/R/i;$arr[$i] =~s/CGC/R/i;
			$arr[$i] =~s/CGA/R/i;$arr[$i] =~s/CGG/R/i;$arr[$i] =~s/AGT/S/i;$arr[$i] =~s/AGC/S/i;$arr[$i] =~s/AGA/R/i;$arr[$i] =~s/AGG/R/i;
			$arr[$i] =~s/GGT/G/i;$arr[$i] =~s/GGC/G/i;$arr[$i] =~s/GGA/G/i;$arr[$i] =~s/GGG/G/i;
		}else{
			$arr[$i]='';
		}
	}
	return join('',@arr); 

}


sub counter{
	my ($time, $program, $not_done_file, $done_file, $log_file)=@_;
	my $count=0;
	while(1){
		sleep(3);
		$count++;
		if ($count==$time*60/3){ #5 minutes
			system("touch $not_done_file");
			my $process = `ps -x'`;
			foreach my $ps (split("\n", $process)){
				if ($ps =~/^\s*(\d+) .*?$program/){
					system("kill -9 $1");
					system("echo 'kill -9 $1 for $program' >> $log_file");
				}
			}
			last;
		}
		if (-e $done_file){
			last;
		}
	}

}

sub check_tRNA_tmRNA_output{
    my ($log_file, $tRNAscan_out, $tRNAscan_not_done, $tmRNA_aragorn_out)=@_;
    system("echo No tRNAscan.out >> $log_file") if (!(-e "$tRNAscan_out"));
    my $process_fail = "There is no output from tRNAscan-SE!\n";
    my $process = "Running tRNAscan-SE is done!\n";
    $process = "Running tRNAscan-SE is not done!\n" if (-e "$tRNAscan_not_done");
    check_wirte_process_file("tRNAscan.out", $process_file, $process, $process_fail);

    system("echo No tmRNA_aragorn.out >> $log_file") if (!(-e "$tmRNA_aragorn_out"));
    $process_fail = "There is no output from aragorn!\n";
    $process = "Running aragorn is done!\n";
    check_wirte_process_file("tmRNA_aragorn.out", $process_file, $process, $process_fail);
}
sub tRNA_tmRNA{
	my ($num, $log_file)=@_;
	system("echo   'find tRNA sequences ...' >> $log_file");
	system("rm -rf /tmp/tscan*");
	system("tRNAscan-SE -B -o tRNAscan.out $num.fna 2>&1 |cat >> $log_file")==0 or system("echo $! >>$log_file") if (!-e "tRNAscan.out");
	system("touch tRNAscan.done") if (-e "tRNAscan.out");

	system("echo   'find tmRNA sequences ...' >> $log_file");
	system("aragorn -m -o tmRNA_aragorn.out $num.fna  2>&1 |cat >> $log_file")==0 or system("echo $! >>$log_file") if (!-e "tmRNA sequences");
	#extract RNA
	system("echo '$exec_dir/extract_RNA.pl $num  extract_RNA_result.txt.tmp' >> $log_file");
	system("perl $exec_dir/extract_RNA.pl $num  extract_RNA_result.txt.tmp");
	system("echo '$exec_dir/make_RNA_png_input.pl  extract_RNA_result.txt.tmp  png_input_RNA' >> $log_file");
	system("perl $exec_dir/make_RNA_png_input.pl  extract_RNA_result.txt.tmp  png_input_RNA");
}


