#!/usr/bin/perl  -w
use CGI;
use CGI::Carp qw(fatalsToBrowser);
# this program will yield the statistics for user's submission everyday of the current month

my $BASE_ROOT="/var/www/html/phast/current/public";
my $tmp_dir = "$BASE_ROOT/tmp"; # this dir will cotain all the temp files and result files.

my $log_file = "$tmp_dir/case_input.log";
my $log_file2 = "$tmp_dir/case_record.log";
my $day_defined = 0;

my $q=new CGI;
my $month = $q->param('month');
my $day = $q->param('day');
if (defined $day){
	$day_defined = 1;
}
my $year = $q->param('year');

my $date  = `date`; # Wed Oct 15 11:01:30 MST 2014
chomp($date);

my $array1 = "Jan, Mar, May, Jul, Aug, Oct, Dec";
my $array2 = 'Apr, Jun, Sep, Nov';
my $array3 = 'Feb';
my $today = '';
if( $date =~/^\w+\s+(\w+)\s+(\d+).*?(\d+)$/s){
	my $today_flag = 0;
	if (defined $month && $month eq $1 && defined $year && $year==$3){
		$today_flag = 1;
	}
	if (!defined $month){ 
		$month = $1;
	}
	if (!defined $year){ 
		$year = $3;
	}
	if (!defined $day){ 
		if ($array1=~/$month/) {
			$day = 31;
		}elsif ($array2=~/$month/) {
			$day = 30;
		}elsif ($array3=~/$month/) {
			$day = 29;
		}
	}
	if ($today_flag==1){
		$today = $2;
	}else{
		$today = $day;
	}
	
}

my $t_count = 0;
my $t_count2 = 0;
my %dict = ();
my %dict2 = ();
if ($day_defined ==0){
	for (my $i = 1; $i <= $day; $i++){
		my $stat = `grep ' $year' $log_file| grep ' $month ' |grep ' $i ' -c`;
		chomp($stat);
		$dict[$i] = "$year, $month, $i,  count: $stat";
		$t_count += $stat;
		my $stat2 = `grep ' $year' $log_file2| grep ' $month ' |grep ' $i ' -c`;
		my $com_stat = `grep ' $year' $log_file2 -A 1| grep ' $month ' -A 1|grep ' $i ' -A 1|grep 'command_line' -c`;
		chomp($stat2);
		$dict2[$i] = "$year, $month, $i, total count: $stat2, command_line count: $com_stat";
		$t_count2 += $stat2;
	} 
}else{
	my $stat = `grep ' $year' $log_file| grep ' $month ' |grep ' $day ' -c`;
	chomp($stat);
	$dict[$day] = "$year, $month, $day,  count: $stat";
	$t_count += $stat;
	my $stat2 = `grep ' $year' $log_file2| grep ' $month ' |grep ' $day ' -c`;
	my $com_stat = `grep ' $year' $log_file2 -A 1| grep ' $month ' -A 1|grep ' $day ' -A 1|grep 'command_line' -c`;
	chomp($stat2);
	$dict2[$i] = "$year, $month, $i, total count: $stat2, command_line count: $com_stat";
	$t_count2 += $stat2;
}
if ($month eq 'Feb' && $day==29 && $dict[$day]=~/count: 0/){
	$day = 28;
}

print "content-type: text/html \n\n";

if ($day_defined ==0){
	print "$year, $month, total submission count: $t_count<br/>";
	for (my $i = 1; $i <= $day; $i++){
		print "$dict[$i]<br/>";
	}
}else{
	print "$year, $month, $day, total submission count: $t_count<br/>";
	print "$dict[$day]<br/>";
}
print "<br/><br/><br/>";

if ($day_defined ==0){
	print "$year, $month, total case count: $t_count2<br/>";
	for (my $i = 1; $i <= $day; $i++){
		print "$dict2[$i]<br/>";
	}
}else{
	print "$year, $month, $day, total case count: $t_count2<br/>";
	print "$dict2[$day]<br/>";
}
print "<br/>";
my $space = `df -h `; 
print "Available space of hard drive is<br/><pre>$space</pre>";
exit;
