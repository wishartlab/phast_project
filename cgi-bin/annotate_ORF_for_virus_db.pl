#!/usr/bin/perl -w

# get names from Blast_all.db
my $vir_temp_dir = "/home/phast/public_html/phage_finder/DB/temp_vir";
my $bac_temp_dir = "/home/phast/public_html/phage_finder/DB/temp_bac";
my $vir_db_file = "$vir_temp_dir/virus.db";
my $ORF_list_filename = "$vir_temp_dir/virus_ORF_list";
my $bac_databse ="$bac_temp_dir/Bacteria_all.db";
my $identity_threshould= 75;

my $exec_dir = "/home/phast/public_html/cgi-bin";

my $start_time = time;
print `date`;

open(IN, $vir_db_file) or die "Cannot open $vir_db_file";
my %hash=();
my @array=();
my $name= '';
while(<IN>){
	if ( $_=~/^>/){
		$name = $_;
		if ($_=~/\sOrf\d+\s+\[/i or $_=~/\sOrf-\d+\s+\[/i){
			push @array, $_;			
		}
		$hash{$name} ='';
	}else{
		$hash{$name}.= $_;
	}
}
close IN;

open(OUT, ">$ORF_list_filename") or die "Cannot write $ORF_list_filename";
foreach my $a (@array){
	print OUT $a;
	print OUT $hash{$a};
}
close OUT;


my @name_array = ('capsid', 'head', 'integrase', ' int', 'plate', 'tail', 'fiber', 'coat', 'transposase',
		 'portal', 'terminase', 'protease', 'lysis', 'lysin ', 'transcript','regulator',
		 'anti', 'repressor', 'excisionase');

print "running Blast....\n";
system("blastall -p blastp -d  $bac_databse  -m 8 -e 1E-10 -i $ORF_list_filename  -o $ORF_list_filename.blast.out -v 4 -b 4 -a 1 -F F");

my %data_hash=();
open (IN, "$bac_temp_dir/Bacteria_all.db_list") or die "Cannot open $bac_temp_dir/Bacteria_all.db_list";
while (<IN>){
	if ($_=~/>(gi\|\d+\|ref\|.*?\|)(.*)/){
		$data_hash{$1}= $2;
	}
}
close IN;
open (IN, "$ORF_list_filename.blast.out") or die "Cannot open $ORF_list_filename.blast.out";
my @line_array=();
print "looking for the names from /home/phast/public_html/phage_finder/DB/temp_bac/Bacteria_all.db_list ......\n";
while (<IN>) {
	my @arr = split (" ", $_);
	if ($arr[2] < $identity_threshould) {
		next;
	}
	#$arr[1]=~s/\|/\\\|/g;
	$rex= $arr[1];
	
	if (defined $data_hash{$rex}){
		$hit= $data_hash{$rex};
		if ($hit =~/hypothetical/i or $hit=~/predicted/i or $hit=~/putative/){
			next;
		}
		my $found = 0;
		foreach (@name_array){
			if ($hit =~/$_/i){
				$found = 1;
				last;
			}
		}
		if ($found ==0){
			next;
		}
		$rex1 = $rex;
		$rex =~s/\|/\\\|/g;
		$_=~s/$rex/$rex1 $hit/;
		push @line_array, $_;
	}else{
		print "$rex  no hit \n";
		push @line_array, $_;
	}
}
close IN; 
print "size of \@line_array =".scalar(@line_array)."\n";


if (scalar(@line_array)>0){
	my $last_key='';
	my $data1 = `cat $vir_db_file`;
	foreach my $i(0..$#line_array){
		if ($line_array[$i] =~/^(.*?)\t(.*?)\t/){
			my $key = $1;
			my $anno_name = $2;
			if ($key eq $last_key){
				delete $line_array[$i];
			}else{
				$last_key = $key;
				$key =~s/\|/\\\|/g;
				$anno_name =~s/gi\|\d+\|//;
				$anno_name =~s/ref\|.*?\|//;
				$anno_name =~s/\[.*?\]//;
				chomp($anno_name);
				$data1 =~s/($key.*?)\[/$last_key $anno_name \[/s;
				print "$1\n";
				print "    Replaced by '$last_key     $anno_name\n";
			}
		}
	}
	open (OUT, ">$vir_db_file") or die "Cannot write $vir_db_file";
	print OUT $data1;
	close OUT;
	chdir $vir_temp_dir;
	print "Formatting virus.db ......\n";
	system("formatdb -i virus.db");
	if (-s "virus.db"){
		print "Copy virus.db* to upper level\n";
		system("cp virus.db* ../.");
		# propagate to cluster
		print "Propagate virus.db* to cluster\n";
		system("$exec_dir/propogate_database.sh  $vir_db_file");
	}else{
		print "virus.db  has no content. Please check!\n";
	}		
}else{
	print "Nothing to change this time!\n";
}

my $end_time = time;
my $time = $end_time - $start_time;
my $min = int($time /60);
my $sec = $time % 60;
print "running time = $min min $sec sec\n";
print "Program exit!\n\n\n";
exit;


		

