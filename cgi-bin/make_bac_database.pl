#!/usr/bin/perl -w
use  Cwd;

my $exec_dir = "/home/phast/public_html/cgi-bin";
my $tmp_dir = "/home/phast/public_html/phage_finder/DB/temp_bac";

my $start_time = time;
chdir $tmp_dir;
print  `date`;
system("rm -rf *") if (-e "list");

system("wget 'ftp://ftp.ncbi.nih.gov/genomes/Bacteria/all.faa.tar.gz'");
if (-e "all.faa.tar.gz"){
	print "all.faa.tar.gz is downloaded!\n";
}else{
	print "No all.faa.tar.gz is downloaded!\n";
	exit;
}
system("tar -xvzf all.faa.tar.gz > /dev/null");
system("ls >list");

open(IN, "list") or die "Cannot open list";
my $output="Bacteria_all.db";
unlink ($output);
open(OUT, ">$output") or die "Cannot write $output";
my $last ='';
my $curr_dir =getcwd;
my $count=0;
while(<IN>) {
	chomp ($_);
	if ($_ eq 'list' or  $_ eq 'make_bac_database.pl' or $_ eq 'all.faa.tar.gz' or  $_ =~/Bacteria_all\.db/  ){
		next;
	}
	
	chdir $_;
	my $v_name = $_;
	
	my $file_list =`ls`;
	my @arr=split("\n", $file_list);
	foreach my $file (@arr){ 
		chomp($file); 
		$count++;
		#print "     $v_name/$file\n";
		my $data = `cat $file`;
		print OUT $data;
	}
	chdir $curr_dir;

}
close IN;
close OUT;
print "In all.faa.tar.gz, we found $count phage organisms!\n";


system("grep '>' Bacteria_all.db > Bacteria_all.db_list");
if (-s "Bacteria_all.db"){
	#print "Copy Bacteria_all* to upper level\n";
	#system("cp Bacteria_all* ../.");
	# propagate to cluster
	#print "Propagate Bacteria_all* to cluster\n";
	#system("$exec_dir/propogate_database.sh  /home/phast/public_html/phage_finder/DB/temp_bac/Bacteria_all.db");
	#cleanup
	#system("rm -rf *") if (-s "list");
}else{
	print "Bacteria_all.db  has no content. Please check!\n";
}
my $end_time = time;
my $time = $end_time - $start_time;
my $min = int($time /60);
my $sec = $time % 60;
print "running time = $min min $sec sec\n";
print "Program exit!\n\n\n";
exit;


