#!/usr/bin/perl -w


# clean up PID == '-' in .ppt file
#Usage: perl  clean_empty_PID_lines.pl <ptt_file> <fna_file>

if (scalar @ARGV!=2){
	print STDERR "Usage: perl  clean_empty_PID_lines.pl <ptt_file> <fna_file>\n";
	exit(-1);
}

my $ppt_file = $ARGV[0];
my $fna_file = $ARGV[1];
open(IN,$fna_file) or die "Cannot open $fna_file";
my $seq='';
while (<IN>){
	chomp($_);
	if ($_=~/^>/){
		next;
	}else{
		$seq .= $_;
	}
}
close IN;
my $leng_seq = length($seq);
print "seq length = $leng_seq\n";
open (IN, $ppt_file) or die "Cannot open $ppt_file";
open (OUT, ">$ppt_file.tmp") or die "Cannot write $ppt_file.tmp";

while (<IN>){
	if ($_=~/^(\d+)\.\.(\d+)/){
		my $len = $2 -$1 +1;
		if ($len == $leng_seq){
			next;
		}
		my @a = split("\t", $_);
		if ($a[3] eq '-'){
			print $_;
			next;
		}
	}
	print OUT $_;
}
close IN;
close OUT;
system("mv -f $ppt_file.tmp $ppt_file");
exit;
