#!/usr/bin/perl
#
#

#this program is used to view RNA
#
use FindBin qw($Bin);
use CGI;
use CGI::Carp qw(fatalsToBrowser);

my $BASE_ROOT="/apps/phast/project";


my $q=new CGI;

my $id = $q->param('id');
my $file= $q->param('file');
if (!defined $file){
	$file = 'png_input';
}
my $value = '';
if ($file=~/RNA/){
	$value="id=$id&type=RNA&file=$file";
}else{
	$value="id=$id&file=$file";
}
my $long_length=1240;
my $short_length=1050;

HTML_Header_results(1);
print <<HTML;
	<center> 
	<div class="rna-map">
	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
	        width="900" height="1300">
	    <param name="movie" value="/RNA.swf">

	    <!--[if !IE]>-->
	    <object type="application/x-shockwave-flash" data="/RNA.swf" width="900" height="1300">
	    <!--<![endif]-->
	        <param name="FlashVars" value="$value" />
	        <p>Alternative Content</p>
	    <!--[if !IE]>-->
	    </object>
	    <!--<![endif]-->

	</object>
	</div>
	</center>
	</BODY>
HTML

exit;



sub HTML_Header_results
{
    print "content-type: text/html","\n\n";
    print <<HTML
    <HTML>
    <HEAD>
    <TITLE> </TITLE>
    </HEAD>
    <BODY BGCOLOR="white">
    <font face="Arial">
    <CENTER>
    <IMG SRC="../image/phage.png" height=125 width=500><BR><BR>
    </Center>
    <center>
    <table align="center"  border="0" cellpadding="0" cellspacing="0" id="navigation" bgcolor='orange'>
        <tr width="100%" style="font-size:19;font-style:arial;">
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../index.html"><b>Home</b></a>&nbsp;&nbsp;&nbsp;</td>
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../documentation.html"  ><b>Documentation</b></a>&nbsp;&nbsp;&nbsp;</td>
    <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../io.html"  ><b>Input</b></a>&nbsp;&nbsp;&nbsp;</td>
    <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../io1.html"  ><b>Output</b></a>&nbsp;&nbsp;&nbsp;</td>
      <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../how_to_use.html"  ><b>Instructions</b></a>&nbsp;&nbsp;&nbsp;</td>
      <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../Download.html"  ><b>Databases</b></a>&nbsp;&nbsp;&nbsp;</td>
          <td class="navText" align="center" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<a href="../contact.html"  ><b>Contact</b></a>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        </table>
</center>
    <BR><BR>
    <P ALIGN="center">
        <IMG SRC="../image/blueline.gif" WIDTH="1240" HEIGHT="7"
         SGI_SRC="../image/blueline.gif">
    </P>
    </font>
    <BODY>
HTML
}


