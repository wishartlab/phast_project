#!/usr/bin/perl  -w
# this program is used to pack all the case of summary.txt and detail.txt in tmp folder.

use  File::Copy;

chdir "/var/www/html/phast/current/public/tmp";
unlink "zz" if (-e 'zz');
my $data =`find . -type f  -name summary.txt -o -name detail.txt `;
my @arry = split "\n",$data;

open(OUT, "|tar -cvzf phast.tz --files-from ");
foreach my $l (@arry){
	if ($l !~/\/13/){
		print OUT $l."\n";
	}
}
close OUT;
my $c = `tar -tvzf phast.tz|wc -l`;
print "count=$c";
move("phast.tz", "../phage_finder/DB/.");


exit;


