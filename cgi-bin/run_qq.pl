#!/usr/bin/perl -w
# this program will run batch job to call phast back end 
# with the qq file. If there is no running.txt ane there is 
# empty queue.txt file , it will append a job into queue.txt
# from qq list. qq file format "-s  NC_XXXXXX\n";
my $HOSTNAME = $ENV{HTTP_PHAST_CLUSTER_HOSTNAME};
my $USERNAME = $ENV{HTTP_PHAST_CLUSTER_USERNAME};
my $remote_host = $USERNAME . '@' . $HOSTNAME;
my $key="~/.ssh/scp-key";
my $tmp_dir = "/var/www/html/phast/current/public/tmp";
chdir $tmp_dir;
my $lines = `wc -l qq`;
$lines=~s/^(\d+).*/$1/;
print "Total case number is $lines\n";
my $coun=369;
while(1){
	sleep(15);
	#print "After 3 seconds\n";
	open(IN, "qq") or die "qq";
	my $first= '';
	my $rest ='';
	my $flag = 0;		
	while(<IN>){
		chomp($_);
		if ($_=~/^\s*$/){
			next;
		}
		if ($flag ==0){
			$first = $_;
			$flag =1;
			next;
		}
		if ($_ ne ''){
			$rest .="$_\n";
		}
	}
	close IN;
	if ($first ne ''){
		my $q_cont=`cat queue.txt`;
		while (1){
			last if (defined $q_cont);
			sleep(2);
			$q_cont=`cat queue.txt`;
		}
		my $r_cont ='';
		if (-e "running.txt"){
			$r_cont=`cat running.txt`;
		}
		if ($r_cont eq '' && defined $q_cont &&  $q_cont eq ''){
			my $NC='';
			my $flag ='';
			if ($first =~/(-\w)\s+(\S+)/){
				$flag = $1;
				 $NC = $2;
			}
			#if (!-d $NC){
			#	print "There is no dir $NC\n";
			#	next;
			#}
			 open (OUT, ">qq") or die "Cannot write qq";
	                print OUT "$rest";
        	        close OUT;

			if ($flag eq '-g' ){
				unless (-d $NC){
					system("mkdir $NC");
				}
				my $path = `ssh -i $key $remote_host  \"cd /home/prion/phage/DB/temp_genome/.; find -name $NC.gbk\" `;
				chomp($path);
				if ($path =~/$NC/){
					system("scp -i $key $remote_host:/home/prion/phage/DB/temp_genome/$path  $NC/.");
				}
			}	
			if (-s "$NC/$NC.log"){
				unlink  "$NC/$NC.log",  "$NC/fail.txt", "$NC/success.txt",  "$NC/$NC.done";
			}
			$coun++;
			while (1){
				last if (`ps x` !~ /phage\.pl/s);
				sleep(3);
			}
			system("echo '$flag  $NC' >> queue.txt");
			print "push '$flag  $NC' into queue.txt, case $coun, ".`date`."\n";
		#	system(" perl /var/www/html/phast/current/public/cgi-bin/phage.pl 2>&1|cat >/dev/null")==0 or print "$!\n\n";
		}else{

			open (OUT, ">qq") or die "Cannot write qq";
            		    print OUT "$first\n$rest";
	                close OUT;

		}
	}else{
		print "qq list is empty, exit run_qq.pl program \n";
		last;
	}
}
exit;

