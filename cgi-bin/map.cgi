#!/usr/bin/perl
# remap NCBI tax
use strict;

my %parent = (); # taxon id to parent hash
my %names = (); # taxon id to name hash
my %nametoid = (); # name to taxon id
my %rank = (); # taxon id to rank (kingdom, phylum etc.) hash
my $sixranks = {superkingdom => 1, phylum => 1, class => 1, order => 1, family => 1, genus => 1};

my $line;
my @tokens;
my $database = "/apps/phast/project/metagen/ncbi.db";
sub getTaxonInfo {
	my ($taxid, $format, $rankon, $idon) = @_;
	if (defined $names{$taxid} and defined $rank{$taxid}){
		if ($rank{$taxid} eq "no rank"){
			return "";
		}
		if ($format eq 'six' and ! defined $sixranks->{$rank{$taxid}}){
		  return "";
		}
		my $final = $names{$taxid}.";";
		if ($idon eq "yes"){
		  $final = $taxid.'::'.$final;
		}
		if ($rankon eq "yes"){
		  $final = $rank{$taxid}."::".$final;
		}
		return $final;
	}
	else{
		die "hash does not have taxid $taxid";
	}
}
sub getFullName {
	my ($taxid, $format, $rankon, $idon) = @_;
	my $fullname = "";
	if ($taxid == 0){
		return "ERROR: cannot map this tax\n";
	}
	while ($taxid != 131567 and $taxid != 1){ # these are general roots
		$fullname = &getTaxonInfo ($taxid, $format, $rankon, $idon).$fullname;
		if (defined $parent{$taxid}){
			$taxid = $parent{$taxid};
		}
		else{
			return "Error: non-root taxid '$taxid' does not have a parent\n";
		}
	}
	return $fullname;
}
sub getNodeByName{
	my ($raw) = @_;
	my @taxons = ();
	if ($raw=~m/::/){
		while ($raw =~m/::([^:;]+)/g){
			push @taxons, $1;
		}
	}
	else{
		@taxons = split(/\W+/, $raw);
	}
	my $idmap = {
		kingdom => "",
		phylum  => "",
		class   => "",
		order   => "",
		genus   => ""};
	foreach (@taxons){
#		print "TAXON $_\n";
		if (defined $nametoid{$_}){
			my $ids = $nametoid{$_};
#			print " taxon '$_' has ID $ids\n";
			while ($ids=~m/(\d+)/g){ # in case many ids connected by ,
#				print "  contains TAXID: $1\n";
				&getRank($idmap, $1);
			}
		}
	}
	my $lowest = "";
	if ($idmap->{genus} ne ""){
		$lowest = $idmap->{genus};
	}
	elsif ($idmap->{order} ne ""){
		$lowest = $idmap->{order};
	}
	elsif ($idmap->{class} ne ""){
		$lowest = $idmap->{class};
	}
	elsif ($idmap->{phylum} ne ""){
		$lowest = $idmap->{phylum};
	}
	elsif ($idmap->{kingdom} ne ""){
		$lowest = $idmap->{kingdom};
	}
#	print "get full name\n";
	if ($lowest eq ""){
		print "ERROR: cannot assign $raw\n";
		return 0;
	}
	else{
		return &bestmatch($idmap, $lowest);
	}
}

sub getRank {
	my ($idmap, $taxid) = @_;
#	print "in function: map = $idmap id = $taxid\n";
	if ($taxid == 131567 or $taxid == 1){
		return;
	}
	if ($rank{$taxid} eq "kingdom" or $rank{$taxid} eq "superkingdom"){ # some record does not match until the superkingdom rank
		if ($idmap->{kingdom} eq ""){
			$idmap->{kingdom} = {};
			$idmap->{kingdom}->{RANK} = 1;
		}
		$idmap->{kingdom}->{$taxid} += 1;
	}
	elsif ($rank{$taxid} eq "phylum"){
		if ($idmap->{phylum} eq ""){
			$idmap->{phylum} = {};
			$idmap->{phylum}->{RANK} = 2;
		}
		$idmap->{phylum}->{$taxid} +=1;
	}
	elsif ($rank{$taxid} eq "class"){
		if ($idmap->{class} eq ""){
			$idmap->{class} = {};
			$idmap->{class}->{RANK} = 3;
		}
		$idmap->{class}->{$taxid} ++;
	}
	elsif ($rank{$taxid} eq "order"){
		if ($idmap->{order} eq ""){
			$idmap->{order} = {};
			$idmap->{order}->{RANK} = 4;
		}
		$idmap->{order}->{$taxid} ++;
	}
	elsif ($rank{$taxid} eq "genus"){
		if ($idmap->{genus} eq ""){
			$idmap->{genus} = {};
			$idmap->{genus}->{RANK} = 5;
		}
		$idmap->{genus}->{$taxid} ++;
	}
	if (defined $parent{$taxid}){
#		print "$taxid calles parent $father->{$taxid}->{id}.\n";
		&getRank($idmap, $parent{$taxid});
	}
	else{
		die "non-root taxid '$taxid' does not have a parent\n";
	}
}

sub bestmatch {
	my ($idmap, $lowest) = @_;
	my $count = {};
	foreach  (keys %{$lowest}){
		my $in = $_; # name
		if ($in ne "RANK"){
#			print "analysing $in\n";
			foreach (keys %{$idmap}){
				my $out = $_; # rank
				if ($idmap->{$out} ne "" and $idmap->{$out}->{RANK} <= $lowest->{RANK}){
#					print "RANK $idmap->{$out}->{RANK}\n";
					foreach (keys %{$idmap->{$out}}){
						if ($_ ne "RANK"){
#							print "compare $_, $in\n";
							if (&upstream($_, $in)){
								$count->{$in} += 1;
							}
						}
					}
					
				}
			}
		} 
	}
	my $best = "";
	foreach (%{$count}){
		if ($best eq ""){
			$best = $_;
		}
		elsif ($count->{$best} < $count->{$_}){
			$best = $_;
		}
	}
	return $best;
}

sub upstream {
	my ($target, $anchor) = @_;
	if ($target == $anchor){
		return 1;
	}
	while ($anchor != 131567 and $anchor != 1 and defined $parent{$anchor}){
#		print "self = $anchor and parent = $parent{$anchor}\n";
		$anchor = $parent{$anchor};
		if ($target eq $anchor){
			return 1;
		}
	}
	return 0;
}

sub loadDB {
	open (R, $database) or die "cannot open $database";
	while ($line = <R>){
		chomp($line);
		@tokens = split (/\t/, $line);
		if ($#tokens == 3){
			$parent{$tokens[0]} = $tokens[1];
			$rank{$tokens[0]}   = $tokens[2];
			$names{$tokens[0]}  = $tokens[3];
			if (defined $nametoid{$tokens[3]}){
				$nametoid{$tokens[3]} = "$nametoid{$tokens[3]},$tokens[0]";
			}
			else{
				$nametoid{$tokens[3]} = $tokens[0];
			}
		}
	}
	close (R);
}

sub read_input
{
    my ($buffer, @pairs, $pair, $name, $value, $FORM);
    # Read in text
    $ENV{'REQUEST_METHOD'} =~ tr/a-z/A-Z/;
    if ($ENV{'REQUEST_METHOD'} eq "POST")
    {
	read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
    } else
    {
	$buffer = $ENV{'QUERY_STRING'};
    }
    # Split information into name/value pairs
    @pairs = split(/&/, $buffer);
    foreach $pair (@pairs)
    {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%(..)/pack("C", hex($1))/eg;
	$FORM->{$name} = $value;
    }
    return $FORM;
}

################# Main starts here ###############
my $buffer;

&loadDB();

print "Content-type: text/html

<head>
	<title>Taxonamy normalization tool</title>
</head>
<body>
";

my $par = &read_input();
my @lines = split(/\n+/, $par->{input});
foreach(@lines){
  if ($_=~m/\w/){
    print &getFullName(&getNodeByName($_), $par->{format}, $par->{rank}, $par->{taxid}),'<BR/>';
  }
}
print "

</body>
";

exit;


