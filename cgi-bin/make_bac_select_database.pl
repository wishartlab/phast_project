#!/usr/bin/perl -w
use  Cwd;

my $exec_dir = "/home/phast/public_html/cgi-bin";
my $tmp_dir = "/home/phast/public_html/phage_finder/DB/temp_bac_select";
chdir $tmp_dir;

my $start_time = time;
print  `date`;
system("rm -rf *") if (-e "list");

system("wget 'ftp://ftp.ncbi.nih.gov/genomes/Bacteria/all.faa.tar.gz'");
if (-e "all.faa.tar.gz"){
	print "all.faa.tar.gz is downloaded!\n";
}else{
	print "No all.faa.tar.gz is downloaded!\n";
	exit;
}

system("tar -xvzf all.faa.tar.gz > /dev/null");
system("ls >list");

open(IN, "list") or die "Cannot open list";
my $output="bacteria_all_select.db";
unlink ($output);
open(OUT, ">$output") or die "Cannot write $output";
my $last ='';
my $curr_dir =getcwd;
my $count=0;
while(<IN>) {
	chomp ($_);
	if ($_ eq 'list' or  $_ eq 'make_bac_select.pl' or $_ eq 'all.faa.tar.gz' or  $_ =~/bacteria_all_select\.db/  ){
		next;
	}
	
	chdir $_;
	my $v_name = $_;
	
	my $file_list =`ls`;
	my @arr=split("\n", $file_list);
	my $file=$arr[0];
		chomp($file); 
		$count++;
		#print "     $v_name/$file\n";
		my $data = `cat $file`;
		print OUT $data;
	
	chdir $curr_dir;

}
close IN;
close OUT;
print "In all.faa.tar.gz, we found $count phage organisms!\n";

open(IN, "bacteria_all_select.db") or die "Cannot open bacteria_all_select.db";
open(OUT, ">bacteria_all_select.db.tmp") or die "Cannot write bacteria_all_select.db.tmp"; 
my %hash =();
my $flag = 0;
while(<IN>) {
	if ($_=~/>gi\|(\d+)/){
		#>gi|158333234|ref|YP_001514406.1| NUDIX hydrolase [Aca
		$hash{$1} += 1;
		if ($hash{$1}>1){
			$flag = 1;
		}else{
			$flag =0;
			print OUT $_;
		}
	}else{
		if ($flag==0){
			print OUT $_;
		}
	}
}
close IN;
close OUT;
system("mv -f bacteria_all_select.db.tmp  bacteria_all_select.db");

if (-s "bacteria_all_select.db"){
	print "Copy bacteria_all_select* to upper level\n";
	system("cp bacteria_all_select* ../.");
	# propagate to cluster
	print "Propagate bacteria_all_select* to cluster\n";
	system("$exec_dir/propogate_database.sh  $tmp_dir/bacteria_all_select.db '-o T' ");
	#clean up
	system("rm -rf * ") if (-s 'list');
}else{
	print "bacteria_all_select.db  has no content. Please check!\n";
}
my $end_time = time;
my $time = $end_time - $start_time;
my $min = int($time /60);
my $sec = $time % 60;
print "running time = $min min $sec sec\n";
print "Program exit!\n\n\n";
exit;


