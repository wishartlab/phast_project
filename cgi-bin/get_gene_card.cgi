#!/usr/bin/perl -w

# this program will display the gene information for proteins of cgview image links.
# it will create a gene card to show all information we got from  glimmer and phage finder

use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $q = new CGI;


my $pp_id = $q->param("id");
my $case_dir = $q->param("dir");


open(IN, "../$case_dir/png_input") or die "Cannot open ../$case_dir/png_input";
my $info ='';
my %hash=();
while (<IN>) {
	if ($_=~/^>/){
		$header= $_;
	}elsif ($_=~/$pp_id/i && $_=~/^section/){
		$info = $_;
		chmod ($info);
	}elsif ($_=~/^region/){
		my @array = split(" ", $_);
		$hash{"$array[1]  $array[$#array]"} = "$array[2]   $array[3]";
	}
}
close IN;
$info =~s/section\s+//;
$header=~s/>//;
my $bac_name;
my $NC;
my $seq_length;
if ($header =~/(.*?)\[asmbl_id:\s+(\S+)\]\S\s+(\d+)/ or $header =~/(.*?)\[(\S+)\]\S\s+(\d+)/){
	$bac_name = $1;
	$NC = $2;
	$seq_length= $3;
}


my @arr = split(/\s\s\s*/, $info);
my $from = $arr[0];
my $to = $arr[1];
my $strand = substr($arr[2], 0, 1);
if ($strand eq '-'){
	$strand = "Backward";
}else{ # '+'
	$strand = "Forward";
}
my $type= $arr[3];
my $phage_specie ='';
my $pro_name = '';
if ($type !~/Hypothetical_protein/i && $type !~/Attachment_site/i && $type !~/tRNA/i && $type !~/tmRNA/i && $type !~/Non_phage-like_protein/i){
	if ($arr[4]=~/(.*);([^;]+)$/){
		$phage_specie = $2;
		$pro_name = $1;
	}
}else{
	$phage_specie = $arr[3];
	$pro_name = $arr[4];
}

my $locus_tag=$pp_id;
if($locus_tag=~/^\d+$/){
	$locus_tag='';
}else{
	$pro_name =~s/$locus_tag//;
	$pro_name =~s/;(\s*\w|\s*|\w);/;/g;
	$pro_name =~s/^(\s*\w|\s*|\w);//g;
}	
if ($locus_tag eq ''){
	$locus_tag = 'N/A';
}
my $evalue = $arr[5];
my $pro_seq = $arr[6];
my $str;
my $i = 0;
while($i < length($pro_seq)){
	$str .= substr($pro_seq, $i, 60)."<br>";
	$i += 60;
}
$pro_seq = $str;	
my ($region, $region_identity) = get_region($from, \%hash );
my @array1 =();
push @array1, "LOCUS_TAG";
push @array1, "ORF START";
push @array1, "ORF STOP";
push @array1, "STRAND";
push @array1, "REGION";
push @array1, "REGION intact or incomplete";
push @array1, "PROTEIN SEQUENCE";
push @array1, "HOMOLOG/ORTHOLOG SPECIES";
push @array1, "HOMOLOG/ORTHOLOG PROTEIN";
push @array1, "HOMOLOG/ORTHOLOG EVALUE";
my @array2;
push @array2, $locus_tag;
push @array2, $from;
push @array2, $to;
push @array2, $strand;
push @array2, $region;
push @array2, $region_identity;
push @array2, $pro_seq;
push @array2, $phage_specie;
push @array2, $pro_name;
push @array2, $evalue;

my $table = "<table width=\"750\" border=\"1\" cellspacing=\"1\" cellpadding=\"4\" style=\"font-size:12;\" > ";
for(my $i =0; $i<=$#array1; $i++){
	$table .= "<tr><td> $array1[$i] </td> <td> $array2[$i] </td></tr>";
}
$table .= "</table>";
my $content ="<table align='left' style=\"font-size:15; font-weight:bold; border:1px solid darkblue; width:770px; padding:15px; margin-left:auto;margin-right:auto\"> ";
$content .= "<tr><td> Definition: $bac_name</td></tr><tr><td> Accession: $NC</td></tr><tr><td> Length: $seq_length bp</td></tr><tr><td> &nbsp; </td></tr><tr><td>  &nbsp; </td></tr>";
$content .= "<tr><td>$table</td></tr>";
$content .= "</table>";


HTML_Page($content);

exit;

sub HTML_Page
{
	my $content = shift;
	print "content-type: text/html \n\n";
  	print <<HTML

	<HTML>
	<HEAD>
	<TITLE>Phage Lookup results</TITLE>
	</HEAD>
	<BODY BGCOLOR="white">
	$content
  	</BODY>
  	</HTML>
HTML
}
#get region number for the current ORF
sub get_region{
	my ($from , $hash) =@_;
	my $region='';
	my $region_identity='';
	foreach my $key (keys %$hash){
		my @arr = split (" ", $$hash{$key});
		if ($from >= $arr[0] && $from <= $arr[1]){
			my @arr_tmp = split(" ", $key);
			$region = $arr_tmp[0];
			$region_identity = $arr_tmp[1];
			last;
		}
	}
	return ($region, $region_identity); 	
}
