#!/usr/bin/perl -w

# this program will extract Paul's files at column 'end5' when there
# is 'PHAGE'  or [ANNO] or [HMM-GLOCAL] on that line. After that we will use that number to search
# the prophage's .gbk file of NCBI (<flag>='gbk' or 'faa') and get the GENE position back and that 
# linked URL if .gbk file exit. If not, use .faa file. Then go to that URL to extract protein sequenc
# input file involved: ../.faa, ../tRNAscan.out, ../tmRNA_aragorn.out
#Usage : perl extract_protein.pl  <case number>  <input_file> <output_file>
use Bio::SeqIO;
use Bio::Seq;
use Cwd;

my $num = $ARGV[0];
my $input_file = $ARGV[1];
my $output_file = $ARGV[2];
my $seq =`cat ../$num.fna`;
$seq =~s/>.*?\n//s;
$seq =~s/[\n\s]//gs;


my %hash_tRNA=();
get_hash_tRNA(\%hash_tRNA);
my $faas=get_FAA_info($num);

if (-s $input_file){
	my ($regions, $head_line)=parse_scan_output($input_file, $num, \%hash_tRNA, $faas);
	print_out($regions, $head_line);
}else{
	print STDERR "There is no $input_file \nprogram exit\n";
	exit(-1);
}
exit;

sub get_FAA_info{
	my $num=shift;
	my @faas=();
	my @data=split '>', `cat ../$num.faa`;
	foreach my $e (@data){
		next if ($e eq '');
                #gi|00001|  [DEFINITION  Escherichia coli str. K-12 substr. MG1655 chromosome   PP_00001        gene     343..2799]
                #gi|00004|  [DEFINITION  Escherichia coli str. K-12 substr. MG1655 chromosome   PP_00004        gene      complement(5310..5537)]
                #gi|16127998|ref|NP_414545.1| threonine synthase, 3734..5020 [Escherichia coli str. K-12 substr. MG1655]
                #gi|49175991|ref|YP_025292.1| toxic membrane protein, small, complement(16751..16903) [Escherichia coli str. K-12 substr. MG1655]
         if ($e=~/^gi\|(\d+)\|.*?(complement\(<*>*\d+\.\.<*>*\d+\)).*?\n/ or $e=~/^gi\|(\d+)\|.*?(<*>*\d+\.\.<*>*\d+).*?\n/){
                	my $f= new FAA();                
					$f->{gi}= $1;
                    $f->{position}=$2;
					$f->{protein_seq}=$';
					$f->{protein_seq}=~s/[\s\n]//gs;
                    if ($f->{position}=~/^.*?complement\(<*>*(\d+)\.\.<*>*(\d+)\)/){
						$f->{end5}=$2; 	$f->{end3}=$1;
					}
					elsif($f->{position}=~/^.*?<*>*(\d+)\.\.<*>*(\d+)/){
                          $f->{end5}=$1; $f->{end3}=$2;
                	}
					push @faas, $f;
         }
             
     }
	return \@faas;


}

sub get_position{
	my ($end5, $gi, $faas)=@_;
	foreach my $f (@$faas){
		if ($f->{end5} == $end5 && $f->{gi}==$gi){
			return $f->{position}, $f->{protein_seq};
		}
	}
	return '', '';
}
# get genes from NCBI and output data to extract_result.txt
sub parse_scan_output{
	my ($filename, $num, $hash_tRNA, $faas)=@_;
	my $head_line='';
	my $head_flag = 1;
	my $r='';
	my %regions=();
	my $cur_region='';
	open(IN2, $filename) or die "Cannot open $filename";
	while(<IN2>) {
		next if ($_=~/^\s*$/);
		$r='';
		if ( $head_flag ==1){
			# gi|53723370|ref|NC_006348.1| Burkholderia mallei ATCC 23344 chromosome 1, complete sequence [asmbl_id: NC_006348], 3510148, gc%: 68.15%
			$head_line = $_;
			if ($head_line =~/gc\%\:\s*0.00/){
				my $sub_seq = $seq;
        	                 $sub_seq =~s/[ATat]//gs;
	                         my $p = length($sub_seq)/length($seq);
                	         $p = int($p *1000) /10;
				 $head_line =~s/gc\%\:\s*0.00/gc\%\: $p/;
			}
			#print $head_line;
			$head_flag =0;
			next;
		}
		#region 1 is from
		if ($_=~/region (\d+) is from/){
			$cur_region=$1;
			$regions{$cur_region}=[];
			next;
		}
		#788904   53725137           gi|41057343|ref|NP_958229.1|, TAG = PHAGE_shigel_Sf6, E-VALUE = 1e-10
		if ($_=~/^(\d+)\s+(\d+)\s+gi\|(\d+)\|.*TAG\s*=\s*(\S+), E-VALUE\s*=\s*(\S+)/){
			# 788904   53725137           gi|41057343|ref|NP_958229.1|, TAG = PHAGE_shigel_Sf6, E-VALUE = 1e-10
			$r= new Virus_hit_record;
			$r->{end5}=$1;
			$r->{local_gi_number}=$2;
			$r->{phage_gi_num}=$3;
			$r->{phage_name}=$4;
			$r->{evalue}=$5;
			$r->{class}='hit';
			($r->{position}, $r->{protein_seq})=get_position($r->{end5},  $r->{local_gi_number}, $faas);
			if (<IN2>=~/^\s\s\s\s\s+\[ANNO\](.*)/){
				$r->{phage_name} .= ":$1";
			}else{
				$r->{phage_name} .= ":";
			}
			push @{$regions{$cur_region}}, $r;
		}elsif ($_=~/^(\d+)\s+(\d+)\s+(\S+),\s+TAG\s*=\s*(\S+), E-VALUE\s*=\s*(\S+)/){
			#4407841  26990594           ORF0001, TAG = PHAGE_coliph_R73, E-VALUE = 3e-16
			$r= new Virus_hit_record;
			$r->{end5}=$1; 
			$r->{local_gi_number}=$2;
			$r->{phage_gi_num}='N/A';
			$r->{phage_name}="$4,$3";
			$r->{evalue}=$5;
			$r->{class}='hit';
			($r->{position}, $r->{protein_seq})=get_position($r->{end5},  $r->{local_gi_number}, $faas);
			if (<IN2>=~/^\s\s\s\s\s+\[ANNO\](.*)/){
                                $r->{phage_name} .= ":$1";
                        }else{
                                $r->{phage_name} .= ":";
                        }
			push @{$regions{$cur_region}}, $r;
		}
		#2586037  attL_3             GATTTAGGTTCCAGCGGCGCAAGCTGTAAGAGTTCGAGTCTCTTCGTCCGCACCA
		elsif ($_ =~/^(\d+)\s+(attL|attR).*?\s+(.*)/ ){
			$r= new Virus_hit_record;
			$r->{end5}=$1;
			$r->{local_gi_number}='N/A';
			$r->{phage_name}=$2;
			$r->{phage_gi_num}= 'N/A';
			$r->{evalue}= 'N/A';
			$r->{end3}= length($3) + $1 -1;
			$r->{position}=$r->{end5}."..".$r->{end3};
			$r->{protein_seq}=$3;
			$r->{class}='hit';
			push @{$regions{$cur_region}}, $r;

		}
		elsif ($_ =~/^(\d+)\s+(tRNA|tmRNA)\s+\[ANNO\](.*)/ ){
			$r= new Virus_hit_record;
			$r->{end5}=$1;
			$r->{class}='hit';
			$r->{phage_name}=$2;
			$r->{phage_gi_num}= 'N/A';
			$r->{evalue}= 'N/A';
			$r->{local_gi_number}='N/A';
			$r->{end3}= $hash_tRNA->{$1};
			my $s='';	
			if ($r->{end5}>$r->{end3}){
				$r->{position}="complement(".$r->{end3}."..".$r->{end5}.")";
				$s=substr($seq, $r->{end3}-1, $r->{end5}- $r->{end3} +1);
				$s=~tr/ATCG/TAGC/;
				$s=reverse($s);
				
			}else{
				$r->{position}=$r->{end5}."..".$r->{end3};
				$s=substr($seq, $r->{end5}-1, $r->{end3}-$r->{end5}+1);
			}
			$r->{protein_seq}=$s;
			push @{$regions{$cur_region}}, $r;
			
		}
		#87086    187921984          [ANNO] phage integrase; bphyt_7359
		elsif ($_ =~/^(\d+)\s+(\d+)\s+\[ANNO\](.*)/){
			$r= new Virus_hit_record;
			$r->{end5}=$1;
			$r->{local_gi_number}=$2;
			$r->{phage_name}=$3;
			$r->{phage_gi_num}= $2;
			if ($r->{phage_name}=~/E-VALUE = (\S+);/){
				$r->{evalue}=$1;
				$r->{phage_name}=~s/E-VALUE = (\S+);//s;
			}else{
	                        $r->{evalue}= 'N/A';
			}
			$r->{class}='';
			($r->{position}, $r->{protein_seq})=get_position($r->{end5},  $r->{local_gi_number}, $faas);
			push @{$regions{$cur_region}}, $r   if ($r->{position} ne '' && $r->{protein_seq} ne '');
		}
		
	}
	close IN2;
	return \%regions, $head_line;
}

sub print_out{
	my $regions=shift;
	my $head_line=shift;
	# output result
	my $line=''; 
	open(OUT, ">$output_file") or die "Cannot write $output_file";
	print OUT $head_line ."\n";
	$line = sprintf("%-30s     %-80s     %-15s     %s\n", "CDS_POSITION", "BLAST_HIT", "EVALUE", "prophage_PRO_SEQ");
	print OUT $line;
	print OUT "--------------------------------------------------------------------------------------------------".
		"-------------------------------------------------------------------------------------\n";
	my @key_array=sort {$a<=>$b} keys %{$regions};
	foreach my $k (@key_array){
		print OUT "\n#### region $k ####\n";
		foreach my $r (@{$regions->{$k}}) {
			$r->{phage_name}=~s/^\s*//s;
			$r->{phage_name}=~s/\s*$//s;
			if ( $r->{phage_name}=~/^(attL|attR|tRNA|tmRNA)/){
				$line = sprintf("%-30s     %-80s     %-15s     %s", $r->{position}, $r->{phage_name}, $r->{evalue}, $r->{protein_seq});
			}else{
				if ($r->{class} eq 'hit'){
					$line = sprintf("%-30s     %-80s     %-15s     %s", $r->{position}, $r->{phage_name}."(gi".$r->{phage_gi_num}.")", $r->{evalue}, $r->{protein_seq});
				}else{
					$line = sprintf("%-30s     %-80s     %-15s     %s", $r->{position}, $r->{phage_name}, $r->{evalue}, $r->{protein_seq});
				}
			}
			print OUT $line."\n";
		}
		
	}
	close OUT;
	 
}

# get tRNA info form gbk and tRNAscan.result
sub get_hash_tRNA{
	my $hash_tRNA = shift;	
	if (-e "../tRNAscan.out"){
		open(IN99, "../tRNAscan.out"); 
		while(<IN99>){
			if ($_=~/gi\|.*?\|ref\|.*?\|\s+\d+\s+(\d+)\s+(\d+)/){
				$hash_tRNA->{$1} = $2;
			}
		}
		close IN99;
	}
	if (-e "../tmRNA_aragorn.out"){
		open(IN99, "../tmRNA_aragorn.out");
		while(<IN99>){
			if ($_=~/Location c\[(\d+),(\d+)\]/){
				$hash_tRNA->{$2}=$1;
			}elsif ($_=~/Location \[(\d+),(\d+)\]/){
				$hash_tRNA->{$1} = $2;
			}
		}
		close IN99;
	}
	if (-e "../$num.gbk"){
		open(IN99, "../$num.gbk");
		while(<IN99>){
			if ($_=~/^\s+tRNA\s+(\d+)\.\.(\d+)/){
				$hash_tRNA->{$1} = $2;
			}
			if ($_=~/^\s+tRNA\s+complement\((\d+).*?(\d+)\)/){
				$hash_tRNA->{$2} = $1;
			}
		}	
		close IN99;
	}
}

package Virus_hit_record;

sub new {
	my $class =shift;
	my $self ={ end5=>'',
		end3=>'',	
		position=>'',	
		local_gi_number=>'',
		phage_gi_num=>'',
		phage_name=>'',
		evalue=>'',
		protein_seq=>'',
		class=>''
		 };
	bless $self, $class;
	return $self;
}

package FAA;
sub new {
	my $class = shift;
	$self={ position=>'',
		end5=>'',
		end3=>'',
		gi=>'',
		protein_seq=>''};
	bless $self , $class;
	return $self;
}



1;
