#!/bin/bash

cluster_dir=$1
host="$HTTP_PHAST_CLUSTER_USERNAME@$HTTP_PHAST_CLUSTER_HOSTNAME"

ssh -i /apps/phast/.ssh/scp-key  $host  "rm -rf $cluster_dir"
ssh -i /apps/phast/.ssh/scp-key  $host  "mkdir -p $cluster_dir"
