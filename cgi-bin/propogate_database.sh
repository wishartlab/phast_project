#!/bin/bash

BASE_ROOT=/var/www/html/phast/current/public
DB=$BASE_ROOT/sub_programs/phage_finder/DB/.
key=/apps/phast/.ssh/scp-key
host="$HTTP_PHAST_CLUSTER_USERNAME@$HTTP_PHAST_CLUSTER_HOSTNAME"

# in void of long time consumming, we make database this way on cluster.
echo "copy virus.db from $host"
scp -i $key    $host:phage/DB/virus.db $DB
echo "copy prophage_virus.db from $host"
scp -i $key    $host:phage/DB/prophage_virus.db $DB
echo "copy prophage_virus_header_lines.db from $host"
scp -i $key    $host:phage/DB/prophage_virus_header_lines.db $DB
echo "copy update.log from $host"
scp -i $key    $host:phage/DB/update.log  $DB
echo "copy vgenome.tbl from $host"
scp -i $key    $host:phage/DB/vgenome.tbl $BASE_ROOT/database/.

# These lines commented out because we are no longer updating the bacterial DB.
# echo "copy bacteria_all_select.db from $host"
# scp -i $key    $host:phage/DB/bacteria_all_select.db $DB
# echo "copy bacteria_all_select_header_lines.db from $host"
# scp -i $key    $host:phage/DB/bacteria_all_select_header_lines.db* $DB
