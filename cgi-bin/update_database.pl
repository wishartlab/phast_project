#!/usr/bin/perl -w

# Update databases on front-end and update Databases web page.

my $base_root ="/var/www/html/phast/current/public";
my $sub_program_dir = "$base_root/sub_programs";
my $exec_dir = "$base_root/cgi-bin";

system("$exec_dir/propogate_database.sh 2>&1");

my $date = `cat $sub_program_dir/phage_finder/DB/update.log`;
my @array=split(" ", $date);
$date = "$array[1] $array[2] $array[$#array]";
print  "Update : $date\n";
print  "current time: ". `date`."\n";

my $size_vir_db = `du -sk $sub_program_dir/phage_finder/DB/virus.db`;
$size_vir_db=~s/^(\d+).*/$1/;
$size_vir_db= int($size_vir_db/100)*100/1000;
my $size_pro_db = `du -sk $sub_program_dir/phage_finder/DB/prophage_virus.db`;
$size_pro_db=~s/^(\d+).*/$1/;
$size_pro_db = int($size_pro_db/100)*100/1000;
print "size_vir_db=$size_vir_db\nsize_pro_db=$size_pro_db\n";
my $html_file = "$base_root/Download.html";
open(IN, $html_file) or die "Cannot open $html_file";
open(OUT, ">$html_file.tmp1") or die "Cannot write $html_file.tmp1";
while(my $l=<IN>) {
	if ($l=~/Last update:.*?</ ) {
		if ($l =~/Bacteria DB/){
			# We are not updating the bacterial database anymore.
		} else {
			$l =~s/Last update:.*?</Last update: $date </gs;
		}
		if ($l =~/Virus DB/){
			$l=~s/\(\S+\s*Mb/\($size_vir_db Mb/;
		} elsif($l =~/Prophage and virus DB/) {
			$l=~s/\(\S+\s*Mb/\($size_pro_db Mb/;
		}
	} elsif ($l=~/Last update:.*?$/ ) {
		if ($l =~/Bacteria DB/){
			# We are not updating the bacterial database anymore.
		} else {
			$l =~s/Last update:.*?$/Last update: $date/gs;
		}
		if ($l =~/Virus DB/) {
			$l=~s/\(\S+\s*Mb/\($size_vir_db Mb/;
		} elsif($l =~/Prophage and virus DB/) {
			$l=~s/\(\S+\s*Mb/\($size_pro_db Mb/;
		}
	}
	print OUT  $l;
}
close IN;
close OUT;
system("mv -f $html_file.tmp1 $html_file");
#print  "perl $exec_dir/get_phage_list.pl 2>&1 \n";
#system("perl $exec_dir/get_phage_list.pl 2>&1");
system("perl $exec_dir/z_find_pack.pl "); # create phast.tz file
print "update End: ". `date`. "\n\n\n";
exit;

