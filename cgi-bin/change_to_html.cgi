#!/usr/bin/perl -w

# this program will change the summary.txt into 
# html since  txt format on different browsers shows weird.
use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $sub_program_dir= "/var/www/html/phast/current/public/sub_programs";
my $base_root="/var/www/html/phast/current/public";
my $q = new CGI;
my $dir = $q->param('num');
my $file= $q->param("file");
my $tmp1 = $q->param("tmp");
my $summary_file = (defined $file && $file ne '')?  $file : "summary.txt";
my $tmp = (defined $tmp1 && $tmp1 ne '')? "$base_root/$tmp1" : "$base_root/tmp";
my $db_data = `grep '>' $sub_program_dir/phage_finder/DB/prophage_virus.db`;
my $search_rec_exec="/cgi-bin/search_rec.cgi?num=$dir&file=$summary_file";
$search_rec_exec.="&tmp=$tmp1" if (defined $tmp1 && $tmp1 ne '');
open (IN, "$tmp/$dir/$summary_file" ) or die "Cannot open $tmp/$dir/$summary_file";
my $head ='';
my $intact = 0;
my $defect=0;
my $question=0;
my $before=1;
my $species= '';
my $link = "http:\/\/www.ncbi.nlm.nih.gov\/genomes\/GenomesGroup.cgi\?taxid=10239\&opt=Virus\&sort=genome";

my $table_content="<table border='1' cellpadding='1' cellspacing='1' style='font-size:14;'>";
$table_content .= "<tr><th style=\"background-color:#33FFCC;\">REGION</th><th style=\"background-color:#33FFCC;\">REGION_LENGTH</th><th style=\"background-color:#33FFCC;\">COMPLETENESS</th><th style=\"background-color:#33FFCC;\">SCORE</th><th style=\"background-color:#33FFCC;\">#CDS</th><th style=\"background-color:#33FFCC;\">REGION_POSITION</th><th  style=\"background-color:#33FFCC;\">POSSIBLE PHAGE</th><th style=\"background-color:#33FFCC;\">GC_PERCENTAGE</th><th  style=\"background-color:#33FFCC;\">DETAIL</th></tr>";
my $reg_num=0;
my $iden_line='';
while (<IN>){

   if ($before ==1){
  chomp($_);
  if ($_ =~ /^Totally\s+(\d+)/){
    #$intact = $1;
    $before = 0;
    next;
  }
  if ($_ =~/Criteria for identifying/ or 
      $_ =~/Criteria for scoring/  
      ){   
    $head .= "<b>$_</b><br>";
    next;
  }
  if($_=~/this table/){
    $_=~s/this table/<a href='$link'>this table<\/a>/;
    }
    if ($_=~/^\s+/){
      $_=~s/^\s+/&nbsp;&nbsp;&nbsp;&nbsp;/;
      
    }
    $head .= "$_<br>";
    next;
   }
  
   if ($before ==0 && $_=~/REGION\s+REGION_LENGTH/){
    $before =-1;
    next;
   }
   if ($before ==0 && $_=~/gc%/i ){
  $species = $_;
    chomp($species);
    if ($species !~/length/){
      my $length=`cat $tmp/$dir/detail.txt`;
      if ($length=~/\.\s*(\d+)\s*,\s*GC/si){
        $species .= ", length = ". $1." bps";
      }
    }
    $species =~s/\[NC_000000\]//;
    next;
   }
   if ($before ==-1 && $_ =~/^\s+\d+/ ) {
    $reg_num++;
    my $color = '';
    if ($_=~/incomplete/){
      $defect++;
      $color = "#E0E0E0";
    }elsif ($_=~/intact/){
      $intact++;
      $color = "#FF3366";
    }elsif ($_=~/questionable/){
      $question++;
      $color = "#33FF88";
    }
    my @array = split (/\s\s\s+/, $_);
    my $reg = $array[1];
    my $leng= $array[2];
    my $t_d = $array[3];
    $t_d=~s/\(([-\d\.]+)\)//;
    my $sc = $1;
    my $orf_p = "<a href=\"/cgi-bin/get_region_DNA.cgi?num=$dir&number=$reg\"><font color='black'>".$array[5]."</font></a>";
    my $cds  = $array[7];
    my $gc = $array[$#array];
    my $pha= $array[14];
    if ($pha !~/\(\d+\)/){
      $pha = get_phage($pha, $db_data);
    }else{
      ($pha)=$pha=~/^(.*?)\(/;
      $pha.=", ......" if ($array[14]=~/,/);
    }
    my @arr=();
    push @arr, "<a href=\"/cgi-bin/change_detail_html.cgi?num=$dir#$reg\"><font color='black'>".$reg."</font></a>";
    push @arr, $leng;
    push @arr, $t_d;
    push @arr, $sc;
    push @arr, $cds;
    push @arr, $orf_p;
    push @arr, $pha;
    push @arr, $gc;
    $table_content .= "<tr>";
    foreach my $i (0..$#arr){
      $table_content .= "<td style=\"background-color:$color;\">$arr[$i]&nbsp;</td>";
    }
    $table_content .= "<td style=\"background-color:$color;\"><a href='$search_rec_exec&rec=$reg'>Detail</a></td>";
    $table_content .= "</tr>";
    next;
   }
}
if ($table_content !~ /<\/table>/){
  $table_content .= "</table>";
}
close IN;

if ($species eq '') {
  $species = "Psudo Genome";
}

my $note = "<font size=3><b>Legend:</b></font><br>".
  "<font size=2><b>REGION</b>: the number assigned to the region<br>".
  "<b>REGION_LENGTH</b>: the length of the sequence of that region (in bp)<br>".
  "<b>COMPLETENESS</b>: a prediction of whether the region contains a intact or incomplete prophage based on the above criteria<br>".
  "<b>SCORE</b>: the score of the region based on the above criteria<br>".
  "<b>#CDS</b>: the number of coding sequnce<br>".
  "<b>REGION_POSITION</b>: the start and end positions of the region on the bacterial chromosome<br>".
  "<b>PHAGE</b>: the phage with the highest number of proteins most similar to those in the region <br>".
  "<b>GC_PERCENTAGE</b>: the percentage of gc nucleotides of the region<br>".
  "<b>DETAIL</b>: detail info of the region<br></font>";


&HTML_PAGE($head,$species,  $reg_num, $intact, $defect, $question,  $table_content, $dir);
exit;

sub HTML_PAGE{
  my ($head,$species,  $reg_num, $intact, $defect, $question,  $table_content, $dir)=@_;
  $species=~s/gc%/GC%/;
print "content-type: text/html \n\n";
print <<END
<HTML>
<HEAD>
<TITLE>intact or incomplete PROPHAGE</TITLE>
</HEAD>
<BODY BGCOLOR="white">
<b>$species</b><br><br>
Total : $reg_num prophage regions have been identified, of which $intact regions are intact, $defect regions are incomplete, $question regions are questionable. <br>
$table_content  
$note 
<BR><BR>
<a href ="/tmp/$dir/$summary_file"> txt file for download </a>
  <br><br><br>$head<br>
  </BODY>
  </HTML>
END
}

sub get_phage{
  my $name = shift;
  my $db_data = shift;
  my $phage_name ='';
  if ($name =~/PHAGE_/){
    #PHAGE_Stenot_phiSMA9
    #PROPHAGE_Xylell_Temecula1
      #PHAGE_Stx2_c_1717
    if ($db_data=~/>$name.*?\[(.*?)\]\n/s){
      $phage_name.=$1;
    }
    if ($name =~/PROPHAGE_/){
      $phage_name = "Prophage ".$phage_name;
    }
    if ($phage_name eq ''){
      $name =~s/-/_/g;
      if ($db_data=~/>$name.*?\[(.*?)\]\n/s){
         $phage_name.=$1;
      }
      $phage_name = $name if ($phage_name eq '');
    }
  }else{
    $phage_name = $name;
  }
  return $phage_name ;
}


