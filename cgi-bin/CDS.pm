#!/usr/bin/perl
use strict;

# CDS object
package CDS;

sub new
{
    my $class = shift;
    my $self = {
    	_gi => shift,
        _from => shift,
        _to  => shift,
        _type => shift,
		_end =>"",
		_pid =>"",
		_refgi => "",
		_ref => "",
		_tag => "",
		_e => "",
        _next => "",
        _visit => 0,
        _cluster => "",
		_other => "",
		_cdsindex => 0,
		_annotation => "", # annotation taken from ptt file
		_density => 0,  # hit density
		_phage => "null", # whether the CDS has been marked with in a phage region
		_fprint=> 0 # a hash that tells whether the node has matched a finger print in annotation lib
    };
    bless $self, $class;
    return $self;
}

sub from {
   my( $self ) = @_;
   return $self->{_from};
}

sub gi {
   my( $self ) = @_;
   return $self->{_gi};
}

sub to {
   my( $self ) = @_;
   return $self->{_to};
}

sub get_info {
	my( $self ) = @_;
	return sprintf "%-10d %-10d         [ANNO] %s;%s\n", $self->{_end}, $self->{_gi}, '-', $self->{_pid};
#	print sprintf "GI: %8d   FROM:  %10d  TO:  %10d (%s)\n", $self->{_gi}, $self->{_from}, $self->{_to}, $self->{_type};
}

sub get_full_info {
	# must get information from the two database use refgi number
	my $name;
	my( $self, $bacteria, $virus ) = @_;
	if (!exists $$virus{$self->{_refgi}}){
		$name = '-';
	}
	else{
		my $array = $$virus{$self->{_refgi}};
		$name = $$array[1];
	}

	#'-';#&linear_search_by_gi($self->{_refgi}, $virus);
	my $modified_tag = $self->{_tag};
	if ($modified_tag=~m/^(.+)\-gi$/){
		$modified_tag = $1;
	}
	my $str1 = sprintf ("%-10d %-10d         gi|%d|ref|%s|, TAG = %s, E-VALUE = %s\n", $self->{_end}, $self->{_gi}, $self->{_refgi}, $self->{_ref}, $modified_tag, $self->{_e});
	my $str2 = sprintf "%-10s %-10s         [ANNO] %s;%s\n", "", "", '-',#$name,
	 $self->{_pid};
	return $str1.$str2;
}
sub next_node {
	my( $self ) = @_;
	return $self->{_next};
}

sub visited {
	my( $self ) = @_;
	return $self->{_visit};
}
sub set_gi {
	my ( $self, $gi ) = @_;
    $self->{_gi} = $gi; 
    return $self->{_gi};	
}
sub set_visit {
	my ( $self, $visit ) = @_;
    $self->{_visit} = $visit; 
    return $self->{_visit};	
}
sub set_type {
	my ( $self, $type ) = @_;
    $self->{_type} = $type; 
    return $self->{_type};	
}

sub type {
	my( $self ) = @_;
	return $self->{_type};
}

sub cluster {
	my( $self ) = @_;
	return $self->{_cluster};
}

sub set_cluster {
	my ( $self, $cluster ) = @_;
    $self->{_cluster} = $cluster; 
    return $self->{_cluster};	
}
# make a hash that list the number of genes that each phage family has
sub make_popularity_database {
	my ($self, $blast, $vir) = @_;
	open (R, $blast) or die "cannot open file $blast";
	my %popularity = (); # main hash that records accurence of each phage
	my $previous = -1;
	my %local = (); # local hash that removes repeat local entries
	my $line;
	while ($line = <R>){
		if ($line=~/^gi\|(\d+)\|ref\|[^\|]+\|([^\|]+)\|(\d+)/){
			my $phage_info;
			if (exists $$vir{$3}){
				$phage_info =  $$vir{$3};
			}
			else{
				die "DIE0: virus.db does not have record for gi: $3";
			}
			if ($1 == $previous){
				if (exists $local{$$phage_info[2]}){
					$local{$$phage_info[2]} = $local{$$phage_info[2]} + 1;
				}
				else{
					$local{$$phage_info[2]} = 1;
				}
			}
			else{
				$previous = $1;
				while (my ($k, $v) = each %local){
					if (exists $popularity{$k}){
						$popularity{$k} = $popularity{$k} + 1;
					}
					else{
						$popularity{$k} = 1;
					}
				}
				%local = ();
				$local{$$phage_info[2]} = 1;
			}		
			
		}
	}
	while (my ($k, $v) = each %local){
		if (exists $popularity{$k}){
			$popularity{$k} = $popularity{$k} + 1;
		}
		else{
			$popularity{$k} = 1;
		}
	}
	close (R);
	return \%popularity;
}
sub make_popularity_list {
	my ($self, $blast, $vir) = @_;
	open (R, $blast) or die "cannot open file $blast";
	my %popularity = (); # main hash that records accurence of each phage
	my $previous = -1;
	my $local = (); # local hash that removes repeat local entries
	my $line;
	while ($line = <R>){
		if ($line=~/^gi\|(\d+)\|ref\|[^\|]+\|([^\|]+)\|(\d+)/ or $line=~/^gi\|(\d+)\|([^\|]+)\|(\d+)/){
			my $phage_info;
			if (exists $$vir{$3}){
				$phage_info =  $$vir{$3};
			}
			else{
				die "DIE0: virus.db does not have record for gi: $3";
			}
			if ($1 == $previous){
				if (exists $$local{$$phage_info[2]}){
					$$local{$$phage_info[2]} = $$local{$$phage_info[2]}."=$$phage_info[0]=";
				}
				else{
					$$local{$$phage_info[2]} = "=$$phage_info[0]=";
				}
			}
			else{
				if ($previous < 0){
					$previous = $1;
					my %newhash = ();
					$local = \%newhash;
					next;
				}
				$popularity{$previous} = $local;
#				print "add $previous\n";
				$previous = $1;
				my %newhash = ();
				$local = \%newhash;
				$$local{$$phage_info[2]} = "=$$phage_info[0]=";
			}			
			
		}
	}
	if (!exists $popularity{$previous}){
		$popularity{$previous} = $local;
#		print "ADD $previous\n";
	}
	close (R);
#	while (my ($k, $v) = each %popularity){
#		print "key $k    value $v\n";
#		while (my ($k2, $v2) = each %$v){
#			print "  value: key $k2   value  $v2\n";
#		}
#	}
#	exit;
	return \%popularity;
}
sub make_data_set { # read .faa and blast output and input
	my ( $self, $faa, $blast, $ptt, $fprint) = @_;
#	my $mem;
	my @dataset = ();
	open (R, $faa) or die "cannot open file $faa";
	my $line;
	while ($line = <R>){
#		$line = '>gi|16272673|ref|NP_438891.1| hypothetical protein, complement(787177..787329) [Haemophilus influenzae Rd KW20]';
#		print $line, "\n";
		if ($line=~m/^>gi\|(\d+)\|.+\D(\d+)\.[\.,\d]+\.+(\d+)\D*/){
				if ($1 == 1383411){
#					print "$1 $2 $3\n";
#					die "found\n";
				}
#			print "OK1\n";
	#		print "$1 $2 $3\n";
			push (@dataset, new CDS($1, $2, $3, 'n'));
		}
		elsif ($line=~m/^>gi\|(\d+)\|.+\D(\d+)\.+(\d+)\D*/){
#			print "OK2 $1 $2 $3\n";
			
			push (@dataset, new CDS($1, $2, $3, 'n'));
			if ($2 == 1383411){
#				$mem = $dataset[$#dataset];
#				print "$1 $2 $3\n";
#				die "found 2 \n";
			}
		}
		elsif ($line=~m/^>/){
			die "failed to parse: $line";
		}
		else{
		#	die "FAILD: $line";
		}
#>gi|30995354|ref|NP_438271.2| iron-utilization periplasmic protein hFbpA, join(104277..104318,104318..105274) [
		
#>gi|16271977|ref|NP_438174.1| glyceraldehyde-3-phosphate dehydrogenase, 2..1021 [Haemophilus influenzae Rd KW20]
#print "over\n";
#exit;
	}
	close(R);	
#	exit;
#	&sort_array(@dataset);
#print "total entries: ", $#dataset,"\n";
	foreach (@dataset){
#		print $_->{_gi}, "\n";
	}
	my $numhit = 0;
	open (R, $blast) or die "cannot open file $blast";
#	my $previous;
	while ($line = <R>){
#		print $line;
		if ($line=~/^gi\|(\d+)\|([^\|]+)\|(\d+)/ or $line=~/^gi\|(\d+)\|ref\|[^\|]+\|([^\|]+)\|(\d+)/){
	#		print $line;
#			if ($1 == $previous){
#				next;
#			}
			my $refgi = $3;
#			$previous = $1;
	#		my $target = &binary_search_by_gi($1, 0, $#dataset, @dataset);
			my $target = &search_by_gi($1, \@dataset);
			if ($target == ""){
				print "error: gi not found\n";
				exit;
			}
			$target->set_type('p');
			if ($target->{_other} eq ""){
				my @array = ();
				$$target{_other} = \@array;
			}
			my $array = $target->{_other};
			if ($line=~/^gi\|(\d+)\|\t*([^\|]+)\|(\d+)\|\w+\|([^\|]+)\|\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)/ or $line=~/^gi\|(\d+)\|ref\|[^\|]+\|\t*([^\|]+)\|(\d+)\|\w+\|([^\|]+)\|\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)/){
				if ($#$array == -1){
					$numhit++;
					$target->{_refgi} = $refgi;
					$target->{_tag} = $2;
					$target->{_ref} = $4;
					$target->{_e} = $5;
				}
				my $otherhash = {_refgi => $refgi, _tag => $2, _ref=>$4, _e=>$5};
				push (@$array, $otherhash);
				$$target{_other} = $array;
			}
			else{
				print "error in parsing .out file";
				exit;
			}
			
		}	
		elsif ($line=~m/^gi/){
			die "DIE: $line\n";
		}
	}
	close (R);
	
	open (R, $ptt) or die "cannot open file $ptt";
	my $di = 0;
	my $CDScount = 0;
	while ($line = <R>){
		if ($line =~m/^(\d+)\.\.(\d+)\t(.*?)\t.*?\t.*?\t.*?\t(.*?)\t.*?\t(.*)/){
			$CDScount++;
			my $used = 0;
			for ($di = 0;$di <= $#dataset; $di++){
#				print $dataset[$di]->{_from}, " == ",$1, "*\n";
				if ($dataset[$di]->{_from} == $1 && $dataset[$di]->{_to} == $2){
					$used = 1;
					if ($3 eq '+'){
						$dataset[$di]->{_end} = $dataset[$di]->{_from};
					}
					else{
						$dataset[$di]->{_end} = $dataset[$di]->{_to};
					}
					my $pid = $4;
				#	if ($pid =~m/(\d+)/){
				#		$dataset[$di]->{_pid} = "PP_".$1;
					#	print "set pid to $4\n";
				#	}
				#	else {
				#		die "error: wrong PID format: $pid";
				#	}
					$dataset[$di]->{_pid} = $pid;
					$dataset[$di]->{_cdsindex} = $CDScount;
					$dataset[$di]->{_annotation} = $5;
					
					
					### NEW, add annotation finger prints
					my %fprint = ();
					
					foreach (@$fprint){
						my $marker = $$_[1];
						my $accmarker = $$_[0];
						if ($dataset[$di]->{_annotation} =~m/$marker/i){
							$fprint{$accmarker} = $_;
			#				print "MARKER: '$accmarker' '$marker' $dataset[$di]->{_pid}\n";
						}
					}
					$dataset[$di]->{_fprint} = \%fprint;
		#			print "assign $dataset[$di]->{_fprint}  \n";
					$di++;
					last;
				}
			}
			if ($used == 0){
				die "Data found in faa but not found in ptt: $line\n";
				foreach (@dataset){
	#				print ">>> $_->{_gi}  $_->{_from}\n";
				}
			}
		}
		elsif ($line =~m/^\d+\.\.\d+/){
			die "DIE: error parsing ptt file at line: $line";
		}
	}
	close (R);
#	print "we have ", $mem->{_from}, "  ", $mem->{_to},"\n";die "";
	$dataset[0]->{_density} = $numhit/($#dataset+1);
	return \@dataset;
}

# private functions
sub sort_array {
	my (@data) = @_;
	my $t;
	for my $x (0 .. $#data){
		for my $y ($x+1 .. $#data){
			if ($data[$y]->from < $data[$x]->from){
				$t = $data[$x];
				$data[$x] =$data[$y];
				$data[$y] = $t;
			}
		}
	}
}

sub binary_search_by_gi {
	my ($gi, $beg, $end, @data) = @_;
	if ($beg > $end){
#		print "erorr: GI ($gi) not found";
		return "";
	}
	my $pos = int(($beg+$end)/2);
#	print "$gi $beg $end ($pos) ", $data[$pos]->gi,"\n";
	if ($data[$pos]->gi < $gi){
#		print $data[$pos]->gi, "<", $gi, "\n";
		return  &binary_search_by_gi($gi, $pos+1, $end, @data);
	}
	elsif ($data[$pos]->gi > $gi){
#		print $data[$pos]->gi, ">", $gi, "\n";
		return &binary_search_by_gi($gi, $beg, $pos-1, @data);
	}
	else{
		return $data[$pos];
	}
}

sub search_by_gi {
	my ($gi, $data) = @_;
	
	foreach (@$data){
	#	print "compare: ", $_->{_gi}, " vs " ,$gi, "\n";
		if ($_->{_gi} == $gi){
			return $_;
		}
	}
	print "compare:  vs " ,$gi, "\n";
	die "cannot find $gi";
}

sub linear_search_by_gi {
	my ($gi, $db) = @_;
	foreach (@$db){
		if ($_->{_gi} == $gi){
			return $_;
		}
	}
	return "";
}
1;
