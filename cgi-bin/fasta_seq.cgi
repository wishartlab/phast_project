#!/usr/bin/perl -w

# show fasta protein sequence

use CGI;
use CGI::Carp qw(fatalsToBrowser);
my $q = new CGI;
my $seq = $q->param('seq');
my $location = $q->param('rec');

print "content-type: text/html \n\n";
print ">$location<br>";
my $ind=0;
while($ind < length($seq)){
	my $str= substr($seq, $ind , 60);
	print "$str<br>";
	$ind +=60;
}

exit;
