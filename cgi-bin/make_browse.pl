#!/usr/bin/perl -w

# this program is used to create a browse html file 'browse.html' 
# for all the cases in /var/www/html/phast/current/public/tmp.
# this program will take make_browse_list in tmp dir to load the previous summary ;
# if you want to refresh all the cases again, delete that file and run this program again
my $t1 = time;
my $base_dir ="/var/www/html/phast/current/public";
my $sub_program_dir = "/var/www/html/phast/current/public/sub_programs";
my $working_dir = "/var/www/html/phast/current/public/tmp";
my $Results_cgi="/cgi-bin/Results.cgi";
chdir $working_dir;
my $list = `ls`;
#print "List $list\n";
my @array =split("\n", $list);
my @gi_array=();
my @NC_arary=();
my @desc_array=();
my @f_array=();
my @phage_count_array=();
#system("rm  $working_dir/make_browse_list");
get_previous_results(\@f_array, \@NC_array, \@desc_array, \@gi_array, \@phage_count_array);
#print "size_f_array=".(scalar @f_array)."\n";
my @array1=();
foreach my $i (@array){
	chomp($i);
	my $found=0;
	foreach my $j (@f_array){
		if ($j eq $i){
			$found =1;
			last;
		}
	}
	if ($found==0){
		push @array1, $i;
	}
}
#print "size_be_array=".(scalar @array)."\n";
@array=@array1;
#print "size_af_array=".(scalar @array)."\n";
foreach my $f (@array){
	chomp($f);
	if (!(-d $f)) {
		next;
	}
	
	if (-e "$f/$f.done"){
		my $f_cp =$f;
		$f_cp =~s/_.*//;
		if ($f_cp =~/^13\d+$/ && length($f_cp) == 10){
			next;
		}
		$data = `cat  $f/$f.done`;
		if ($data =~/\nACCESSION:\s*(\S+)/s){
			$NC = $1;
		}else{
			$NC= '';
		}
		
		if ($data =~/\nGI:\s*(\d+)/s){
			$gi = $1;
		}else{
			$gi = '';
		}
		
		if ($data =~/^(.*?)\n/s){
			$desc = $1;
		}else{
			$desc = 'NONE';
		}
		if ($NC !~/NC_000000/ && $NC ne '' && $NC ne 'N/A'){
			push @NC_array, $NC;
			push @gi_array, $gi;
			push @desc_array, $desc;
			push @f_array, $f;
			#print "IN: $f  $NC  $gi\n";
			my $count = get_phage_count($f);
                	push @phage_count_array, $count;

		}
	}
	
		
}
my $t2 = time;
print "check list time = ".($t2-$t1)."\n";
# clean up the redundunt result.
my @gi1=();
my @NC1=();
my @f1=();
my @desc1 = ();
my @phage_count_array1=();
for (my $i =0; $i<=$#gi_array; $i++){
	my $flag = 0;
	for (my $j=0; $j <= $#gi1; $j++){
	#	print "'$f_array[$i]',  '$f1[$j]', '$gi1[$j]', '$gi_array[$i]' \n";
		if ($gi_array[$i] eq $gi1[$j]){
			$flag = 1;
			last;
		}
	}
	if ($flag ==0) {
		push @gi1, $gi_array[$i];
		push @NC1, $NC_array[$i];
		push @f1,  $f_array[$i];
		push @desc1, $desc_array[$i];
		push @phage_count_array1, $phage_count_array[$i];

	}
}
my $t3=time;
print "sort and filt time=".($t3-$t2)."\n";
open(OUT, ">browse.html") or die "Cannot write browse.html";
open(OUT2, ">browse1.html") or die "Cannot write browse1.html";
open(OUT1, ">list") or die "Cannot write list file!";
open(OUT4, ">make_browse_list") or die "Cannot write make_browse_list";
my $line='';
my $line1='';
$line .= "<tr><th>Description</th>
		 <th>Case number</th>
		 <th>NC number</th>
		<th>GI number</th>
		<th>PHAGE#</th> </tr>\n";
$line1 .= "<tr><th width='60%'>Description</th>
		 <th align='left' width='20%'>NC number</th>
		<th align='left' width='14%'>GI number</th>
		<th width='6%'>PHAGE#</th> </tr>\n";
my %hash_sort4=();
for (my $k =0; $k<= $#NC1; $k++){
	$hash_sort4{$NC1[$k]}= "$f1[$k]\t$NC1[$k]\t$gi1[$k]\t$desc1[$k]\t$phage_count_array1[$k]";	
}
@NC1 = sort{$a cmp $b} @NC1;
@gi1=();
@f1=();
@desc1=();
@phage_count_array1=();
foreach (@NC1){
	my @tmp = split("\t", $hash_sort4{$_});
	push @gi1, $tmp[2];
	push @f1, $tmp[0];
	push @desc1, $tmp[3];
	push @phage_count_array1, $tmp[4];
}
for (my $k =0; $k<= $#NC1; $k++){
	
	#print "$NC1[$k]  $gi1[$k]\n";
	print OUT4 "$f1[$k]\t$NC1[$k]\t$gi1[$k]\t$desc1[$k]\t$phage_count_array1[$k]\n";
		if ($NC1[$k] ne '' && $gi1[$k] ne ''){
			print OUT1  "$NC1[$k]  $gi1[$k]\n";
		}
		if ($gi1[$k] ne ''){
			$gi1[$k] = "$gi1[$k]";
		}
		$desc1[$k] =~ s/\s+$//;
		my $str = "<tr><td>$desc1[$k]</td>
		 <td><a href='$Results_cgi?num=$f1[$k]' target='_blank'>$f1[$k]</a></td>
		 <td><a href='$Results_cgi?num=$f1[$k]' target='_blank'>$NC1[$k]</a></td>
		<td align='center'><a href='$Results_cgi?num=$f1[$k]' target='_blank'>$gi1[$k]</a></td> 
		<td><a href='$Results_cgi?num=$f1[$k]'>$phage_count_array1[$k]</a></td></tr>\n";
		my $str1 = "<tr><td>$desc1[$k]</td>
		 <td><a href='$Results_cgi?num=$f1[$k]' target='_blank'>$NC1[$k]</a></td>
		<td><a href='$Results_cgi?num=$f1[$k]' target='_blank'>$gi1[$k]</a></td> 
		<td align='center'><a href='$Results_cgi?num=$f1[$k]' target='_blank'>$phage_count_array1[$k]</a></td></tr>\n";
		$line .=$str;
		$line1 .= $str1;
		
	
}
print OUT "<BODY BGCOLOR=\"white\">
	<CENTER>
	<IMG SRC=\"/image/phage.png\" height=125 width=500>
	</Center>
	<BR><BR> <table align='center' style=\"border:1px solid darkblue;width:900px;table-layout:fixed\"> 
            \n";
print OUT $line;
print OUT "</table></BODY>\n";
close OUT;

print OUT2 "<table align='center' style=\"border:1px solid darkblue;width:900px;table-layout:fixed\">
			\n";
print OUT2 $line1;
print OUT2 "</table>";
close OUT2;
close OUT1;
close OUT4;
my $t4=time;
print "make browse1.html and list time=". ($t4-$t3)."\n";
my $t_data = `cat browse1.html`;
my $total_count = `wc -l list`;
$total_count=~s/^(\d+).*/$1/;
my $t_html_con = `cat $base_dir/Download_template.html`;
$t_html_con =~s/##########insert table here#############/$t_data/s;
$t_html_con =~s/##########insert total count here#############/$total_count Genomes/s;
my $date = `cat $sub_program_dir/phage_finder/DB/update.log`;
my $size_vir_db = `du -sk $sub_program_dir/phage_finder/DB/virus.db`;
$size_vir_db=~s/^(\d+).*/$1/;$size_vir_db /=1024; $size_vir_db=int($size_vir_db*1000)/1000;
my $size_pro_db = `du -sk $sub_program_dir/phage_finder/DB/prophage_virus.db`;
$size_pro_db=~s/^(\d+).*/$1/;$size_pro_db /=1024; $size_pro_db=int($size_pro_db*1000)/1000;
my $size_phast_db = `du -sk $sub_program_dir/phage_finder/DB/phast.tz`;
$size_phast_db=~s/^(\d+).*/$1/;$size_phast_db /=1024;$size_phast_db=int($size_phast_db*1000)/1000;
my $size_bacteria= `du -sm $sub_program_dir/phage_finder/DB/bacteria_all_select.db`;
$size_bacteria=~s/^(\d+).*/$1/;
$size_bacteria /=1024;
$size_bacteria = int($size_bacteria*1000)/1000;
my $size_DNA= `du -smD $working_dir/z_DNA_fragment_DB.gz`;
$size_DNA=~s/^(\d+).*/$1/;
$size_DNA /=1024;
$size_DNA = int($size_DNA*1000)/1000;
my @arr = split(" ", $date);
$date = "$arr[1] $arr[2] $arr[$#arr]";
$t_html_con =~s/#####virus_size#####/$size_vir_db Mb/s;
$t_html_con =~s/#####prophage_size#####/$size_pro_db Mb/s;
$t_html_con =~s/#####bacteria_size#####/$size_bacteria Gb/s;
$t_html_con =~s/#####phast_db_size#####/$size_phast_db Mb/s;
$t_html_con =~s/#####DNA_db_size#####/$size_DNA Gb/s;
$t_html_con =~s/#####date#####/$date/gs;


open(OUT, ">$base_dir/Download.html") or die "Cannot rewrite $base_dir/Download.html";
print OUT $t_html_con;
close OUT;
my $t5= time;
print "make Download.html time=".($t5-$t4)."\n";
print "Total time of make_browse.pl = ". ($t5-$t1)."\n";
exit;
	
sub get_previous_results{
	my ($f_array, $NC_array, $desc_array, $gi_array, $phage_count_array)=@_;
	if (-e "$working_dir/make_browse_list"){
		open (IN, "$working_dir/make_browse_list");
		while(my $line=<IN>){
			chomp($line);
			if ($line=~/^\s*$/){
				next;
			}
			my @tmp=split("\t", $line);
			push @$f_array, $tmp[0];
			push @$NC_array, $tmp[1];
			push @$gi_array, $tmp[2];
			push @$desc_array, $tmp[3];
			push @$phage_count_array, $tmp[4];
		}
		close IN;
	}
}

sub get_phage_count{
	my $dir = shift;
	my $count = 0;
	if (-e "$dir/detail.txt"){
		open (IN1, "$dir/detail.txt");
        	while( my $line = <IN1>){
                	if ($line=~/#### region (\d+)\s+####/){
                        	$count=$1;
                	}
        	}
        	close IN1;
	}
	return $count;
}	
