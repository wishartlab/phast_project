#!/bin/bash

num=$1
db=$2
key='/apps/phast/.ssh/scp-key'
host="$HTTP_PHAST_CLUSTER_USERNAME@$HTTP_PHAST_CLUSTER_HOSTNAME"
output_file="pathogenicity.out"

echo "Parallel BLASTing $num.faa against the $db ..."
START=$(date +%s)
blast_p_dir="/home/prion/phage/tmp/$num/blast_p"
ssh -i $key  $host "mkdir -p $blast_p_dir"
scp -i $key  $num.faa  $host:$blast_p_dir/.
ssh -i $key  $host "cd $blast_p_dir; perl /home/prion/phage/cgi/call_blast_parallel.pl $blast_p_dir/$num.faa $db" >/dev/null
echo "transferring $output_file ..."
scp -i $key  $host:$blast_p_dir/$num.faa_blast_out  $output_file
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "Parallel BLASTing $num.faa against the $db $DIFF seconds"

exit
