#!/usr/bin/perl -w

# this program will input .faa file and phpico.txt file or phmedio.txt or phregions.txt file
# and bases on the  .txt file  to filter the .faa file. What is needed is the non phage blast hit part in .txt file.
# The output file should be the filtered info in .faa format.
# Usage: perl non_hit_region_pro_to_ptt.pl  <.faa file > <.txt file > <ouput_file> 

if (@ARGV!=3){
	print "Usage : perl non_hit_region_pro_to_ptt.pl  <.faa file > <.txt file > <ouput_file> \n";
	exit;
}

open(IN, $ARGV[1]) or die "Cannot open $ARGV[1]";
my @array = ();
while (<IN>) {
	if ($_=~/^(\d+)\s+\d+\s+\[ANNO\]\s*-/){
		push @array, $1;
	}
}
close IN;
my $num= scalar(@array);
open (IN, $ARGV[0]) or die "Cannot open $ARGV[0]";
open (OUT, ">$ARGV[2]") or die "Cannot write $ARGV[2]";
my $end_5 ='';
my $end_3 ='';
my $flag =0;
while(<IN>){
	if ($_=~/>gi.*?(\d+)\.\.(\d+)/ ){
		#>gi|00010|  [ Pseudomonas putida KT2440 chromosome      PP_00010        gene     9548..11062]
		#>gi|00009|  [ Pseudomonas putida KT2440 chromosome      PP_00009        gene      complement(8812..8946)]
		$end_5 = $1; $end_3 = $2;
		if ($_=~/complement\(/){
			my $temp = $end_5;
			$end_5 = $end_3;
			$end_3 = $temp;
		}
		$flag =0;
		foreach my $i (0..$#array){
			if ($array[$i] == $end_5){
				$flag = 1;
				last;
			}
		}
		if ($flag ==1){
			print OUT $_;
		}			
	}else{
		if ($flag ==1){
			print OUT $_;
		}
	}
}
close IN;
close OUT;
exit;

	
	
