package model {
	public class Band {
		public var next:Band;
		public var host:Region;
		
		public var id:int; // unique id
		public var index:int; // table index
		
		public var elementId:int = 0; // id for type of element
		
		public var start:int;
		public var end:int;
		public var direction:String;
		public var phagename:String;
		public var shortname:String;
		
		public var protein:String;
		public var species:String;
		public var ppnumber:String; // pp is a number (with put pp header)
		public var ginum:String; // gi could be null
		public var evalue:String; // a string because it could be null
		public var proteinseq:String;
		public var taglabel:String; // a shorten label for display tags
		
		public function Band(from:int, to:int, phage:String, direct:String, parent:Region){
			this.start = from;
			this.end = to;
			this.phagename = phage;
			this.direction = direct;
			this.host = parent;
		}
		
		public function pend(value:Band):void{
			if (this.next == null)
				this.next = value;
			else
				this.next.pend(value);
		}
		
		public function getLength():int{
			return this.end - this.start + 1;
		}
		/**
		 * parse the rest of tokens (newly added)
		 * this is a bit crappy because Jack forgot to remove space
		 * within a token. It parses the string backward
		 */
		public function build(tokens:Array):void{
			var re:RegExp = /(gi\d+)/;
			var parts:Array;
			if (tokens[4] != "Attachment_site"){
				this.proteinseq = tokens[tokens.length-1];
				this.evalue = tokens[tokens.length-2];
				this.ginum = "gi N/A";
				var proinfo:String = new String;
				for (var i:int = 5; i < tokens.length-2; i++){
					parts = String(tokens[i]).match(re);
					if (parts != null){
						this.ginum = parts[0];
						proinfo = proinfo + String(tokens[i]).substr(0, String(tokens[i]).length - String(parts[0]).length - 2);
					}
					else
						proinfo = proinfo + tokens[i];
				}
				tokens = proinfo.split(/;/);
				this.protein = tokens[1];
				this.ppnumber = tokens[0];
				
				this.species = tokens[2];
			}
			else{
				this.proteinseq = "Not avaliable";
				this.evalue = "evalue N/A";
				this.protein = tokens[5];
				this.ppnumber = tokens[1];
				this.ginum = "gi N/A";
				this.species = "N/A";
			}
			
			
		}
	}
}