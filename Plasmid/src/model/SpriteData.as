package model
{
	import flash.display.Sprite;
	
	import spark.components.Label;
	
	public class SpriteData extends Sprite
	{
		public var band:Band = null;
		public var index:int; // local index
		public var bin:int;
		public var region:Region = null;
		public var tag:spark.components.Label = null;
		public function SpriteData()
		{
			super();
		}
	}
}