package model
{
	import flash.events.*;
	import flash.geom.Point;
	
	import spark.components.Label;
	import spark.primitives.Rect;

	public class BandArea extends Rect
	{
		public var label:Label;
		public var band:Band;
		public var checked:Boolean;
		public function BandArea()
		{
			checked = false;
		}
	}
}