package model
{
	import mx.graphics.SolidColor;
	
	import spark.primitives.Ellipse;

	public class Legend
	{
		public var Index:int;
		public var Color:String;
		public var Name:String;
		public function Legend(id:int, c:uint, label:String)
		{
			this.Index = id;
			this.Name = label;
			this.Color = "";
		}
	}
}