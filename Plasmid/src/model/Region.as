package model {
	public class Region {
		public var from:int;
		public var to:int;
		public var index:int;
		public var next:Region;
		public var head:Band;
		public var type:String;
		public var position:int;
		public var cg:String;
		public function Region(start:int, end:int, id:int, t:String){
			this.from = start;
			this.to = end;
			this.index = id;
			this.type = t;
		}
		public function pend(value:Region):void{
			if (this.next == null)
				this.next = value;
			else
				this.next.pend(value);
		}
		public function addBand(band:Band):void{
			if (this.head == null)
				this.head = band;
			else
				this.head.pend(band);
		}
		public function getLength():int{
			return this.to - this.from + 1;
		}
		public function scaler(min_global_len:Number, min_local_len:Number):Number{
			var scale:Number;
			var shortest:Number = Number.NaN;
			var iterator:Band;
			for (iterator = this.head; iterator != null; iterator = iterator.next){
				if(isNaN(shortest))
					shortest= iterator.getLength();
				else if (iterator.getLength() < shortest)
					shortest= iterator.getLength();
			}
			scale = min_local_len / shortest;
			if (this.getLength() * scale < min_global_len){
				scale = min_global_len / this.getLength();
			}
			return scale;
		}
		public function numberOfCDS():int{
			var b:Band;
			var c:int = 0;
			for (b=this.head; b != null; b = b.next){
				if (b.protein != "Attachment_site")
					c++;
			}
			return c;
		}
	}
}