package model {
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.controls.listClasses.ListBaseContentHolder;
	import mx.core.FlexShape;

	public class MyDataGrid extends DataGrid {
		public var metadata:MetaData;
		
		public function MyDataGrid(){
			super();         
		}
		override protected function drawSelectionIndicator(
			indicator:Sprite, x:Number, y:Number,
			width:Number, height:Number, color:uint,
			itemRenderer:IListItemRenderer):void
		{
			var g:Graphics = Sprite(indicator).graphics;
			g.clear();
			g.beginFill(color);
			g.drawRect(50, 0, this.width-50, height);
			g.endFill();
			
			indicator.x = x;
			indicator.y = y;
		}
		
		override protected function drawHighlightIndicator(
			indicator:Sprite, x:Number, y:Number,
			width:Number, height:Number, color:uint,
			itemRenderer:IListItemRenderer):void
		{
			var g:Graphics = Sprite(indicator).graphics;
			g.clear();
			g.beginFill(color);
			g.drawRect(50, 0, this.width-50, height);
			g.endFill();
			
			indicator.x = x;
			indicator.y = y;
		}
		
		override protected function drawRowBackground(s:Sprite, rowIndex:int, y:Number, height:Number, color:uint, dataIndex:int):void {
			var contentHolder:ListBaseContentHolder = ListBaseContentHolder(s.parent);
			var background:Shape;
			if (rowIndex < s.numChildren){
				background = Shape(s.getChildAt(rowIndex));             
			}             
			else{                 
				background = new FlexShape();
				background.name = "background";
				s.addChild(background);
			}
		
			background.y = y;
		
			// Height is usually as tall is the items in the row, but not if
			// it would extend below the bottom of listContent
			var height:Number = Math.min(height, contentHolder.height - y);
	
			var g:Graphics = background.graphics;
			g.clear();
		
			var color2:uint = 0x0;
			if(dataIndex<this.dataProvider.length){
				color2 = this.metadata.findBandColorCode(this.dataProvider.getItemAt(dataIndex).Name);
	//			if(this.dataProvider.getItemAt(dataIndex).Color){
	//				ArrayCollection(this.dataProvider).getItemAt(dataIndex).Color = "";
	//			}
	//			else{
	//				color2 = color;
	//			}
			}
			else {
				color2 = color;
			}
			g.beginFill(color, getStyle("backgroundAlpha"));
			g.drawRect(50, 0, contentHolder.width-50, height);
			g.endFill();
			if (color2 != color){
				g.beginFill(color2, getStyle("backgroundAlpha"));
				g.drawEllipse(10, height*0.25, 30, height*0.5);
				g.endFill();
			}
		}
		
		override protected function mouseOutHandler(event:MouseEvent):void
		{
			super.mouseOutHandler(event);
		}
		public function adjustColSize():void{
			DataGridColumn(this.columns[0]).width = 52;
			DataGridColumn(this.columns[1]).width = 50;
			DataGridColumn(this.columns[2]).width = this.width-100;
		}
	}
}
