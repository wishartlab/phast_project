package model {
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.*;
	
	public class MetaData {
		import mx.graphics.SolidColor;
		
		/* master */
		public var master:Plasmid;
		
		/* indexed tables */
		public var totalNumOfBands:int = 0; // number of bands (not number of unique bands)
		public var _bandNames:Array; // name table (String)
		public var _bandColors:Array; // color table ("SolidColor")
		public var _bandByID:Array; // Band object table (used for pop up window, "Band")
		
		/* URL data */
		private const _domainURL:String = "http://216.69.156.9";//"http://ec2-184-73-211-12.compute-1.amazonaws.com";
		public var loadingComplete:Boolean = false;
		private var _URL:Object;
		private var _figureURL:String;// = "http://ec2-184-73-211-12.compute-1.amazonaws.com/phage/tmp/26986745/png_input";
		private var _urlReq:URLRequest;
		private var _urlLdr:URLLoader;
		
		
		/* data structure */
		public var headRegion:Region;
		public var plasmidName:String="Example Plasmid";
		public var totalbasepairs:int=1000000;
		
		/** constructor */
		public function MetaData(URL:Object){
			this._urlLdr = new URLLoader();
			this._URL = URL;
			this._urlLdr.addEventListener(Event.COMPLETE, doEvent);
			this._urlLdr.addEventListener(Event.OPEN, doEvent);
			this._urlLdr.addEventListener(HTTPStatusEvent.HTTP_STATUS, doEvent);
			this._urlLdr.addEventListener(IOErrorEvent.IO_ERROR, doEvent);
			this._urlLdr.addEventListener(ProgressEvent.PROGRESS, doEvent);
			this._urlLdr.addEventListener(SecurityErrorEvent.SECURITY_ERROR, doEvent);
		}
		
		/* file data process functions */
		public function get getRegion():Region{
			return this.headRegion;
		}
		
		public function addRegion(value:Region):void{
			if (this.headRegion == null)
				this.headRegion = value;
			else
				this.headRegion.pend(value);
		}
		
		
		/* load file */
		public function loadFile():void{
			this._figureURL = this._domainURL+"/tmp/"+this._URL.id+"/png_input\n";
			this.master.addDebugInfo("load from "+this._figureURL);
			this._urlReq = new URLRequest(_figureURL);
			this.loadingComplete = false;
			Security.allowDomain("*");
			this._urlLdr.load(_urlReq);
		}
		
		/* parser file */
		
		private function doEvent(evt:Event):void {
			if (evt.type == HTTPStatusEvent.HTTP_STATUS){
				this.master.addDebugInfo("HTTP status: "+HTTPStatusEvent(evt).status+"\n");
			}
			else if (evt.type == IOErrorEvent.IO_ERROR){
				this.master.addDebugInfo("IO error\n");
			}
			else if (evt.type == SecurityErrorEvent.SECURITY_ERROR){
				this.master.addDebugInfo("security error\n");
			}
			else if (evt.type == Event.COMPLETE){
				var ldr:URLLoader = evt.currentTarget as URLLoader;
		//		this.master.addDebugInfo(ldr.data);
				// setup regions
				var allBands:Band;
				var band:Band;
				var allRegions:Region;
				var region:Region;
				var data:String = ldr.data as String;
				var lines:Array;
				var tokens:Array;
				var i:int;
				if (data != null){
					lines = data.split("\n", 10000);
				//	this.master.addDebugInfo("TOTAL lines = "+lines.length+"\n");
					for (i = 0; i < lines.length; i++){
				//		this.master.addDebugInfo(lines[i] as String);
						if (String(lines[i]).charAt(0) == ">"){
							tokens = String(lines[i]).split(",");
							this.plasmidName = tokens[0];
							tokens = String(lines[i]).split(" ");
							this.totalbasepairs = parseInt(tokens[tokens.length-1]);
					//		this.master.debug.text+="READ TOTAL = "+String(lines[i]).split(",")[2]+"\n";
						}
						else if (String(lines[i]).length < 7)
							continue;
						else if (String(lines[i]).substr(0, 7) == "section"){
				//			this.master.addDebugInfo(lines[i] as String);
							tokens = String(lines[i]).split(/\s\s\s+/);
							band = new Band(parseInt(tokens[1]), parseInt(tokens[2]), tokens[4], this.decodeDirectionFlag(String(tokens[3])), null);
							this.master.addDebugInfo(band.start+"\n");
							this.master.addDebugInfo(band.end+"\n");
							this.master.addDebugInfo(band.phagename+"\n\n\n");
							band.build(tokens);
							if (allBands == null)
								allBands = band;
							else
								allBands.pend(band);
						}
						else if (String(lines[i]).substr(0, 6) == "region"){
							tokens = String(lines[i]).split(/\s+/);
							region = new Region(parseInt(tokens[2]), parseInt(tokens[3]), parseInt(tokens[1]), tokens[4]);
							region.cg = tokens[5];
							if (allRegions == null)
								allRegions = region;
							else
								allRegions.pend(region);
						}
					}
					// direct bands to specific region
					if (allRegions == null){
						// no regions error
					}
					this.headRegion = null;
					// add regions
					region = allRegions;
					if (allRegions != null)
						allRegions = allRegions.next;
					while (region != null){
						region.next = null;
						this.addRegion(region);	
						region = allRegions;
						if (allRegions != null)
							allRegions = allRegions.next;
					}
					// add band to region
					band = allBands;
					if (allBands != null)
						allBands = allBands.next;
					while (band != null){
						band.next = null;
						for (region = this.headRegion; region != null; region = region.next){
							if (band.start <= region.to && band.end >= region.from){
								band.host = region;
								region.addBand(band);
								break;
							}
						}
						band = allBands;
						if (allBands != null)
							allBands = allBands.next;
					}
					// read completed
					this.loadingComplete = true;
					this.assignData(); // assign band into tables
					this.master.showData(); // call master object to show data
					return;
				}
			} 
		}
		private function decodeDirectionFlag(flag:String):String{
			if (flag.charAt(0) == "+")
				return "forward";
			return "backward";
		}
		
		/* process file */
		/**
		 * assign data color table
		 */
		private function assignData():void{
			this.totalNumOfBands = 0; 
			this._bandNames = new Array;
			this._bandColors = new Array;
			this._bandByID = new Array;
			for (var r:Region = this.headRegion; r != null; r = r.next){
				for (var b:Band=r.head; b != null; b=b.next){
					var index:int = this.insertToColorTable(b.phagename);
					b.elementId = this.elementId(b.phagename);
					b.id = this.totalNumOfBands;
					b.index = index;
					b.shortname = this.rename(b.phagename);
					b.taglabel = this.shortname(b.phagename);
					this.totalNumOfBands++;
					this._bandByID.push(b);
				}
			}
		}
		
		/**
		 * find the index of a band, given band name
		 */
		public function findBandIndex(bandName:String):int{
			var index:int;
			for (index = 0; index < this._bandNames.length; index++)
				if (String(this._bandNames[index]).localeCompare(bandName) == 0)
					return index;
			return -1;
		}
		
		/**
		 *  find the color of a band giiven its name
		 */
		public function findBandColorCode(bandName:String):uint{
			var index:int = this.findBandIndex(bandName);
			return SolidColor(this._bandColors[index]).color;
		}
		
		/**
		 * add a new band and randomly assign a color to it
		 * bands must have unqiue names, if there's already a band with identical name
		 * the index of that band is returned
		 */
		public function insertToColorTable(bandName:String):int{
			var index:int = this.findBandIndex(bandName);
			var colornew:SolidColor;
			if (index >= 0)
				return index;
			// now assign new color
			this._bandNames.push(bandName);
			// check if it's a pre-defined name, which has pre-defined colour
			if (bandName == "Attachment_site")
				colornew = new SolidColor(0x000000, 0.9);
			else if (bandName == "Phage-like_protein")
				colornew = new SolidColor(0xFF9900, 0.9);
			else if (bandName == "Terminase")
				colornew = new SolidColor(0x00FFFF, 0.9);
			else if (bandName == "Intergrase")
				colornew = new SolidColor(0xFF0000, 0.9);
			else if (bandName == "Non_phage-like_protein")
				colornew = new SolidColor(0xFFFFFF, 0.9);
			else if (bandName == "Hypothetical_protein")
				colornew = new SolidColor(0x153E7E, 0.9);
			else if (bandName == "Portal_protein")
				colornew = new SolidColor(0xFF8040, 0.9);
			
			else if (bandName == "Head_protein")
				colornew = new SolidColor(0x0000FF, 0.9);
			else if (bandName == "Plate_protein") 
				colornew = new SolidColor(0x6600FF, 0.9);
			
			else if (bandName == "Tail_protein")
				colornew = new SolidColor(0x808000, 0.9);
			else if (bandName == "Transposase")
				colornew = new SolidColor(0xFFF8C6, 0.9);
			else if (bandName == "Lysis_protein")
				colornew = new SolidColor(0x6C2DC7, 0.9);
			else if (bandName == "Fiber_protein")
				colornew = new SolidColor(0x254117, 0.9);
			else if (bandName == "tRNA")
				colornew = new SolidColor(0xFFC0CB, 0.9);
			else if (bandName == "Protease")
				colornew = new SolidColor(0xFFFF00, 0.9);
			else{
				this.master.debug.text+="unrecognized tag: "+bandName+"\n";
				colornew = new SolidColor(0xFFFFFF*Math.random(), 0.9);
			}
			this._bandColors.push(colornew);
			return this._bandNames.length-1;
		}
		// get element id by name
		public function elementId(bandName:String):int{
			if (bandName == "Attachment_site")
				return 7;
			else if (bandName == "Phage-like_protein")
				return 9;
			else if (bandName == "Terminase")
				return 2;
			else if (bandName == "Intergrase")
				return 8;
			else if (bandName == "Non_phage-like_protein")
				return 11;
			else if (bandName == "Hypothetical_protein")
				return 10;
			else if (bandName == "Portal_protein")
				return 3;
				
			else if (bandName == "Head_protein")
				return 5;
			
				
			else if (bandName == "Tail_protein")
				return 6;
			else if (bandName == "Transposase")
				return 12;
			else if (bandName == "Lysis_protein")
				return 1;
			else if (bandName == "Fiber_protein")
				return 13;
			else if (bandName == "Plate_protein")
				return 14;
			else if (bandName == "tRNA")
				return 15;
			else if (bandName == "Protease")
				return 4;
			else{
				this.master.debug.text+="unrecognized tag: "+bandName;
				return 0;
			}
		}
		/** give a better name for display purpose */
		public function rename(bandName:String):String{
			if (bandName == "Attachment_site")
				return "Att";
			else if (bandName == "Phage-like_protein")
				return "Phage-like protein";
			else if (bandName == "Terminase")
				return "Terminase";
			else if (bandName == "Intergrase")
				return "Intergrase";
			else if (bandName == "Non_phage-like_protein")
				return "Other";
			else if (bandName == "Hypothetical_protein")
				return "Hypothetical protein";
			else if (bandName == "Portal_protein")
				return "Portal";
			
			else if (bandName == "Head_protein")
				return "Coat";
			else if (bandName == "Plate_protein")
				return "Plate";
			
			else if (bandName == "Tail_protein")
				return "Tail shaft";
			else if (bandName == "Transposase")
				return "Transposase";
			else if (bandName == "Lysis_protein")
				return "Lysis";
			else if (bandName == "Fiber_protein")
				return "Tail fiber";
			else if (bandName == "tRNA")
				return "tRNA";
			else if (bandName == "Protease")
				return "Protease";
			else{
				return bandName;
			}
		}
		
		public function shortname(bandName:String):String{
			if (bandName == "Attachment_site")
				return "Att";
			else if (bandName == "Phage-like_protein")
				return "PLP";
			else if (bandName == "Terminase")
				return "Ter";
			else if (bandName == "Intergrase")
				return "Int";
			else if (bandName == "Non_phage-like_protein")
				return "Oth";
			else if (bandName == "Hypothetical_protein")
				return "Hyp";
			else if (bandName == "Portal_protein")
				return "Por";
				
			else if (bandName == "Head_protein")
				return "Coa";
			else if (bandName == "Plate_protein")
				return "Pla";
				
			else if (bandName == "Tail_protein")
				return "sha";
			else if (bandName == "Transposase")
				return "Tra";
			else if (bandName == "Lysis_protein")
				return "Lys";
			else if (bandName == "Fiber_protein")
				return "fib";
			else if (bandName == "tRNA")
				return "RNA";
			else if (bandName == "Protease")
				return "Pro";
			else{
				return bandName;
			}
		}

	}
}