#!/usr/bin/perl
use strict;
package PhageTable;
use Gene;
use Prophage;
use Record;
use Dictionary;
use IS;

my $IS = new IS();
sub new
{
	my $class = shift;
	my %GI_acc = ();
	my %acc_name = ();
	my %acc_size = ();
	my %acc_num_gene = ();
	my %localacchash = ();
	my %acc_GIs=();
	my $self = {
		# constant data
		_file => shift,		# phage genome file
		_GI_acc => \%GI_acc,		# hash GI -> its host's acc
		_acc_name => \%acc_name,	# acc -> genome name
		_acc_size => \%acc_size,		# acc -> genome size
		_acc_num_gene => \%acc_num_gene,	# acc -> number of genes in genome
		_acc_GIs=> \%acc_GIs,
		# assessment data
		_localacchash => \%localacchash,	# store informations of a given phage
		_integrase => 0,
        _integrase_names=>'',
        _transposase => 0,
        _transposase_names=>'',
        _structural => 0,
        _structural_names=>'',
		_totalgene => 0,  # for completeness score
		_totalbase => 0,  # for completeness score
		_useIS => shift,	# wether to use IS filter
		_IS => 0			# IS element to exclude
	};
	bless $self, $class;
	
	# load file into constant hashes
	open (R, $self->{_file}) or die "cannot read phage genome file",  $self->{_file};
	while (my $line=<R>){
		chomp($line);# added by Jack on Jan 24, 2013; no this line, the last element when arrayed will be with '\n';
		my @tokens = split(/\t/, $line);
		if ($#tokens > 3){
			my ($acc, $name, $size, $ngene);
			$acc = $tokens[0];
			$name = $tokens[1];
			$size = $tokens[2];
			$ngene = $tokens[3];
		 	next if (defined $acc_name{$acc});	
			$acc_name{$acc} = $name;
			$acc_size{$acc} = $size;
			$acc_num_gene{$acc} = $ngene;
			for my $i (4 .. $#tokens){
				if (defined $GI_acc{$tokens[$i]}){
					#warn "Warning: GI:tokens[$i] has been found in more than one place in phage table";
				}
				else{
					$GI_acc{$tokens[$i]} = $acc;
				}
				$acc_GIs{$acc}.= "$tokens[$i] ";
			}
		}
	}
	return $self;
}

# reset the assessment hash
sub clear {
	my ($self) = @_;
	my %hash = ();
	$self->{_localacchash} = \%hash;
	$self->{_integrase} = 0;
    $self->{_integrase_names}='';
    $self->{_transposase} = 0;
    $self->{_transposase_names}='';
    $self->{_structural} = 0;
    $self->{_structural_names}='';
	$self->{_totalgene} = 0;
	$self->{_totalbase} = 0;
	$self->{_IS} = 0;
}

sub printParts {
	my ($self) = @_;
	print "Integrase:   ", $self->{_integrase},"\n";
	print "Transposase: ", $self->{_transposase},"\n";
	print "Other keys:  ", $self->{_structural},"\n";
	print "IS        :  ", $self->{_IS},"\n";
}
sub assessprophage {
	my ($self, $prophage, $n, $c) = @_;
	if ($prophage->getNumberOfCDS() > 15){
		$self->{_integrase} += 1;
		$self->{_integrase_names}.="# integrase CDS >15, ";
	}

	if ($prophage->getNumberOfCDS() > 20){
		$self->{_integrase} += 1;
		$self->{_structural} += 1;
		$self->{_integrase_names}.="# integrase CDS >20, ";
        $self->{_structural_names}.="# struc CDS >20, ";
	}
	$self->{_totalbase} += $prophage->getTail()->getEnd() - $prophage->getHead()->getStart() + 1;
	my $gene_c=0;
	for (my $gene = $prophage->getHead(); $gene ne ''; $gene = $gene->next()){	
		$gene_c++;
		if ($self->{_useIS} > 0 and $IS->isExc($gene->getLocalGI()) == 1){
			$self->{_IS} = -1;
		}
		elsif ($self->{_useIS} > 0 and $IS->isInc($gene->getLocalGI()) == 1){
			$self->{_IS} = 1;
		}	
#		$gene->printInfo(0);
#		if ($gene->getProduct() =~m/integrase/i or $gene->getProduct() =~m/site[\s\-]specific recombinase/i or $gene->getProduct() =~m/phage recombinase/i){
		 if ($gene->getProduct() =~m/integrase|specific recombinase|phage recombinase/i){
			$self->{_integrase} += 1;
			$self->{_integrase_names}.=$gene->getProduct()."gi".$gene->getLocalGI.", ";	
		}elsif ($gene->getProduct() =~m/transposase/i){
			$self->{_transposase} += 1;
			$self->{_transposase_names}.=$gene->getProduct()."gi".$gene->getLocalGI.", ";	
		}
		elsif (	$gene->getProduct() =~m/(capsid|holin|head|coat|envelope|virion|flippase|host|injection)/i){
			$self->{_structural} += 1;
			$self->{_structural_names}.=$gene->getProduct().$gene->getLocalGI.", ";	
		}
		
		my $records = $gene->getBLASTresult();
#		print STDERR "aaaa row $n column $c, rec count=".(scalar @$records)."\n" if ($records ne '' && $records ne 'keyword' && $n >=0);
		my $gi_str='';
		if ($records ne '' and $records ne 'keyword'){
			foreach (@$records){
				#if ($_->getDefinition() =~m/integrase/i or $_->getDefinition() =~m/site.specific recombinase/i or $_->getDefinition() =~m/phage recombinase/i)
				if ($_->getDefinition() =~m/integrase|specific recombinase|phage recombinase/i)

				{
					$self->{_integrase} += 1;
					$self->{_integrase_names}.=$_->getDefinition()."gi".$_->getGI.", ";
#					print STDERR "aaaa row $n column $c, integrase=". $_->getGI."\n" if (($n==1||$n==2)&&$n >=0);
				}
				elsif ($_->getDefinition() =~m/transposase/i){
					$self->{_transposase} += 1;
					$self->{_transposase_names}.=$_->getDefinition()."gi".$_->getGI.", ";
#					print STDERR "aaaa row $n column $c transposase=". $_->getGI."\n" if (($n==1||$n==2)&&$n>=0);
				}
				elsif ($_->getDefinition() =~m/(capsid|holin|head|coat|envelope|virion|flippase|host|injection)/i){
					$self->{_structural} += 1;
					$self->{_structural_names}.=$_->getDefinition()."gi".$_->getGI.", ";	
#					print STDERR "aaaa row $n column $c, _strcutural detected\n" if ($n>=0);
				}else{
#					print STDERR "aaaa row $n column $c,no inte , no trans. no struct detected\n" if ($n>=0);
				}
				my $gi = $_->getGI();
				my $acc = '';
				if (defined $self->{_GI_acc}->{$gi}){
#					print $self->{_GI_acc}->{$_->getGI()};
					$acc = $self->{_GI_acc}->{$gi};
					if (defined $self->{_localacchash}->{$acc}){
						$self->{_localacchash}->{$acc}->{$gi} = 1;
					}
					else{
						my %newhash = ();
						$newhash{$gi} = 1;
						$self->{_localacchash}->{$acc} = \%newhash;
					}
				}
				else{
	
				}
				my $query_gi= $_->getQueryGI;
				if ($gi_str !~/$query_gi/){
					$self->{_totalgene}++ ;
					$gi_str.="$query_gi ";
				}
#				$_->printInfo();
			}
		}
		
		
		if ($gene == $prophage->getTail()){
			last;
		}
	}

}

sub evaluate {
	my ($self, $gsize, $steppenalty, $compscore) = @_;
	# penalty will be total gaps between joined prophages
#	print STDERR "nnn steppenalty=$steppenalty\n";
	my $score = 0 - abs($steppenalty);
	# base score makes sure the phage has integrase and structural protein (transposase+structural for mu-likes)
#	print STDERR "mmm ".$self->{_IS}."\n";
#	print STDERR "_integrase=".$self->{_integrase}." _transposase=".$self->{_transposase}." _structural=".$self->{_structural}." \n";
	if ($self->{_IS} == 1){
		$score += $gsize;
#		print STDERR "lll add $gsize to score\n";
	}
	elsif ($self->{_IS} == -1){
		$score += 0;
#		print STDERR "lll No add $gsize to score\n";
	}
	elsif (($self->{_integrase} > 0 and $self->{_structural} > 0) or # regular phage
	($self->{_transposase} > 0 and $self->{_structural} > 0)){ # Mu llike phage
		$score += $gsize;
#		print STDERR "lll add $gsize to score with 3 \n";
	}
	my $hit_names = $self->{_integrase_names}.$self->{_transposase_names}. $self->{_structural_names};
	# phage genome bonus join two prophages if they can make a complete known phage genome
	my $max = 0; # record the max score in the while loop
	my $NC_max='NA';
	my $percentage_max=-1;
	my $hit_count_max=-1;
	my $origin_length_max=-1;
	my $max_gis='';
	while (my ($acc, $hash) = each %{$self->{_localacchash}}){
	        my $curkey = scalar(keys %$hash);				# number of genes found
		my $value = join " ", keys %$hash;
		my $orikey = $self->{_acc_num_gene}->{$acc};	# number of genes in phage genome
		my $gis =$self->{_acc_GIs}->{$acc};
		my $s = 0;
		my $percentage= $curkey/$orikey;
		#from Jack, modified on Jan 23.2013
		if ($percentage > $percentage_max){
			$percentage_max=$percentage;
			$hit_count_max= $curkey;
			$origin_length_max= $orikey;
			$NC_max=$acc;
			$max_gis=$gis;
		}
		if ($orikey < 20){
			if ($percentage >= 0.5){
				$s = $compscore*$percentage;
			}
		}
		else{
			if ($percentage >= 0.15){
				$s = $compscore*$percentage;
			}
		}
#		print STDERR "pppp $acc, $curkey,'$value', $orikey, $compscore, $s\n";
		if ($s > $max){
			$max = $s;
		}
#			print "$acc  $curkey / $orikey: $s\n";
	}
	$score += $max;
#	print STDERR "final = $score, percentage_max=$percentage_max, hit_count_max=$hit_count_max, origin_length_max=$origin_length_max, NC_max=$NC_max\n";
	return $score, $percentage_max, $hit_count_max, $origin_length_max, $NC_max, $max_gis, $hit_names;
}

# assign a completeness (was called viablity) score to a prophage (wrapped in PhageTable class)
# the output is a string (so Jack doesn't need to change his parser)
# output (according to paper): case A: =150=PHAGE NAME=
# case B: =some score=PHAGE NAME=
# case C: =-1=
# the script doesn't calculate score for case C (it will be added by Jack's script) 
sub completeness {
	my ($self) = @_;
	my $mostcompletephage = ''; # the phage that has most genes in the prophage region (by %)
	my $mostpercentage = 0; # percentage of above
	my $most_curkey='';
	my $most_orikey='';
	while (my ($acc, $hash) = each %{$self->{_localacchash}}){
	        my $curkey = scalar(keys %$hash);				# number of genes found
			my $orikey = $self->{_acc_num_gene}->{$acc};	# number of genes in phage genome
			if ($curkey == $orikey){ # case A
				return "=150=", $self->{_acc_name}->{$acc},"=";
			}
			if ($curkey/$orikey > $mostpercentage){
				$mostpercentage = $curkey/$orikey;
				$mostcompletephage = $acc;
				$most_curkey=$curkey;
				$most_orikey=$orikey;
			}
	}
	if ($mostpercentage >= 0.5){
	#	my $score = 100*($self->{_totalbase})/($self->{_acc_size}->{$mostcompletephage});
		my $score = int(150*$mostpercentage);
		if ($score > 150){
			$score = 150;
		}
		return "=$score=".($self->{_acc_name}->{$mostcompletephage})."=";
	}
	return "=-1="; # default result for case C
}
1;
